<head>
    <meta charset="utf-8">
    <title>
       <?php echo $title; ?>
    </title>
    <meta name="description">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="apple-touch-icon" href="//ced.ky.gov/Webfiles/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="//ced.ky.gov/Webfiles/favicon.ico">
    <meta property="og:site_name" content="Team Kentucky | Cabinet for Economic Development">
    <meta property="og:title" content="Team Kentucky">
  
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?04022019">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?04022019">

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-QZX89X3Y1Y"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-QZX89X3Y1Y');
</script>


    <!--Bootstrap-->
    <link rel="stylesheet" href="site/lib/bootstrap/dist/css/bootstrap.min.css" />
      <link rel="stylesheet" href="site/css/site.css" />
<!--     <link rel="stylesheet" href="site/css/nky.css" /> -->
     <link rel="stylesheet" href="site/css/nky-menu.css" />


<!-- WEB FONT -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Rokkitt:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="site/lib/fontawesome/all.min.css" />


</head>

<body>


<!--
<nav class="navbar navbar-expand-lg navbar-light nav-menu sticky-top">

     <a class="navbar-brand col-lg-3 col-6" href=""><img src="/site/images/logo.jpg" class="img-fluid"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
--> 
      <!-- has to be col-12 because it needs to be full width on small when the button is toggled -->
<!--
      <div class="collapse navbar-collapse col-lg-9 col-12" id="navbarSupportedContent">
        <ul class="navbar-nav nav-fill w-100">
            <li class="nav-item"><a class="nav-link" href="https://ced.ky.gov/Locating_Expanding/Why_Kentucky">WHY KENTUCKY</a></li>
            <li class="nav-item"><a class="nav-link" href="">Speed to Market</a></li>
            <li class="nav-item"><a class="nav-link" href="https://ced.ky.gov/Locating_Expanding/Financial_Incentives">Incentives & Programs</a></li>
            <li class="nav-item"><a class="nav-link" href="https://ced.ky.gov/Locating_Expanding/Available_Sites_Buildings">Available Sites</a></li>
            <li class="nav-item"><a class="nav-link" href="https://ced.ky.gov/Newsroom/News_Releases">News</a></li>
            <li class="nav-item"><a class="nav-link" href="https://ced.ky.gov/Home/ContactUs">Contact</a></li>
        </ul>
       
      </div>
</nav>
-->

<nav class="navbar navbar-expand-lg navbar-light nav-menu sticky-top">
  <a class="navbar-brand" href="NKY-index.php"><img src="/site/images/logo.jpg"  style="height: 70px;"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-top" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Why Kentucky
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <div class="container">
            <div class="row">
              <div class="col-md-8">
                <span class="nav-section">Why Kentucky</span>
                <ul class="nav flex-column">
<li class="nav-item"><a class="nav-link" href="NKY-lifeInKy.php">Life in Kentucky</a></li>
<li class="nav-item"><a class="nav-link" href="NKY-workforce.php">Workforce and Talent</a></li>
<li class="nav-item"><a class="nav-link" href="NKY-BusinessCosts.php">Business Costs</a></li>
<li class="nav-item"><a class="nav-link" href="NKY-TopIndustries.php">Top Industries</a></li>
<li class="nav-item"><a class="nav-link" href="NKY-International.php">International</a></li>
<li class="nav-item"><a class="nav-link" href="NKY-FuturePlanning.php">Future Planning</a></li>
<!-- <li class="nav-item"><a class="nav-link" href="NKY-Awards.php">Awards and Successes</a></li> -->
              </ul>
              </div>
              <!-- /.col-md-4
              <div class="col-md-4">
    
              </div>  -->
              <!-- /.col-md-4  -->
              <div class="col-md-4 border-left">
                <hr class="spacer-25">
                
                  <img src="/site/images/1.jpg" alt="ky" class="img-fluid">
                
                <p>
                    <h5>Your New Kentucky Home</h5>
                </p><p>
                    In Kentucky, job growth, lifestyle and affordability fire on all cylinders. From landing your dream job and finding the perfect place to live, to chasing adventure around our beautiful state; Welcome to your New Kentucky Home.</p>
              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->


        </div>
      </li>


      <li class="nav-item"><a class="nav-link" href="NKY-SpeedToMarket.php">Speed to Market</a></li>


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-top" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Incentives & Programs
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <div class="container">
            <div class="row">
              <div class="col-md-8">
                <span class=" nav-section">Incentives & Programs</span>
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link" href="/NKY-AccessToCapital.php">Access to Capital</a></li>
                    <li class="nav-item"><a class="nav-link" href="/NKY-SmallBusinessCredits.php">Small Business Credits</a></li>
                    <li class="nav-item"><a class="nav-link" href="https://www.kyinnovation.com/" target="_blank">KY Innovation</a></li>
                    <li class="nav-item"><a class="nav-link" href="/NKY-KPDI.php">Kentucky Product Development Initiative</a></li>
                   <!--  <li class="nav-item"><a class="nav-link" href="#">Industry Specific Support</a></li> -->
              </ul>
              </div>
              <!-- /.col-md-4  
              <div class="col-md-4">
                <ul class="nav flex-column">
                <li class="nav-item"> </li>
              </ul>
              </div>-->
              <!-- /.col-md-4  -->
              <div class="col-md-4 border-left">
                <hr class="spacer-25">
                  <img src="/site/images/2.jpg" alt="ky" class="img-fluid">
                <p class="text-white">Contact Us</p>
              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->

        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-top" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         Available Sites
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
<div class="container">
            <div class="row">
              <div class="col-md-8">
                <span class="nav-section">Available Sites</span>
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link" href="https://properties.zoomprospector.com/kentucky" target="_blank">Available Sites & Buildings Search Tool</a></li>
                    <li class="nav-item"><a class="nav-link" href="/NKY-BuildReady.php">Build-ready sites</a></li>
              </ul>
              </div>
              <!-- /.col-md-4  
              <div class="col-md-4">
                <ul class="nav flex-column">
                <li class="nav-item"> </li>
              </ul>
              </div>-->
              <!-- /.col-md-4  -->
              <div class="col-md-4 border-left">
                <hr class="spacer-25">
                  <img src="/site/images/3.jpg" alt="ky" class="img-fluid">
                <p class="text-white">Contact Us</p>
              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->
          

        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-top" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          News
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">


          <div class="container">
            <div class="row">
              <div class="col-md-8">
               <span class="nav-section">News</span>
                <ul class="nav flex-column">
                    <li class="nav-item"><a class="nav-link" href="/NKY-PressReleases.php">Press Releases</a></li>
                    <li class="nav-item"><a class="nav-link" href="/NKY-AnnualReports.php">Annual Reports</a></li>
                    <li class="nav-item"><a class="nav-link" href="/NKY-KedfaMeetings.php">KEDFA Meeting News</a></li>
              </ul>
              </div>
              <!-- /.col-md-4  
              <div class="col-md-4">
                <ul class="nav flex-column">
                <li class="nav-item">
                  <a class="nav-link active" href="#">Active</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Link item</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Link item</a>
                </li>
              </ul>
              </div>
              /.col-md-4  -->
              <div class="col-md-4 border-left">
                <hr class="spacer-25">
                  <img src="/site/images/4.jpg" alt="ky" class="img-fluid">
                <p class="text-white">Contact Us</p>
              </div>
              <!-- /.col-md-4  -->
            </div>
          </div>
          <!--  /.container  -->


        </div>
      </li>


       <li class="nav-item dropdown">
        <a class="nav-link" href="/NKY-ContactUs.php" role="button">
         <i class="fa-solid fa-comment"></i>
        </a>
      

      </li>

    </ul>
  </div>
</nav>

