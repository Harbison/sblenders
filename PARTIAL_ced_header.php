
<head>
    <meta charset="utf-8">
    <title>
        <?php if (!empty($ced_title)) { echo $ced_title; } else {echo 'CED'; } ?>
    </title>
    <meta name="description">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="apple-touch-icon" href="//ced.ky.gov/Webfiles/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/x-icon" href="//ced.ky.gov/Webfiles/favicon.ico">
    <meta property="og:site_name" content="Team Kentucky | Cabinet for Economic Development">
    <meta property="og:title" content="Team Kentucky">
    <script async="" src="//cse.google.com/adsense/search/async-ads.js"></script>
    <script type="text/javascript" async="" src="https://www.googleadservices.com/pagead/conversion_async.js"></script>
    <script type="text/javascript" async="" src="https://ssl.google-analytics.com/ga.js"></script>
    <script src="https://connect.facebook.net/signals/config/1494034997417289?v=2.9.33&amp;r=stable" async=""></script>
    <script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script>
    <script type="text/javascript" async="" src="https://snap.licdn.com/li.lms-analytics/insight.min.js"></script>
    <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-5TPTPVL"></script>
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?04022019">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?04022019">

    <!--Bootstrap-->
    <link rel="stylesheet" href="site/lib/bootstrap/dist/css/bootstrap.min.css" />

    <link rel="stylesheet" href="site/css/site.min.css" />


    <!--Fonts-->
    <script src="https://use.typekit.net/fun2svc.js"></script>
    <style type="text/css">
        .tk-museo-sans {
            font-family: "museo-sans",sans-serif;
        }

        .tk-museo-sans-rounded {
            font-family: "museo-sans-rounded",sans-serif;
        }
    </style>

    <style type="text/css">
        @font-face {
            font-family: tk-museo-sans-n3;
            src: url(https://use.typekit.net/af/620bf8/00000000000000000000e7fe/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff2"),url(https://use.typekit.net/af/620bf8/00000000000000000000e7fe/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff"),url(https://use.typekit.net/af/620bf8/00000000000000000000e7fe/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("opentype");
            font-weight: 300;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: tk-museo-sans-n5;
            src: url(https://use.typekit.net/af/a28b50/00000000000000000000e803/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/a28b50/00000000000000000000e803/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/a28b50/00000000000000000000e803/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");
            font-weight: 500;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: tk-museo-sans-n7;
            src: url(https://use.typekit.net/af/e3ca36/00000000000000000000e805/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/e3ca36/00000000000000000000e805/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/e3ca36/00000000000000000000e805/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("opentype");
            font-weight: 700;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: tk-museo-sans-rounded-n3;
            src: url(https://use.typekit.net/af/491586/00000000000000003b9b1e2d/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff2"),url(https://use.typekit.net/af/491586/00000000000000003b9b1e2d/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff"),url(https://use.typekit.net/af/491586/00000000000000003b9b1e2d/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("opentype");
            font-weight: 300;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: tk-museo-sans-rounded-n7;
            src: url(https://use.typekit.net/af/9baf4a/00000000000000003b9b1e2f/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/9baf4a/00000000000000003b9b1e2f/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/9baf4a/00000000000000003b9b1e2f/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("opentype");
            font-weight: 700;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: tk-museo-sans-rounded-n5;
            src: url(https://use.typekit.net/af/a03e49/00000000000000003b9b1e2e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/a03e49/00000000000000003b9b1e2e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/a03e49/00000000000000003b9b1e2e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");
            font-weight: 500;
            font-style: normal;
            font-display: auto;
        }
    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <script>try { Typekit.load({ async: true }); } catch (e) { }</script>


    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500&amp;display=swap" rel="stylesheet">
 

 


   
    <!-- End Google Tag Manager -->
    <style type="text/css">
        @font-face {
            font-family: museo-sans;
            src: url(https://use.typekit.net/af/620bf8/00000000000000000000e7fe/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff2"),url(https://use.typekit.net/af/620bf8/00000000000000000000e7fe/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff"),url(https://use.typekit.net/af/620bf8/00000000000000000000e7fe/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("opentype");
            font-weight: 300;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: museo-sans;
            src: url(https://use.typekit.net/af/a28b50/00000000000000000000e803/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/a28b50/00000000000000000000e803/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/a28b50/00000000000000000000e803/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");
            font-weight: 500;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: museo-sans;
            src: url(https://use.typekit.net/af/e3ca36/00000000000000000000e805/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/e3ca36/00000000000000000000e805/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/e3ca36/00000000000000000000e805/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("opentype");
            font-weight: 700;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: museo-sans-rounded;
            src: url(https://use.typekit.net/af/491586/00000000000000003b9b1e2d/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff2"),url(https://use.typekit.net/af/491586/00000000000000003b9b1e2d/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("woff"),url(https://use.typekit.net/af/491586/00000000000000003b9b1e2d/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n3&v=3) format("opentype");
            font-weight: 300;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: museo-sans-rounded;
            src: url(https://use.typekit.net/af/9baf4a/00000000000000003b9b1e2f/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/9baf4a/00000000000000003b9b1e2f/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/9baf4a/00000000000000003b9b1e2f/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("opentype");
            font-weight: 700;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: museo-sans-rounded;
            src: url(https://use.typekit.net/af/a03e49/00000000000000003b9b1e2e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff2"),url(https://use.typekit.net/af/a03e49/00000000000000003b9b1e2e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("woff"),url(https://use.typekit.net/af/a03e49/00000000000000003b9b1e2e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n5&v=3) format("opentype");
            font-weight: 500;
            font-style: normal;
            font-display: auto;
        }
    </style>
    <script src="https://www.google.com/cse/static/element/323d4b81541ddb5b/cse_element__en.js?usqp=CAI%3D" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="https://www.google.com/cse/static/element/323d4b81541ddb5b/default+en.css">
    <link type="text/css" rel="stylesheet" href="https://www.google.com/cse/static/style/look/v4/default.css">
    <script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/718092536/?random=1615840057592&amp;cv=9&amp;fst=1615840057592&amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;eid=376635470&amp;u_h=1080&amp;u_w=1920&amp;u_ah=1058&amp;u_aw=1920&amp;u_cd=24&amp;u_his=3&amp;u_tz=-240&amp;u_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;gtm=2oa330&amp;sendb=1&amp;ig=1&amp;data=event%3Dgtag.config&amp;frm=0&amp;url=https%3A%2F%2Fced.ky.gov%2F&amp;ref=https%3A%2F%2Fced.ky.gov%2Fworkforce%2Fbssc.aspx&amp;tiba=Team%20Kentucky%20%7C%20Cabinet%20for%20Economic%20Development&amp;hn=www.googleadservices.com&amp;async=1&amp;rfmt=3&amp;fmt=4"></script>

</head>


<body class="home">

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TPTPVL"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="blip"></div>

    <div id="covid-19_banner">
        Visit <a href="//kycovid19.ky.gov" target="_blank"><strong>kycovid19.ky.gov</strong></a> for the latest information on the novel coronavirus in Kentucky.
    </div>

    <div id="searchEngine">
        <div id="seClose">
            <div></div>
            <div></div>
        </div>
        <script async src="https://cse.google.com/cse.js?cx=010336175831918163413:jgf0ftg7id0"></script>
        <div class="gcse-search"></div>
    </div>

    <!--Menu-->
    <div id="menu">
    <nav id="mainNav">
        <ul>

                <li class="topItem hasSub">
                    <a>Locating Expanding</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/Locating_Expanding" title="How we help">How We Help</a></li>
                        <li class="subItem"><a href="/Locating_Expanding/Why_Kentucky" title="Why kentucky">Why Kentucky</a></li>
                        <li class="subItem"><a href="/Locating_Expanding/Business_Costs" title="business Costs">Business Costs</a></li>
                        <li class="subItem"><a href="/Locating_Expanding/Available_Sites_Buildings" title="Available Sites">Available Sites/Buildings</a></li>
                        <li class="subItem"><a href="/Locating_Expanding/Financial_Incentives" title="Financial Incentives">Financial Incentives</a></li>
                        <li class="subItem"><a href="/Locating_Expanding/Living_In_Kentucky" title="Living in KY">Living In Kentucky</a></li>
                        <li class="subItem"><a href="/Locating_Expanding/Community_Profiles" title="Community profiles">Community Profiles</a></li>
                        <li class="subItem"><a href="/Workforce" title="Work force">Workforce</a></li>
                    </ul>
                </li>
                <li class="topItem hasSub">
                    <a>Existing Industry</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/Existing_Industries/">Overview</a></li>
                        <li class="subItem"><a href="/Existing_Industries/Major_Industries">Major Industries</a></li>
                        <li class="subItem"><a href="/KYFacts/Kentucky_Facilities">Kentucky Facilities</a></li>
                    </ul>
                </li>
                <li class="topItem hasSub">
                    <a>Entrepreneurship</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/Entrepreneurship">Overview</a></li>
                        <li class="subItem"><a href="/Entrepreneurship/Access_To_Capital">Access to Capital</a></li>
                        <li class="subItem"><a href="/Entrepreneurship/Expanding_Your_Market">Expanding Your Market</a></li>
                        <li class="subItem"><a href="/Entrepreneurship/Advocacy">Business Advocacy</a></li>
                        <li class="subItem"><a href="/Entrepreneurship/Investor_Information">Investor Information</a></li>
                        <li class="subItem"><a href="/Entrepreneurship/Accelerators_Incubators">Accelerators &amp; Incubators</a></li>
                    </ul>
                </li>
                <li class="topItem hasSub">
                    <a>Workforce</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/Workforce">Overview</a></li>
                        <li class="subItem"><a href="/Workforce/KYFAME">KY FAME</a></li>
                        <li class="subItem"><a href="/Workforce/Work_Ready_Communities">Work Ready Communities</a></li>
                        <li class="subItem"><a href="/Workforce/Workforce_Data">Workforce Data</a></li>
                        <li class="subItem"><a href="/Workforce/Workforce_Resources">Workforce Resources</a></li>
                        <li class="subItem"><a href="/Workforce/BSSC">BSSC</a></li>
                    </ul>
                </li>
                <li class="topItem hasSub">
                    <a>International</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/International">Overview</a></li>
                        <li class="subItem"><a href="/International/International_Facilities">International Facilities</a></li>
                        <li class="subItem"><a href="/Locating_Expanding">How We Help</a></li>
                        <li class="subItem"><a href="/Home/International_Offices">International Offices</a></li>
                        <li class="subItem"><a href="/International/Exports">Exports</a></li>
                    </ul>
                </li>
                <li class="topItem hasSub">
                    <a>KY Facts</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/KYFacts">Overview</a></li>
                        <li class="subItem"><a href="/KYFacts/About_Kentucky">About Kentucky</a></li>
                        <li class="subItem"><a href="/KYFacts/Kentucky_Facilities">Kentucky Facilities</a></li>
                        <li class="subItem"><a href="/KYFacts/Infrastructure_Utilities">Infrastructure &amp; Utilities</a></li>
                        <li class="subItem"><a href="/Home/COVID_19_Resources">COVID 19 Resources</a></li>
                    </ul>
                </li>
                <li class="topItem hasSub">
                    <a>Newsroom</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/Newsroom/News_Releases">News Releases</a></li>
                        <li class="subItem"><a href="/Newsroom/Newsletters">Newsletters</a></li>
                        <li class="subItem"><a href="/Newsroom/Publications">Publications</a></li>
                        <li class="subItem"><a href="/Newsroom/KEDFA_Meeting_Approvals">KEDFA Meeting News</a></li>
                        <li class="subItem"><a href="/newsroom/Annual_Reports">Annual Reports</a></li>
                        <li class="subItem"><a href="/newsroom/Articles">Articles</a></li>
                        <li class="subItem"><a href="/newsroom/Webinars">Webinars</a></li>
                        <li class="subItem"><a href="/Home/Transparency">Kentucky Economic Development Partnership Board</a></li>
                    </ul>
                </li>
                <li class="topItem hasSub">
                    <a>About the Cabinet</a>
                    <ul class="subMenu">
                        <li class="subItem"><a href="/Home/AboutUs">About Us</a></li>
                        <li class="subItem"><a href="/Home/Staff">Our Staff</a></li>
                        <li class="subItem"><a href="/Home/International_Offices">International Offices</a></li>
                        <li class="subItem"><a href="/Home/Partners">Partners</a></li>
                        <li class="subItem"><a href="/Home/BoardsCommissions">Boards &amp; Commissions</a></li>
                        <li class="subItem"><a href="/Home/Employment">Employment</a></li>
                        <li class="subItem"><a href="/Home/Transparency">Transparency</a></li>
                        <li class="subItem"><a href="/Home/Accessibility">Accessibility</a></li>
                        <li class="subItem"><a href="/Home/ContactUs">Contact Us</a></li>
                    </ul>
                </li>

            </ul>
    </nav>
</div>
    <!--#menu-->


    <!--Quick Contact-->
    <div id="quickContact">
        <div id="qcClose">
            <div></div>
            <div></div>
        </div>

        <!-- Quick Contact Load -->
        <div id="quickContactStaffer" class="row">

		<div class="col-6">
			<img class="fullWidth" src="https://ced.ky.gov/Webfiles/imgs/staff/JeffTaylor.jpg" alt="Jeff Taylor">
		</div>
		<div class="col-6 padding-l-0">
			<strong>Jeff Taylor</strong>
            <div class="qcInfo">
                Commissioner<br>
                Business Development<br><br>
                <strong>Kentucky Cabinet for Economic Development</strong><br>
                <span class="phone" data-label=": Quick Contact (1-800)"> 800.626.2930 </span><br />
                <span class="phone" data-label=": Quick Contact (502)">502.564.7670 </span><br />
                <a class="txt-sm track" data-label=": Quick Contact (jeff-email) href="mailto:jeff.taylor@ky.gov">Jeff.Taylor@ky.gov</a>
            </div>
		</div>
</div>

        <hr class="spacer-30">
        <div id="qcContactForm" class="contactForm">

        </div>
    </div><!--#quickContact-->
    <!--Main-->
    <div id="main">
        <div id="topBar">
            <div class="container-fluid">
                <div class="row">
                    <div id="navIconContainer" class="col-3 position-relative">
                        <div id="navIcon">
                            <div></div>
                            <div></div>
                            <div></div>
                            <span class="navTxt">Menu</span>
                        </div>
                    </div>
                    <div class="col-6">
                            <a id="logo" href="/"></a>
                    </div>
                    <div class="col-3 text-right">
                        <div id="searchIcon"></div>
                        <a id="quickContactBtn" class="btn track" data-label="Do Business in KY (wideLayout) ced.ky.gov/LP/electric_vehicle">Do Business in KY</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>

