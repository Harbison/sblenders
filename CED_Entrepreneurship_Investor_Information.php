<section class="col-12 col-md-9">
<h1 class="h1 clr-blue">Investor Information</h1>
<hr class="margin-40" />
<p>The Cabinet for Economic Development works to help connect investors with qualified small businesses and startups, with the goal of helping those businesses access capital that will enable them to grow.</p>
<p>If you are working with a small business or looking to invest in one, the Cabinet is here to help.</p>
<p class="note">Please note that the Cabinet does not intend or make any endorsement by the inclusion or exclusion of information from this page or site</p>
<hr class="spacer-60" />
<h2 class="clr-blue">Opportunities for Investors</h2>
<hr class="spacer-20" />
<h4>Kentucky Angel Investors Network</h4>
<hr class="spacer-10" />
<p>The Kentucky Angels bring new ventures and investors together across the Commonwealth via monthly online meetings, providing investors access to early-stage deal flow on a statewide basis. Out-of-state accredited investors are welcome too.</p>
<a class="btn sm" href="http://www.kyangels.net/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Kentucky Angel Investment Tax Credit</h4>
<hr class="spacer-10" />
<p>Offers tax credits of up to 40 percent of a qualified investment in Kentucky small businesses.</p>
<a class="btn sm" href="https://ced.ky.gov/Entrepreneurship/KAITC" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Kentucky Enterprise Fund</h4>
<hr class="spacer-10" />
<p>The Kentucky Enterprise Fund, managed by the Kentucky Science and Technology Corporation, provides pre-seed and seed stage investments to Kentucky based, scaleable startups. The fund looks for companies engaged in commercialization of R&amp;D, applying new technology or with the potential to accelerate technology innovation, improve competitiveness or spur significant economic growth and return on investment.</p>
<a class="btn sm" href="https://www.keyhorse.vc/kentucky-enterprise-fund" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Commonwealth Seed Capital</h4>
<hr class="spacer-10" />
<p>An independent, non-profit fund that invests in early-stage Kentucky businesses to facilitate the commercialization of innovative ideas and technologies.</p>
<a class="btn sm" href="https://ced.ky.gov/Entrepreneurship/Commonwealth_Seed" target="_blank" rel="noopener">Learn More</a><hr class="spacer-100" />
<h4>Kentucky Opportunity Zones</h4>
<hr class="spacer-10" />
<p>With 144 Opportunity Zones in 84 counties, Kentucky offers a wealth of opportunities. Follow the link below to learn more about Kentucky Opportunity Zones.</p>
<a class="btn sm" href="http://kyoz.cedky.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-100" />
<h2 class="clr-blue">Angel Investors</h2>
<hr class="spacer-20" />
<p>The term &ldquo;angel investor&rdquo; originated on Broadway for those individuals that funded productions. Today, hundreds of thousands of angel investors provide backing for entrepreneurs and small businesses in a wide variety of sectors. Kentucky is home to many of these angel investment groups. If you are interested in learning more about joining a Kentucky-based angel investor group, please contact these groups directly.<br /><br />The Cabinet makes no recommendations on any venture capital groups. The following is provided for informational purposes only.</p>
<hr class="spacer-60" />
<h4>Bluegrass Angels</h4>
<hr class="spacer-10" />
<p>The Bluegrass Angels seek to encourage the development of start-up companies and to support new businesses in the region by providing the region&rsquo;s entrepreneurs with seed capital and management guidance from experienced industry and entrepreneurial veterans.</p>
<a class="btn sm" href="http://bluegrassangels.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Cherub Fund</h4>
<hr class="spacer-10" />
<p>The cherub fund was formed in 2013 in order to grow the startup community in kentucky. The fund is comprised of entrepreneurs, angel investors and supporters of entrepreneurs.</p>
<a class="btn sm" href="http://www.cherubfund.org/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Enterprise Angels</h4>
<hr class="spacer-10" />
<p>The Enterprise Angels are a formal group of 40+ angel investors who seek to encourage the development of start-up companies and to support new businesses in the Louisville area by providing entrepreneurs with seed capital and management guidance from industry and entrepreneurial veterans.</p>
<a class="btn sm" href="https://www.greaterlouisville.com/EnterpriseCORP/Clients/EnterpriseAngels/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Tri State Angel Investment Group</h4>
<hr class="spacer-10" />
<p>Tri State Angel Investment Group&rsquo;s focus is to make the majority of its investments within the Kentucky-Ohio-West Virginia tri-state region, provided sufficient quality of investment opportunities are available.</p>
<hr class="spacer-20" />
<p class="note">Interested Kentucky-based venture capital groups may be added to this list by contacting <a href="mailto:econdev@ky.gov">econdev@ky.gov</a>.</p>
<hr class="spacer-100" />
<h1 class="h1 clr-blue">Impact Investing</h1>
<hr class="margin-40" />
<p>Listed below are names of various groups headquartered in Kentucky with a mission to create social and environmental impact, in addition to monetary returns. The Cabinet makes no recommendations on any venture capital groups. The following is provided for informational purposes only.</p>
<h4>Access Ventures</h4>
<hr class="spacer-10" />
<p>Access Ventures seeks organizations that share our desire to see people and communities flourish and operate businesses that are economically viable.</p>
<a class="btn sm" href="http://www.accessventures.org" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Appalachian Impact Fund</h4>
<hr class="spacer-10" />
<p>Seeks to advance opportunity in Eastern Kentucky through an approach that blends capacity building grantmaking with creative investment capital.</p>
<a class="btn sm" href="https://www.appalachianky.org/appalachian-impact-fund/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-10" />
<h4>Sheltowee</h4>
<hr class="spacer-10" />
<p>Sheltowee aims to create exceptional returns by making critical connections between communities, entrepreneurs, and investors providing the resources required to drive impactful products and services into market.</p>
<a class="btn sm" href="http://www.sheltowee.vc" target="_blank" rel="noopener">Learn More</a><hr class="spacer-100" />
<h1 class="clr-blue">Venture Capital</h1>
<hr class="spacer-20" />
<p>Listed below are the names of various venture capital groups in Kentucky. The Cabinet makes no recommendations on any venture capital groups. The following is provided for informational purposes only.</p>
<hr class="spacer-60" />
<h4>6ixth Event</h4>
<a class="btn sm" href="http://www.go6ixthevent.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Advantage Capital</h4>
<hr class="spacer-10" />
<p>Advantage Capital is an investment firm that provides equity and debt capital along with strategic and operational counsel. The firm prefers to invest in the communication, business service, information technology, life science and energy sectors. It was founded in 1992 in St. Louis, Mo.</p>
<a class="btn sm" href="http://www.advantagecap.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Chrysalis Ventures</h4>
<hr class="spacer-10" />
<p>Headquartered in Louisville, Kentucky, Chrysalis seeks to partner with entrepreneurs to build enduring businesses in industries undergoing significant transformation.</p>
<a class="btn sm" href="http://www.chrysalisventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Connetic Ventures</h4>
<a class="btn sm" href="http://www.connetic.ventures" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Crimson Hill</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.crimsonhillllc.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Kentucky Highlands Investment</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.khic.org" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Lunsford Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.lunsfordcapital.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Marshall Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://marshallventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Meritus Ventures</h4>
<hr class="spacer-10" />
<p>Meritus is a venture capital fund formed to make equity investments in private, expansion-stage companies in predominantly rural areas in central and southern Appalachia.</p>
<a class="btn sm" href="http://www.meritusventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Narwhal Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://www.gonarwhalventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Poplar Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://poplarventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Radicle Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://www.radiclecapital.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Render Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://render.capital/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" /><hr class="spacer-60" />
<h4>River Cities Capital Funds</h4>
<hr class="spacer-10" />
<p>River Cities seeks to invest in high-growth IT and healthcare companies that have a market-validated proposition.</p>
<a class="btn sm" href="http://rccf.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-20" />
<p class="note">Interested Kentucky-based venture capital groups may be added to this list by contacting <a href="mailto:econdev@ky.gov">econdev@ky.gov</a>.</p>
<hr class="spacer-100" />
<h1 class="clr-blue">Private Equity</h1>
<hr class="spacer-20" />
<p>The following firms, located in Kentucky, provide private equity opportunities for small businesses and entrepreneurs. The Cabinet makes no recommendations on any private equity groups. The following is provided for informational purposes only.</p>
<hr class="spacer-60" />
<h4>Blue Equity</h4>
<hr class="spacer-10" />
<p>Blue Equity, LLC is an independent, private equity firm that invests in enterprises with solid developmental potential. Blue Equity forms partnerships with existing management teams to leverage the collective expertise of all involved.</p>
<a class="btn sm" href="http://blueequity.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Global Equity Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.globalequityvc.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>MiddleGround Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://middlegroundcapital.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Newfield Capital Partners</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://newfieldcap.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Prather Capital Partners</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://prathercapital.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>River Hill Capital</h4>
<hr class="spacer-10" />
<p>River Hill Capital, LLC was organized in June 1996 to develop, establish or expand strategic and managerial relationships with a select group of privately held or small publicly held companies.</p>
<p><a class="btn sm" href="http://www.riverhillcapital.com/" target="_blank" rel="noopener">Learn More</a></p>
<hr class="spacer-20" />
<p class="note">Interested Kentucky-based private equity groups may be added to this list by contacting <a href="mailto:econdev@ky.gov">econdev@ky.gov</a>.</p>
<hr class="spacer-100" />
<h1 class="clr-blue">EB-5 Immigrant Investor Information</h1>
<hr class="spacer-20" />
<p>This is a federal program intended to stimulate the U.S. economy through job creation and capital investment by foreign investors. The program sets aside EB-5 visas for participants who invest in approved commercial enterprises based on proposals for promoting economic growth. Under this program, entrepreneurs (and their spouses and unmarried children under 21) are eligible to apply for a green card (permanent residence) if they:</p>
<ul>
<li>Make the necessary investment in a commercial enterprise in the United States; and</li>
<li>Plan to create or preserve 10 permanent full-time jobs for qualified U.S. workers.</li>
</ul>
<a class="btn sm" href="https://ced.ky.gov/Entrepreneurship/EB_5" target="_blank" rel="noopener"> Learn about EB-5 designation in Kentucky</a><hr class="spacer-60" />
<h4>Bluegrass International Fund</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.bluegrass-fund.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" /></section><section class="col-12 col-md-9">
<h1 class="h1 clr-blue">Investor Information</h1>
<hr class="margin-40" />
<p>The Cabinet for Economic Development works to help connect investors with qualified small businesses and startups, with the goal of helping those businesses access capital that will enable them to grow.</p>
<p>If you are working with a small business or looking to invest in one, the Cabinet is here to help.</p>
<p class="note">Please note that the Cabinet does not intend or make any endorsement by the inclusion or exclusion of information from this page or site</p>
<hr class="spacer-60" />
<h2 class="clr-blue">Opportunities for Investors</h2>
<hr class="spacer-20" />
<h4>Kentucky Angel Investors Network</h4>
<hr class="spacer-10" />
<p>The Kentucky Angels bring new ventures and investors together across the Commonwealth via monthly online meetings, providing investors access to early-stage deal flow on a statewide basis. Out-of-state accredited investors are welcome too.</p>
<a class="btn sm" href="http://www.kyangels.net/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Kentucky Angel Investment Tax Credit</h4>
<hr class="spacer-10" />
<p>Offers tax credits of up to 40 percent of a qualified investment in Kentucky small businesses.</p>
<a class="btn sm" href="https://ced.ky.gov/Entrepreneurship/KAITC" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Kentucky Enterprise Fund</h4>
<hr class="spacer-10" />
<p>The Kentucky Enterprise Fund, managed by the Kentucky Science and Technology Corporation, provides pre-seed and seed stage investments to Kentucky based, scaleable startups. The fund looks for companies engaged in commercialization of R&amp;D, applying new technology or with the potential to accelerate technology innovation, improve competitiveness or spur significant economic growth and return on investment.</p>
<a class="btn sm" href="https://www.keyhorse.vc/kentucky-enterprise-fund" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Commonwealth Seed Capital</h4>
<hr class="spacer-10" />
<p>An independent, non-profit fund that invests in early-stage Kentucky businesses to facilitate the commercialization of innovative ideas and technologies.</p>
<a class="btn sm" href="https://ced.ky.gov/Entrepreneurship/Commonwealth_Seed" target="_blank" rel="noopener">Learn More</a><hr class="spacer-100" />
<h4>Kentucky Opportunity Zones</h4>
<hr class="spacer-10" />
<p>With 144 Opportunity Zones in 84 counties, Kentucky offers a wealth of opportunities. Follow the link below to learn more about Kentucky Opportunity Zones.</p>
<a class="btn sm" href="http://kyoz.cedky.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-100" />
<h2 class="clr-blue">Angel Investors</h2>
<hr class="spacer-20" />
<p>The term &ldquo;angel investor&rdquo; originated on Broadway for those individuals that funded productions. Today, hundreds of thousands of angel investors provide backing for entrepreneurs and small businesses in a wide variety of sectors. Kentucky is home to many of these angel investment groups. If you are interested in learning more about joining a Kentucky-based angel investor group, please contact these groups directly.<br /><br />The Cabinet makes no recommendations on any venture capital groups. The following is provided for informational purposes only.</p>
<hr class="spacer-60" />
<h4>Bluegrass Angels</h4>
<hr class="spacer-10" />
<p>The Bluegrass Angels seek to encourage the development of start-up companies and to support new businesses in the region by providing the region&rsquo;s entrepreneurs with seed capital and management guidance from experienced industry and entrepreneurial veterans.</p>
<a class="btn sm" href="http://bluegrassangels.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Cherub Fund</h4>
<hr class="spacer-10" />
<p>The cherub fund was formed in 2013 in order to grow the startup community in kentucky. The fund is comprised of entrepreneurs, angel investors and supporters of entrepreneurs.</p>
<a class="btn sm" href="http://www.cherubfund.org/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Enterprise Angels</h4>
<hr class="spacer-10" />
<p>The Enterprise Angels are a formal group of 40+ angel investors who seek to encourage the development of start-up companies and to support new businesses in the Louisville area by providing entrepreneurs with seed capital and management guidance from industry and entrepreneurial veterans.</p>
<a class="btn sm" href="https://www.greaterlouisville.com/EnterpriseCORP/Clients/EnterpriseAngels/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Tri State Angel Investment Group</h4>
<hr class="spacer-10" />
<p>Tri State Angel Investment Group&rsquo;s focus is to make the majority of its investments within the Kentucky-Ohio-West Virginia tri-state region, provided sufficient quality of investment opportunities are available.</p>
<hr class="spacer-20" />
<p class="note">Interested Kentucky-based venture capital groups may be added to this list by contacting <a href="mailto:econdev@ky.gov">econdev@ky.gov</a>.</p>
<hr class="spacer-100" />
<h1 class="h1 clr-blue">Impact Investing</h1>
<hr class="margin-40" />
<p>Listed below are names of various groups headquartered in Kentucky with a mission to create social and environmental impact, in addition to monetary returns. The Cabinet makes no recommendations on any venture capital groups. The following is provided for informational purposes only.</p>
<h4>Access Ventures</h4>
<hr class="spacer-10" />
<p>Access Ventures seeks organizations that share our desire to see people and communities flourish and operate businesses that are economically viable.</p>
<a class="btn sm" href="http://www.accessventures.org" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Appalachian Impact Fund</h4>
<hr class="spacer-10" />
<p>Seeks to advance opportunity in Eastern Kentucky through an approach that blends capacity building grantmaking with creative investment capital.</p>
<a class="btn sm" href="https://www.appalachianky.org/appalachian-impact-fund/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-10" />
<h4>Sheltowee</h4>
<hr class="spacer-10" />
<p>Sheltowee aims to create exceptional returns by making critical connections between communities, entrepreneurs, and investors providing the resources required to drive impactful products and services into market.</p>
<a class="btn sm" href="http://www.sheltowee.vc" target="_blank" rel="noopener">Learn More</a><hr class="spacer-100" />
<h1 class="clr-blue">Venture Capital</h1>
<hr class="spacer-20" />
<p>Listed below are the names of various venture capital groups in Kentucky. The Cabinet makes no recommendations on any venture capital groups. The following is provided for informational purposes only.</p>
<hr class="spacer-60" />
<h4>6ixth Event</h4>
<a class="btn sm" href="http://www.go6ixthevent.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Advantage Capital</h4>
<hr class="spacer-10" />
<p>Advantage Capital is an investment firm that provides equity and debt capital along with strategic and operational counsel. The firm prefers to invest in the communication, business service, information technology, life science and energy sectors. It was founded in 1992 in St. Louis, Mo.</p>
<a class="btn sm" href="http://www.advantagecap.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Chrysalis Ventures</h4>
<hr class="spacer-10" />
<p>Headquartered in Louisville, Kentucky, Chrysalis seeks to partner with entrepreneurs to build enduring businesses in industries undergoing significant transformation.</p>
<a class="btn sm" href="http://www.chrysalisventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Connetic Ventures</h4>
<a class="btn sm" href="http://www.connetic.ventures" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Crimson Hill</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.crimsonhillllc.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Kentucky Highlands Investment</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.khic.org" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Lunsford Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.lunsfordcapital.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Marshall Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://marshallventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Meritus Ventures</h4>
<hr class="spacer-10" />
<p>Meritus is a venture capital fund formed to make equity investments in private, expansion-stage companies in predominantly rural areas in central and southern Appalachia.</p>
<a class="btn sm" href="http://www.meritusventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Narwhal Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://www.gonarwhalventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Poplar Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://poplarventures.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Radicle Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://www.radiclecapital.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Render Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://render.capital/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" /><hr class="spacer-60" />
<h4>River Cities Capital Funds</h4>
<hr class="spacer-10" />
<p>River Cities seeks to invest in high-growth IT and healthcare companies that have a market-validated proposition.</p>
<a class="btn sm" href="http://rccf.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-20" />
<p class="note">Interested Kentucky-based venture capital groups may be added to this list by contacting <a href="mailto:econdev@ky.gov">econdev@ky.gov</a>.</p>
<hr class="spacer-100" />
<h1 class="clr-blue">Private Equity</h1>
<hr class="spacer-20" />
<p>The following firms, located in Kentucky, provide private equity opportunities for small businesses and entrepreneurs. The Cabinet makes no recommendations on any private equity groups. The following is provided for informational purposes only.</p>
<hr class="spacer-60" />
<h4>Blue Equity</h4>
<hr class="spacer-10" />
<p>Blue Equity, LLC is an independent, private equity firm that invests in enterprises with solid developmental potential. Blue Equity forms partnerships with existing management teams to leverage the collective expertise of all involved.</p>
<a class="btn sm" href="http://blueequity.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Global Equity Ventures</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.globalequityvc.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>MiddleGround Capital</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://middlegroundcapital.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Newfield Capital Partners</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://newfieldcap.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>Prather Capital Partners</h4>
<hr class="spacer-10" /><a class="btn sm" href="https://prathercapital.com/" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" />
<h4>River Hill Capital</h4>
<hr class="spacer-10" />
<p>River Hill Capital, LLC was organized in June 1996 to develop, establish or expand strategic and managerial relationships with a select group of privately held or small publicly held companies.</p>
<p><a class="btn sm" href="http://www.riverhillcapital.com/" target="_blank" rel="noopener">Learn More</a></p>
<hr class="spacer-20" />
<p class="note">Interested Kentucky-based private equity groups may be added to this list by contacting <a href="mailto:econdev@ky.gov">econdev@ky.gov</a>.</p>
<hr class="spacer-100" />
<h1 class="clr-blue">EB-5 Immigrant Investor Information</h1>
<hr class="spacer-20" />
<p>This is a federal program intended to stimulate the U.S. economy through job creation and capital investment by foreign investors. The program sets aside EB-5 visas for participants who invest in approved commercial enterprises based on proposals for promoting economic growth. Under this program, entrepreneurs (and their spouses and unmarried children under 21) are eligible to apply for a green card (permanent residence) if they:</p>
<ul>
<li>Make the necessary investment in a commercial enterprise in the United States; and</li>
<li>Plan to create or preserve 10 permanent full-time jobs for qualified U.S. workers.</li>
</ul>
<a class="btn sm" href="https://ced.ky.gov/Entrepreneurship/EB_5" target="_blank" rel="noopener"> Learn about EB-5 designation in Kentucky</a><hr class="spacer-60" />
<h4>Bluegrass International Fund</h4>
<hr class="spacer-10" /><a class="btn sm" href="http://www.bluegrass-fund.com" target="_blank" rel="noopener">Learn More</a><hr class="spacer-60" /></section>