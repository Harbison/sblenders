<?php 
$title = "Life in Kentucky | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue"> Life in Kentucky</h1>
                <p class="text-blue">
                <span class="med-text">
                Kentucky is one of the most affordable places to call home in the U.S. <b>with cost of living 11.1% below the national average</b>. From groceries, taxes and utility costs, your money goes farther in your new Kentucky home.
                </span>
           
            </div>
        </div>
    </div>
</section>



<!-- INKY -->
<section class="container-fluid dark_blue_band py-4">
    <div class="dark_blue_band">
	    <div class="row text-center">
	    	<div class="col">
	    		<span class="db-num-big full"> Top 20 </span> 
	    		<span class="db-num-med full">housing</span>
	    		<img class="db-img" src="/site/images/house.jpg">
	   		</div>
	   		<div class="col">
	    		<span class="db-num-big full">3rd Nationally</span> 
	    		<span class="db-num-med full">utility affordability</span>
	    		<img class="db-img" src="/site/images/afford.jpg">
	   		</div>
	   		<div class="col">
	    		<span class="db-num-big full"> Top 10 </span> 
	    		<span class="db-num-med  full">childcare costs</span>
	    		<img class="db-img" src="/site/images/child.jpg">
	   		</div>
	   		<div class="col">
	    		<span class="db-num-big full"> Top 10 </span> 
	    		<span class="db-num-med full">safest states</span>
	    		<img class="db-img" src="/site/images/safe.jpg">
	   		</div>
	    </div>
 </section>

<!-- LIFE COMMUNITY IMAGES -->
<section class="container mt-3">
	    <div class="row text-center">

	    	<div class="col-lg-4 mb-3">
	    		<img class="img-fluid" src="/site/images/henderson.jpg">
	    		Henderson, Kentucky
	    	</div>
	    	<div class="col-lg-4 mb-3">
	    		<img  class="img-fluid" src="/site/images/lexington.jpg">
	    		Lexington, Kentucky
	    	</div>
	    	<div class="col-lg-4 mb-3">
	    		<img class="img-fluid"  src="/site/images/ashland.jpg">
	    		Ashland, Kentucky
	    	</div>
	    </div>
</section>

<!-- quote for life -->
<section class="container mt-5">
	    <div class="row text-center">
	    	<div class="col-md-1 d-md-block d-none"></div>
	    	<div class="col-2">
	    		<img class="img-fluid" src="/site/images/quote.jpg">
	    	</div>
	    	<div class="col-8">
	    		<span class="text-big text-blue">Kentucky just makes sense.  It has all the ingredients for success.&quot;</span>
	    	</div>
	    	<div class="col-md-1 d-none d-md-block"></div>
</section>


<!-- LIFE COMMUNITY IMAGES -->
<section class="container my-5 pt-3 light-gray">
	    <div class="row text-center">

	    	<div class="col-lg-4 d-lg-block d-none mb-3">
	    		<span class="db-num-big text-blue full">Lexington & Louisville</span>
	    		
	    	</div>
	    	<div class="col-lg-4 d-lg-block d-none mb-3">
	    		<span class="db-num-big text-blue full">Georgetown</span>

	    	</div>
	    	<div class="col-lg-4 d-lg-block d-none mb-3">
	    		<span class="db-num-big text-blue full">Henderson & Paducah</span>

	    	</div>
	    </div>
	    <div class="row text-center">
			<div class="col-lg-4 d-lg-block d-none  mb-3">
		    	<span class="db-num-med text-blue full">2 of the top 10 most affordable cities to buy a home</span>
		    </div>
		    <div class="col-lg-4 d-lg-block d-none  mb-3">
					<span class="db-num-med text-blue full">One of the nation's top cities to raise a family</span>
		    </div>
		    <div class="col-lg-4 d-lg-block d-none  mb-3">
		    	<span class="db-num-med text-blue full">2 of the top 20 friendliest southern towns</span>
		    </div>
		</div>
	   	<div class="row text-center">
			<div class="col-lg-4 d-lg-block d-none  mb-3">
		    	<img class="img-fluid" src="/site/images/louisville.jpg" height="300" >
		    </div>
		    <div class="col-lg-4 d-lg-block d-none  mb-3">
				<img  class="img-fluid" src="/site/images/georgetown.jpg" height="300">
		    </div>
		    <div class="col-lg-4 d-lg-block d-none  mb-3">
		    	<img class="img-fluid"  src="/site/images/paducah.jpg" height="300">
		    </div>
		</div>


		<div class="row text-center">
		    <div class="col-lg-4 d-md-none  mb-3">
				<span class="db-num-big text-blue full">Lexington & Louisville</span>
		    	<span class="db-num-med text-blue full">2 of the top 10 most affordable cities to buy a home</span>
		    	<img class="img-fluid" src="/site/images/louisville.jpg" height="300" >
		    </div>
		    <div class="col-lg-4 d-md-none  mb-3">
		    	<span class="db-num-big text-blue full">Georgetown</span>
				<span class="db-num-med text-blue full">One of the nation's top cities to raise a family</span>
				<img  class="img-fluid" src="/site/images/georgetown.jpg" height="300">
		    </div>
		    <div class="col-lg-4 d-md-none  mb-3">
		    	<span class="db-num-big text-blue full">Henderson & Paducah</span>
		    	<span class="db-num-med text-blue full">2 of the top 20 friendliest southern towns</span>
		    	<img class="img-fluid"  src="/site/images/paducah.jpg" height="300">
		    </div>
		</div>


</section>




<!-- SUPPLY CONNEX SITES -->
<section>
    <div class="container">
        <div class="row">
            <div id="lifeky" class="col-12 col-md-4" >
            	<img src="/site/images/lifeky-cir.jpg" class="img-fluid">
            </div>
            <div id="outdoor" class="col-12 col-md-4 py-3 bg-grey">
                <h2 class="new">Outdoor Adventure</h2>
                <br clear="all">
                <p>
               From east to west or north to south, there is a one-of-a-kind wonder waiting to be explored. Visit the longest cave system in the world at Mammoth Cave National Park, the wonder of the Red River Gorge or the famous moonbow at Cumberland Falls. You can also see Kentucky’s natural beauty at our 44 state parks, where families have been making memories together for 100 years.<hr class="spacer-25"> </p>
                
                <br clear="all">
                 <a class="btn new bottom" href="https://www.kentuckytourism.com/things-to-do/outdoors" target="_blank">Read More</a>
            </div>

            <div id="culture" class="col-12 col-md-4 py-3">
                <h2 class="new">Culture</h2>
         
                <br clear="all">
                <p>
                   Take a dive into our history and culture by stopping for a visit at the Muhammad Ali Center, Abraham Lincoln Birthplace National Historic Park, Loretta Lynn's Homeplace Butcher Holler, the National Corvette Museum, the Louisville Slugger Museum, Kentucky Horse Park, Churchill Downs or Keeneland.
					<hr class="spacer-25">
                </p>

                <br clear="all">
                <hr class="spacer-25">
                 <a class="btn new bottom" href="https://www.kentuckytourism.com/things-to-do/culture" target="_blank">Read More</a>
            </div>
        </div>
        <hr class="spacer-25">
        <div class="row mb-3">
             <div id="available_sites" class="col-12 col-md-6 ">
                <h2 class="new">Education</h2>
                <b>Kentucky is investing in our future through education</b><br clear="all">
                <p>
                    From our youngest learners to those ready to lead, Kentucky is making sure to
					invest in our future leaders. Kentucky boasts 8 public universities, 16 community
					and technical colleges and 18 private, non-profit colleges and universities with a
					pipeline of over 207,200 students. The state has also invested more than $245
					million in Kentucky’s career and technical high schools across the state.
					<hr class="spacer-25">
				</p>


				<br clear="all">
            </div>
            <div class="col-12 col-md-6">
                <img src="/site/images/andy-ed.jpg" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>





 <?php include('NKY-footer.php'); ?>