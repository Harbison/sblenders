


var lenders =[
   {
      "lender_name":"American Bank & Trust",
      "contact_name":"Julie Richardson, Vice President",
      "contact_email":"jrichardson@ambanking.com",
      "add_1":"1302 Scottsville Road",
      "city_state_zip":"Bowling Green, KY 42102-",
      "phone":"270-393-2410",
      "fax":"270-796-8998",
      "service_area":"Allen, Butler, Edmonson, Logan, Simpson, Warren"
   },
   {
      "lender_name":"American Founders Bank Inc",
      "contact_name":"David Verville, EVP & CCO",
      "contact_email":"dverville@afbusa.com",
      "add_1":"1200 Forest Bridge Road",
      "city_state_zip":"Louisville, KY 40223-",
      "phone":"502-638-4745",
      "fax":"502-638-4755",
      "service_area":"Bullitt, Hardin, Henry, Jefferson, Meade, Oldham, Shelby, Spencer"
   },
   {
      "lender_name":"Bank of the Bluegrass & Trust Co",
      "contact_name":"Thomas Greinke, SVP/Chief Lending Officer",
      "contact_email":"tgreinke@bankofthebluegrass.com",
      "add_1":"215 Southland Drive",
      "city_state_zip":"Lexington, KY 40503-",
      "phone":"859-685-3046",
      "fax":"859-260-2055",
      "service_area":"Bourbon, Clark, Fayette, Jessamine, Madison, Scott, Woodford"
   },
   {
      "lender_name":"Central Bank and Trust Co",
      "contact_name":"Mark Fox, Vice President",
      "contact_email":"mfox@centralbank.com",
      "add_1":"300 West Vine Street",
      "city_state_zip":"Lexington, KY 40507-",
      "phone":"859-253-6233",
      "fax":"859-253-6100",
      "service_area":"Boone, Bourbon, Boyle, Bullitt, Campbell, Clark, Fayette, Franklin, Garrard, Grant, Harrison, Jefferson, Jessamine, Kenton, Madison, Mercer, Montgomery, Oldham, Owen, Powell, Scott, Shelby, Spencer, Woodford"
   },
   {
      "lender_name":"Citizens Bank of Kentucky Inc",
      "contact_name":"Darren Gillespie, Senior Vice President & Chief Lending Officer",
      "contact_email":"darreng@wercitizens.bank",
      "add_1":"620 Broadway",
      "city_state_zip":"Paintsville, KY 41240-",
      "phone":"606-264-3048",
      "fax":"606-789-9773",
      "service_area":"Bourbon, Boyd, Breathitt, Carter, Clark, Elliott, Estill, Fayette, Floyd, Greenup, Johnson, Knott, Lawrence, Letcher, Lewis, Madison, Magoffin, Martin, Montgomery, Morgan, Perry, Pike, Powell, Rowan, Wolfe"
   },
   {
      "lender_name":"Citizens Deposit Bank",
      "contact_name":"Greg Hambrick, Loan Officer",
      "contact_email":"ghambrick@cdbky.com",
      "add_1":"941 US Highway 62",
      "city_state_zip":"Bardwell, KY 42023-",
      "phone":"270-628-9199",
      "fax":"270-628-0277",
      "service_area":"Ballard, Calloway, Carlisle, Fulton, Graves, Hickman, Marshall, McCracken"
   },
   {
      "lender_name":"Citizens First Bank",
      "contact_name":"Karisa Peterson, Barren County Community President",
      "contact_email":"kpeterson@citizensfirstbank.com",
      "add_1":"",
      "city_state_zip":"Glasgow, KY 42142-",
      "phone":"270-659-4003",
      "fax":"270-629-4889",
      "service_area":"Allen, Barren, Butler, Edmonson, Hart, Logan, Metcalfe, Monroe, Simpson, Warren"
   },
   {
      "lender_name":"Commercial Bank",
      "contact_name":"Howard Elam, Vice President",
      "contact_email":"Howard.Elam@cbwl.cc",
      "add_1":"550 Main Street",
      "city_state_zip":"West Liberty, KY 41472-",
      "phone":"606-743-3195",
      "fax":"606-743-9824",
      "service_area":"Elliott, Magoffin, Menifee, Morgan, Rowan, Wolfe"
   },
   {
      "lender_name":"Commercial Bank KY/TN",
      "contact_name":"Ryan Reece, Vice President",
      "contact_email":"rreece@cbtn.com",
      "add_1":"1285 Cumberland Gap Parkway",
      "city_state_zip":"Corbin, KY 40701-",
      "phone":"606-523-2978",
      "fax":"606-523-0623",
      "service_area":"Bell, Harlan, Knox, Laurel, Whitley"
   },
   {
      "lender_name":"Commercial Bank of Grayson",
      "contact_name":"Mark D. Strother, President & CEO",
      "contact_email":"markstrother@cbgrayson.com",
      "add_1":"208 East Main Street",
      "city_state_zip":"Grayson, KY 41143-",
      "phone":"606-474-7811",
      "fax":"606-474-2100",
      "service_area":"Boyd, Carter, Elliott, Greenup, Lawrence, Lewis, Rowan"
   },
   {
      "lender_name":"Commonwealth Credit Union",
      "contact_name":"Robert Minichan, Commercial Lending Manager",
      "contact_email":"rminichan@cwcu.org",
      "add_1":"1425 Louisville Road",
      "city_state_zip":"Frankfort, KY 40601-",
      "phone":"502-564-4775",
      "fax":"N/A",
      "service_area":"Anderson, Fayette, Franklin, Jefferson, Mercer, Scott, Shelby"
   },
   {
      "lender_name":"Community Financial Services Bank Inc",
      "contact_name":"Jesse Clark, Commercial Lender / Master Financial Coach",
      "contact_email":"jessec@cfsvcs.com",
      "add_1":"221 West 5th Street",
      "city_state_zip":"Benton, KY 42025-",
      "phone":"270-527-4603",
      "fax":"N/A",
      "service_area":"Calloway, Graves, Marshall, McCracken"
   },
   {
      "lender_name":"Community Trust Bank Inc",
      "contact_name":"Terry Spears, VP/Small Business Banking Officer",
      "contact_email":"spearste@ctbi.com",
      "add_1":"346 North Mayo Trail",
      "city_state_zip":"Pikeville, KY 41501-",
      "phone":"606-437-3216",
      "fax":"606-218-8313",
      "service_area":"All Kentucky Counties"
   },
   {
      "lender_name":"Community Ventures Corporation",
      "contact_name":"Brenda Weaver, President of Lending",
      "contact_email":"bweaver@cvky.org",
      "add_1":"1450 North Broadway",
      "city_state_zip":"Lexington, KY 40505-",
      "phone":"859-231-0054",
      "fax":"859-231-0179",
      "service_area":"All Kentucky Counties"
   },
   {
      "lender_name":"Cumberland Security Bank",
      "contact_name":"Mark A. Ross, Exec V P",
      "contact_email":"mross@csbweb.com",
      "add_1":"107 South Main Street",
      "city_state_zip":"Somerset, KY 42501-",
      "phone":"606-679-9361",
      "fax":"606-679-4167",
      "service_area":"Casey, Laurel, Lincoln, McCreary, Pulaski, Rockcastle, Russell, Wayne"
   },
   {
      "lender_name":"Cumberland Valley National Bank",
      "contact_name":"Candi Burton, AVP/Credit Risk Manager",
      "contact_email":"candiburton@cvnb.com",
      "add_1":"100 North Main Street",
      "city_state_zip":"London, KY 40741-",
      "phone":"606-878-7010",
      "fax":"606-864-7825",
      "service_area":"Bullitt, Fayette, Jefferson, Knox, Laurel, Madison, Oldham, Pulaski, Shelby, Whitley"
   },
   {
      "lender_name":"Farmers Bank and Trust Company",
      "contact_name":"Terry L. Emrick, Community President",
      "contact_email":"Terry.Emrick@farmers247.com",
      "add_1":"1555 South Green Street",
      "city_state_zip":"Henderson, KY 42420-",
      "phone":"270-827-9574",
      "fax":"270-827-9442",
      "service_area":"Caldwell, Crittenden, Livingston"
   },
   {
      "lender_name":"Farmers National Bank of Danville",
      "contact_name":"Jesse Johnson, Asst VP",
      "contact_email":"Jesse.Johnson@fnbky.com",
      "add_1":"304 West Main St",
      "city_state_zip":"Danville, KY 40422-",
      "phone":"859-238-0665",
      "fax":"859-238-9731",
      "service_area":"Boyle, Casey, Fayette, Garrard, Jessamine, Lincoln, Marion, Mercer, Washington"
   },
   {
      "lender_name":"Fifth Third Bank",
      "contact_name":"Tassie Montgomery, Officer",
      "contact_email":"tassie.montgomery@53.com",
      "add_1":"5695 Romar Drive",
      "city_state_zip":"Milford, OH 45150-",
      "phone":"513-530-0638",
      "fax":"513-324-0147",
      "service_area":"All Kentucky Counties"
   },
   {
      "lender_name":"First Community Bank of the Heartland Inc",
      "contact_name":"Greg Gunter, Exec V P",
      "contact_email":"greg.gunter@fcbheartland.com",
      "add_1":"114 East Jackson Street",
      "city_state_zip":"Clinton, KY 42031-",
      "phone":"270-653-6335",
      "fax":"270-653-2003",
      "service_area":"Ballard, Carlisle, Fulton, Graves, Hickman, Livingston, Marshall, McCracken"
   },
   {
      "lender_name":"First Financial Bank",
      "contact_name":"Jeffrey A. Magginnis, Vice President/SBA Manager",
      "contact_email":"jeff.magginnis@bankatfirst.com",
      "add_1":"3535 E 96th Street, Suite 135",
      "city_state_zip":"Indianapolis, IN 46240-",
      "phone":"317-237-1588",
      "fax":"N/A",
      "service_area":"Anderson, Bullitt, Fayette, Franklin, Henry, Jefferson, Mercer, Oldham, Owen, Scott, Shelby, Spencer, Woodford"
   },
   {
      "lender_name":"First Harrison Bank",
      "contact_name":"Stacey Bernard, Business Development Officer",
      "contact_email":"sbernard@firstharrison.com",
      "add_1":"1612 Highway 44 E",
      "city_state_zip":"Shepherdsville, KY 40165-",
      "phone":"502-548-5387",
      "fax":"502-543-3517",
      "service_area":"Bullitt, Fayette, Hardin, Jefferson, Meade, Nelson, Oldham, Shelby, Spencer"
   },
   {
      "lender_name":"First Harrison Bank",
      "contact_name":"Julayne Amstutz, Vice President",
      "contact_email":"jamstutz@firstharrison.com",
      "add_1":"3131 Grant Line Rd.",
      "city_state_zip":"New Albany, IN 47150-",
      "phone":"502-298-0465",
      "fax":"812-981-0199",
      "service_area":"Bullitt, Fayette, Hardin, Jefferson, Meade, Nelson, Oldham, Shelby, Spencer"
   },
   {
      "lender_name":"First National Bank of Grayson",
      "contact_name":"Eric Stinson, V.P. Commercial Lending",
      "contact_email":"estinson@fnbgrayson.com",
      "add_1":"",
      "city_state_zip":"Grayson, KY 41143-",
      "phone":"606-474-2000",
      "fax":"606-474-6626",
      "service_area":"Boyd, Carter, Elliott, Lewis, Magoffin, Menifee, Morgan, Rowan"
   },
   {
      "lender_name":"First National Bank of Russell Springs",
      "contact_name":"Steven Fletcher, EVP & Senior Lender",
      "contact_email":"sfletcher@fnbrs.com",
      "add_1":"36 W Steve Wariner Drive",
      "city_state_zip":"Russell Springs, KY 42642-",
      "phone":"270-866-4343",
      "fax":"270-866-4340",
      "service_area":"Adair, Casey, Pulaski, Russell"
   },
   {
      "lender_name":"First Southern National Bank",
      "contact_name":"Doug Daniel, Loan Admin. Mgr.",
      "contact_email":"ddaniel@fsnb.net",
      "add_1":"",
      "city_state_zip":"Stanford, KY 40484-",
      "phone":"606-365-6971",
      "fax":"606-365-4957",
      "service_area":"Allen, Ballard, Bourbon, Boyle, Butler, Caldwell, Carlisle, Casey, Christian, Clark, Crittenden, Edmonson, Estill, Fayette, Garrard, Graves, Hopkins, Jackson, Jessamine, Laurel, Lincoln, Logan, Lyon, Madison, McCracken, McCreary, Mercer, Muhlenberg, Pulaski, Rockcastle, Russell, Scott, Simpson, Todd, Trigg, Warren, Wayne, Woodford"
   },
   {
      "lender_name":"First United Bank and Trust",
      "contact_name":"Tom McFarland, Chief Lending Officer",
      "contact_email":"TMcFarland@efirstunitedbank.com",
      "add_1":"",
      "city_state_zip":"Madisonville, KY 42431-",
      "phone":"270-643-1686",
      "fax":"270-821-6875",
      "service_area":"Crittenden, Daviess, Henderson, Hopkins, McLean, Muhlenberg, Union, Webster"
   },
   {
      "lender_name":"FNB Bank Inc",
      "contact_name":"Ike Nichols, Commercial Lending",
      "contact_email":"Ike.Nichols@thinkfnb.com",
      "add_1":"3445 Lone Oak Road",
      "city_state_zip":"Paducah, KY 42003-",
      "phone":"270-554-1748",
      "fax":"N/A",
      "service_area":"Ballard, Calloway, Carlisle, Christian, Fulton, Graves, Hickman, Livingston, Lyon, Marshall, McCracken, Trigg"
   },
   {
      "lender_name":"Forcht Bank NA",
      "contact_name":"Michael D. Sharpe, London/Williamsburg Market President",
      "contact_email":"misharpe@forchtbank.com",
      "add_1":"",
      "city_state_zip":"London, KY 40743-",
      "phone":"606-862-5720",
      "fax":"606-877-5909",
      "service_area":"Boone, Fayette, Grant, Green, Jefferson, Knox, Laurel, Madison, McCreary, Pulaski, Taylor, Whitley"
   },
   {
      "lender_name":"Franklin Bank and Trust Company",
      "contact_name":"Kirk Pierce, Sr Vice Pres",
      "contact_email":"kirk_pierce@fbtco.com",
      "city_state_zip":"Franklin, KY",
      "phone":"270-586-2598",
      "fax":"270-586-2550",
      "service_area":"Allen, Barren, Butler, Edmonson, Logan, Simpson, Warren"
   },
   {
      "lender_name":"German American Bank",
      "contact_name":"Michael Beckwith, Divisional President",
      "contact_email":"michael.beckwith@germanamerican.com",
      "add_1":"313 Frederica Street",
      "city_state_zip":"Owensboro, KY 42301-",
      "phone":"270-663-4668",
      "fax":"N/A",
      "service_area":"Barren, Daviess, Fayette, Simpson, Warren"
   },
   {
      "lender_name":"Huntington National Bank",
      "contact_name":"Perry L. Dunn, Vice President & Sr. SBA Specialist",
      "contact_email":"Perry.L.Dunn@huntington.com",
      "add_1":"2333 Alexandria Drive",
      "city_state_zip":"Lexington, KY 40504-",
      "phone":"859-514-6022",
      "fax":"N/A",
      "service_area":"All Kentucky Counties"
   },
   {
      "lender_name":"Independence Bank",
      "contact_name":"Wayne Mattingly, Vice President, Agriculture Loan Officer",
      "contact_email":"wmattingly@1776bank.com",
      "add_1":"2425 Frederica Street",
      "city_state_zip":"Owensboro, KY 42302-",
      "phone":"270-686-1776",
      "fax":"270-686-0081",
      "service_area":"Anderson, Boyle, Clark, Daviess, Fayette, Franklin, Hancock, Henderson, Henry, Jefferson, Jessamine, Madison, McCracken, McLean, Mercer, Owen, Scott, Shelby, Warren, Webster, Woodford"
   },
   {
      "lender_name":"Kentucky Bank",
      "contact_name":"Kayla Hardy, Commercial Lending Administrative Assistant",
      "contact_email":"kayla.hardy@kybank.com",
      "add_1":"360 E Vine Street, Suite 100",
      "city_state_zip":"Lexington, KY 40507-",
      "phone":"502-469-7380",
      "fax":"859-225-0510",
      "service_area":"Bourbon, Clark, Elliott, Fayette, Franklin, Harrison, Jessamine, Madison, Rowan, Scott, Woodford"
   },
   {
      "lender_name":"Kentucky Farmers Bank",
      "contact_name":"Jeffrey D. Elswick, Sr. V.P./Chief Commercial Lender",
      "contact_email":"jelswick@kentuckyfarmersbank.com",
      "add_1":"",
      "city_state_zip":"Ashland, KY 41101-",
      "phone":"606-929-5071",
      "fax":"606-929-5196",
      "service_area":"Boyd, Carter, Greenup"
   },
   {
      "lender_name":"Kentucky Highlands Investment Corporation",
      "contact_name":"Jerry Rickett, President",
      "contact_email":"Jrickett@khic.org",
      "add_1":"",
      "city_state_zip":"London, KY 40743-",
      "phone":"606-864-5175",
      "fax":"606-864-5194",
      "service_area":"Bell, Clay, Clinton, Cumberland, Estill, Harlan, Jackson, Knox, Laurel, Lee, Leslie, Letcher, Lincoln, Madison, McCreary, Owsley, Perry, Pulaski, Rockcastle, Russell, Wayne, Whitley"
   },
   {
      "lender_name":"Monticello Banking Company",
      "contact_name":"Rodney Weaver, Sr Vice Pres",
      "contact_email":"rweaver@ebank.ky",
      "add_1":"50 North Main Street",
      "city_state_zip":"Monticello, KY 42633-",
      "phone":"606-348-8411",
      "fax":"606-348-1253",
      "service_area":"Barren, Casey, Clinton, Pulaski, Russell, Warren, Wayne"
   },
   {
      "lender_name":"Mountain Association for Community Economic Development",
      "contact_name":"Paul Wright, Enterprise Development Director",
      "contact_email":"pwright@maced.org",
      "add_1":"433 Chestnut Street",
      "city_state_zip":"Berea, KY 40403-",
      "phone":"859-986-2373",
      "fax":"859-986-1299",
      "service_area":"Adair, Bath, Bell, Boyd, Breathitt, Carter, Casey, Clark, Clay, Clinton, Cumberland, Edmonson, Elliott, Estill, Fleming, Floyd, Garrard, Green, Greenup, Harlan, Hart, Jackson, Johnson, Knott, Knox, Laurel, Lawrence, Lee, Leslie, Letcher, Lewis, Lincoln, Madison, Magoffin, Martin, McCreary, Menifee, Metcalfe, Monroe, Montgomery, Morgan, Nicholas, Owsley, Perry, Pike, Powell, Pulaski, Robertson, Rockcastle, Rowan, Russell, Wayne, Whitley, Wolfe"
   },
   {
      "lender_name":"Paducah Bank & Trust",
      "contact_name":"Tom Clayton, Loan Sales Mgr.",
      "contact_email":"tomclayton@paducahbank.com",
      "add_1":"555 Jefferson Street",
      "city_state_zip":"Paducah, KY 42002-2600",
      "phone":"270-575-5700",
      "fax":"270-575-6615",
      "service_area":"Ballard, Calloway, Carlisle, Crittenden, Graves, Livingston, Lyon, Marshall, McCracken, Trigg"
   },
   {
      "lender_name":"Republic Bank & Trust Company",
      "contact_name":"Rob Ruddick, AVP/SBA Lending Specialist",
      "contact_email":"rruddick@republicbank.com",
      "add_1":"430 Connector Road",
      "city_state_zip":"Georgetown, KY 40324-",
      "phone":"859-519-3371",
      "fax":"859-268-1623",
      "service_area":"Boone, Bullitt, Campbell, Daviess, Fayette, Franklin, Hardin, Henderson, Jefferson, Kenton, Oldham, Scott, Shelby"
   },
   {
      "lender_name":"South Central Bank",
      "contact_name":"Joshua Devore, CPA",
      "contact_email":"josh.devore@southcentralbank.com",
      "add_1":"208 South Broadway",
      "city_state_zip":"Glasgow, KY 42141-",
      "phone":"270-629-1003",
      "fax":"270-651-6580",
      "service_area":"Allen, Barren, Daviess, Edmonson, Fayette, Hardin, Hart, Jefferson, LaRue, Metcalfe, Monroe, Pulaski, Simpson, Warren"
   },
   {
      "lender_name":"Southeast Kentucky Economic Development Corporation (SKED)",
      "contact_name":"Brett Traver, Executive Director",
      "contact_email":"btraver@centertech.com",
      "add_1":"2292 S Highway 2",
      "city_state_zip":"Somerset, KY 42501-",
      "phone":"606-677-6100",
      "fax":"N/A",
      "service_area":"Adair, Bath, Bell, Breathitt, Casey, Clay, Clinton, Cumberland, Estill, Floyd, Garrard, Green, Harlan, Jackson, Jessamine, Johnson, Knott, Knox, Laurel, Lawrence, Lee, Leslie, Letcher, Lincoln, Magoffin, Martin, McCreary, Menifee, Metcalfe, Monroe, Morgan, Owsley, Perry, Pike, Pulaski, Rockcastle, Rowan, Russell, Taylor, Wayne, Whitley, Wolfe"
   },
   {
      "lender_name":"Stock Yards Bank & Trust Company",
      "contact_name":"Brett Stilwell, Relationship Manager/Commercial Lending",
      "contact_email":"Brett.Stilwell@syb.com",
      "add_1":"3230 E. 10th Street",
      "city_state_zip":"Jeffersonville, IN 47130",
      "phone":"502-625-2531",
      "fax":"502-625-0848",
      "service_area":"Boone, Bullitt, Jefferson, Kenton, Nelson, Oldham, Shelby, Spencer"
   },
   {
      "lender_name":"The Cecilian Bank",
      "contact_name":"Tracie Oliver, Loan Officer",
      "contact_email":"Toliver@cecilianbank.com",
      "add_1":"",
      "city_state_zip":"Elizabethtown, KY 42701-",
      "phone":"270-737-1593",
      "fax":"270-737-6391",
      "service_area":"Breckinridge, Grayson, Hardin, Meade"
   },
   {
      "lender_name":"The Citizens Bank",
      "contact_name":"Ryan Neff, Vice President",
      "contact_email":"ryandneff@tcbanytime.com",
      "add_1":"114 West Main Street",
      "city_state_zip":"Morehead, KY 40351-",
      "phone":"606-780-0000",
      "fax":"606-784-4616",
      "service_area":"Bath, Carter, Elliott, Fleming, Magoffin, Menifee, Montgomery, Morgan, Nicholas, Rowan"
   },
   {
      "lender_name":"Town & Country Bank and Trust Co",
      "contact_name":"Emerson Ballard,",
      "contact_email":"Emerson.Ballard@mytcbt.com",
      "add_1":"201 N Third Street",
      "city_state_zip":"Bardstown, KY 40004-",
      "phone":"502-348-3911",
      "fax":"502-348-0666",
      "service_area":"Anderson, Jessamine, Nelson"
   },
   {
      "lender_name":"Traditional Bank",
      "contact_name":"Spears Stilz, Senior V P",
      "contact_email":"spears.stilz@traditionalbank.com",
      "add_1":"49 West Main Street",
      "city_state_zip":"Mt. Sterling, KY 40353-",
      "phone":"859-497-8661",
      "fax":"859-498-0643",
      "service_area":"Bath, Bourbon, Clark, Fayette, Franklin, Harrison, Jessamine, Madison, Menifee, Montgomery, Nicholas, Rowan, Scott, Woodford"
   },
   {
      "lender_name":"United Bank & Capital Trust Company",
      "contact_name":"Zach Moore, SVP Chief Market Lender",
      "contact_email":"Zach.Moore@unitedbankky.com",
      "add_1":"125 W Main Street",
      "city_state_zip":"Frankfort, KY 40601-",
      "phone":"502-227-1600",
      "fax":"502-227-1676",
      "service_area":"Anderson, Boone, Bullitt, Campbell, Fayette, Franklin, Hardin, Jefferson, Jessamine, Kenton, Mercer, Scott, Woodford"
   },
   {
      "lender_name":"United Citizens Bank & Trust Co",
      "contact_name":"Tammy Pryor, Vice President",
      "contact_email":"tpryor@unitedcitizensbank.com",
      "add_1":"8198 Main Street",
      "city_state_zip":"Campbellsburg, KY 40011-",
      "phone":"502-532-7392",
      "fax":"502-532-6607",
      "service_area":"Carroll, Henry, Jefferson, Oldham, Owen, Shelby, Trimble"
   },
   {
      "lender_name":"United Cumberland Bank",
      "contact_name":"Michael J. Bush, Vice-President, Commercial Lending Officer",
      "contact_email":"mbush@unitedcumberland.com",
      "add_1":"47 South Main Street",
      "city_state_zip":"Whitley City, KY 42653-",
      "phone":"606-376-5031",
      "fax":"606-376-5038",
      "service_area":"Bell, Casey, Clay, Clinton, Jackson, Knox, Laurel, Lincoln, McCreary, Pulaski, Rockcastle, Russell, Wayne, Whitley"
   },
   {
      "lender_name":"United Southern Bank",
      "contact_name":"Andrew J. Wilson, Branch Manager/Loan Officer",
      "contact_email":"",
      "add_1":"4405 Canton Pike",
      "city_state_zip":"Hopkinsville, KY 42240-",
      "phone":"270-886-8580",
      "fax":"270-885-0076",
      "service_area":"Christian, Hopkins, Todd"
   },
   {
      "lender_name":"Whitaker Bank",
      "contact_name":"Rodney Williams, Area President",
      "contact_email":"rwilliams@whitakerbank.com",
      "add_1":"130 West Main Street",
      "city_state_zip":"Frankfort, KY 40601-",
      "phone":"502-875-6340",
      "fax":"N/A",
      "service_area":"Fayette, Franklin, Garrard, Jefferson, Knox, Letcher, Mercer, Montgomery, Perry, Powell, Rowan, Scott, Whitley, Wolfe"
   },
   {
      "lender_name":"Wilson & Muir Bank & Trust Co",
      "contact_name":"Mark Hardin, Loan Officer & Credit Analyst",
      "contact_email":"mhardin@wilsonmuirbank.com",
      "add_1":"107 N 3rd Street",
      "city_state_zip":"Bardstown, KY 40004-",
      "phone":"502-454-5400",
      "fax":"502-454-6863",
      "service_area":"Breckinridge, Bullitt, Butler, Edmonson, Grayson, Hardin, Hart, Jefferson, LaRue, Marion, Meade, Nelson, Ohio, Oldham, Shelby, Spencer, Washington"
   }
];

var counties = [
   'All Kentucky Counties',
   'All Lenders',
	'Adair',
	'Allen',
	'Anderson',
	'Ballard',
	'Barren',
	'Bath',
	'Bell',
	'Boone',
	'Bourbon',
	'Boyd',
	'Boyle',
	'Bracken',
	'Breathitt',
	'Breckinridge',
	'Bullitt',
	'Butler',
	'Caldwell',
	'Calloway',
	'Campbell',
	'Carlisle',
	'Carroll',
	'Carter',
	'Casey',
	'Christian',
	'Clark',
	'Clay',
	'Clinton',
	'Crittenden',
	'Cumberland',
	'Daviess',
	'Edmonson',
	'Elliott',
	'Estill',
	'Fayette',
	'Fleming',
	'Floyd',
	'Franklin',
	'Fulton',
	'Gallatin',
	'Garrard',
	'Grant',
	'Graves',
	'Grayson',
	'Green',
	'Greenup',
	'Hancock',
	'Hardin',
	'Harlan',
	'Harrison',
	'Hart',
	'Henderson',
	'Henry',
	'Hickman',
	'Hopkins',
	'Jackson',
	'Jefferson',
	'Jessamine',
	'Johnson',
	'Kenton',
	'Knott',
	'Knox',
	'LaRue',
	'Laurel',
	'Lawrence',
	'Lee',
	'Leslie',
	'Letcher',
	'Lewis',
	'Lincoln',
	'Livingston',
	'Logan',
	'Lyon',
	'Madison',
	'Magoffin',
	'Marion',
	'Marshall',
	'Martin',
	'Mason',
	'McCracken',
	'McCreary',
	'McLean',
	'Meade',
	'Menifee',
	'Mercer',
	'Metcalfe',
	'Monroe',
	'Montgomery',
	'Morgan',
	'Muhlenberg',
	'Nelson',
	'Nicholas',
	'Ohio',
	'Oldham',
	'Owen',
	'Owsley',
	'Pendleton',
	'Perry',
	'Pike',
	'Powell',
	'Pulaski',
	'Robertson',
	'Rockcastle',
	'Rowan',
	'Russell',
	'Scott',
	'Shelby',
	'Simpson',
	'Spencer',
	'Taylor',
	'Todd',
	'Trigg',
	'Trimble',
	'Union',
	'Warren',
	'Washington',
	'Wayne',
	'Webster',
	'Whitley',
	'Wolfe',
	'Woodford'
	];


const FILTER_ALL = 'all';


// var load = [
//    {'poop': 'so this is how you want it', 'am': 2},
//    {'poop': 'soupy mix of potatoes', 'am': 3}, 
//    {'poop': 'dupe', 'am': 2},
//    {'poop': 'glue and soup', 'am': 5}, 
//    {'poop': 'blue', 'am':'stew'}
//    ];
   
//    var need = ['mix'];

//    rtest = load.filter(function(v) {
//       return v.poop.indexOf(need) !== -1;
//    });

// console.log(rtest);



new Vue({
 el: '#app2',

 data() {
   return {
     lenders: lenders,
     counties: counties,
     selectedType: FILTER_ALL
   }
 },

 computed: {
   filteredLenders() {
     if (this.selectedType === FILTER_ALL) {
         return this.lenders;
     } else if (this.selectedType === 'All Lenders') {
         return lenders;
     } else {
         var needle = this.selectedType;
         var filterLenders = lenders.filter(function(lender) {
               return lender.service_area.indexOf(needle) !== -1 ;
         });

         return filterLenders;
      }
   }
 },

 methods: {
   selectType(target) {
      //console.log(target.value);

     //this.selectedType = target.getAttribute('value');
     this.selectedType = target.value;
   }
 }
})


