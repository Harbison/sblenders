﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

const urlParams = new URLSearchParams(window.location.search);

$(function () {

	$('#navIcon').click(function () {
		$('body').toggleClass('menuOpen');
		$('body').removeClass('quickContactOpen');
	});
	$('#quickContactBtn, #mobileCTAbtn').click(function () {
		$('body').addClass('quickContactOpen');
		$('body').removeClass('menuOpen');
	});
	$('#qcClose').click(function () {
		$('body').removeClass('quickContactOpen');
	});
	$('#searchIcon').click(function () {
		$('body').addClass('seOpen');
	});
	$('#seClose').click(function () {
		$('body').removeClass('seOpen');
	});


// Load Top

	$('#blip').data('visibility', 'visible');

	if ($(document).scrollTop() > 0) {
		if ($('#blip').data('visibility') == 'visible') {
			$('#blip').data('visibility', 'hidden');
			$('body').addClass('scrolling');
		}
	}
	else {
		if ($('#blip').data('visibility') == 'hidden') {
			$('#blip').data('visibility', 'visible');
			$('body').removeClass('scrolling');
		}
	}


// Scroll Top

	$('#blip').data('visibility', 'visible');


	$('#qcContactForm input, #qcContactForm textarea').focus(function () {
		$(this).siblings('.inputPH').css('display', 'none');
	});
	$('#qcContactForm input, #qcContactForm textarea').blur(function () {
		if ($(this).val().length > 0) {
			$(this).siblings('.inputPH').css('display', 'none');
		} else {
			$(this).siblings('.inputPH').css('display', 'block');
		}
	});



	$('#submitBtn').click(function () {
		var emptyValidator = $('.validator').css('visibility');
		if (emptyValidator == 'hidden') {
			$('#pleaseWait').css('display', 'block');
		}
	});



/* Home Page Slider */

	var cards = $("#homeSlider .slide.randomize");
	for (var i = 0; i < cards.length; i++) {
		var target = Math.floor(Math.random() * cards.length - 1) + 1;
		var target2 = Math.floor(Math.random() * cards.length - 1) + 1;
		cards.eq(target).before(cards.eq(target2));
	}




	$('#homeSlider .slide').each(function () {
		var homeSliderContents = $('#homeSlider').html();
		$('#homeSliderBG').html(homeSliderContents);
		$('#homeSliderBG .slide .container').remove();
	});
	$('#homeSliderBG .slide').each(function () {
		var homeSliderBG = $(this).attr('data-bg-img');
		$(this).attr('style', 'background-image:url(' + homeSliderBG + ')')
	});



	var homeSliderBG = {
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		dots: false
	};
	var homeSlider = {
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		arrows: true,
		dots: true,
		prevArrow: '<div class="slick-arrow slick-prev"></div>',
		nextArrow: '<div class="slick-arrow slick-next"></div>',
		asNavFor: '.homeSlider',
		appendDots: '#homeSliderDots',
		appendArrows: "#homeSliderArrows",
		autoplay: true
	};
	$('#homeSlider').slick(homeSlider);
	$('#homeSliderBG').slick(homeSliderBG);


/* Staff Slider */

	var cards = $(".staffMember");
	for (var i = 0; i < cards.length; i++) {
		var target = Math.floor(Math.random() * cards.length - 1) + 1;
		var target2 = Math.floor(Math.random() * cards.length - 1) + 1;
		cards.eq(target).before(cards.eq(target2));
	}



	var staffSlider = {
		slidesToShow: 8,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: '<div class="slick-arrow slick-prev"></div>',
		nextArrow: '<div class="slick-arrow slick-next"></div>',
		appendArrows: "#staffSliderArrows",
		pauseOnHover: true,
		pauseOnFocus: true,
		infinite: false,
		responsive: [
			{
				breakpoint: 1601,
				settings: {
					slidesToShow: 6,
					slidesToScroll: 6,
				}
			},
			{
				breakpoint: 1201,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 5
				}
			},
			{
				breakpoint: 1025,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 993,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 430,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	};
	$('#staffSlider').slick(staffSlider);

/* End Staff Slider */

/* Testimonial Slider */

	var testimonialPicSlider = {
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		dots: false
	};
	var testimonialSlider = {
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		fade: true,
		appendDots: '#testimonialDots',
		prevArrow: '<div class="slick-arrow slick-prev"></div>',
		nextArrow: '<div class="slick-arrow slick-next"></div>',
		asNavFor: '#testimonialPicSlider',
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					adaptiveHeight: true
				}
			},
		]
	};
	$('#testimonialPicSlider').slick(testimonialPicSlider);
	$('#testimonialSlider').slick(testimonialSlider);


/* Logo Slider */

	var logoSlider = {
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000,
		pauseOnDotsHover: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	};
	$('#logoSlider').slick(logoSlider);
	$('#logoSlider .slick-dots').on('click', function () {
	$('#logoSlider').slick('slickPause');
	});


	$('#ctaContact input, #ctaContact textarea').focus(function () {
		$(this).siblings('.inputPH').css('display', 'none');
	});
	$('#ctaContact input, #ctaContact textarea').blur(function () {
		if ($(this).val().length > 0) {
			$(this).siblings('.inputPH').css('display', 'none');
		} else {
			$(this).siblings('.inputPH').css('display', 'block');
		}
	});




	var sidebarContent = $('#sideBar').html();
	$('#mobileSidebar').append(sidebarContent);



	var sidebar = $('#sidebar').html();
	$('#mobileSidebar').append(sidebar);



	var noCacheString = Math.floor(Math.random() * 100) + 1;
	$('.noCache').each(function () {
		var href = $(this).attr('href');

		if (href) {
			href += (href.match(/\?/) ? '&' : '?') + noCacheString;
			$(this).attr('href', href);
		}
	});



	$('#homeNews #MainContent_NewsPanel span').addClass('col-12 col-md-4');




	//$('.employeeContainer').each(function () {
	//	var employeePic = $(this).attr('employeePic');
	//	$(this).children('.employeePhoto').attr('style', 'background-image:url(' + employeePic + ')');
	//});
	
	
	//$('.emailLink').each(function () {
	//	var employeeLink = $(this).attr('employeeLink');
	//	$(this).append('<a href="mailto:' + employeeLink + '"></a>');
	//	$(this).children('.employeePhoto').append('<div class="popup">' + employeeLink + '</div>');
	//});
	//$('.profileLink').each(function () {
	//	var employeeLink = $('.profileLink').attr('employeeLink');
	//	$(this).append('<a href="' + employeeLink + '"></a>');
	//	$(this).children('.employeePhoto').append('<div class="popup">View Bio/Contact</div>');
	//});


});





$(window).scroll(function () {
	if ($(document).scrollTop() > 0) {
		if ($('#blip').data('visibility') == 'visible') {
			$('#blip').data('visibility', 'hidden');
			$('body').addClass('scrolling');
		}
	}
	else {
		if ($('#blip').data('visibility') == 'hidden') {
			$('#blip').data('visibility', 'visible');
			$('body').removeClass('scrolling');
		}
	}
});

/* Logistics Map */
$(function () {
	function cities() {/* Louisville */
		if ($('#louisville').hasClass('selected') && $('#newYork').hasClass('selected')) {
			$('#mapLouisville').addClass('active');
			$('#mileage').append('650');
		}
		if ($('#louisville').hasClass('selected') && $('#charlotte').hasClass('selected')) {
			$('#mapLouisville').addClass('active');
			$('#mileage').append('335');
		}
		if ($('#louisville').hasClass('selected') && $('#washington').hasClass('selected')) {
			$('#mapLouisville').addClass('active');
			$('#mileage').append('475');
		}
		if ($('#louisville').hasClass('selected') && $('#chicago').hasClass('selected')) {
			$('#mapLouisville').addClass('active');
			$('#mileage').append('275');
		}
		if ($('#louisville').hasClass('selected') && $('#atlanta').hasClass('selected')) {
			$('#mapLouisville').addClass('active');
			$('#mileage').append('315');
		}

		/* Lexington */
		if ($('#lexington').hasClass('selected') && $('#newYork').hasClass('selected')) {
			$('#mapLexington').addClass('active');
			$('#mileage').append('595');
		}
		if ($('#lexington').hasClass('selected') && $('#charlotte').hasClass('selected')) {
			$('#mapLexington').addClass('active');
			$('#mileage').append('280');
		}
		if ($('#lexington').hasClass('selected') && $('#washington').hasClass('selected')) {
			$('#mapLexington').addClass('active');
			$('#mileage').append('410');
		}
		if ($('#lexington').hasClass('selected') && $('#chicago').hasClass('selected')) {
			$('#mapLexington').addClass('active');
			$('#mileage').append('310');
		}
		if ($('#lexington').hasClass('selected') && $('#atlanta').hasClass('selected')) {
			$('#mapLexington').addClass('active');
			$('#mileage').append('300');
		}

		/* Northern KY */
		if ($('#nky').hasClass('selected') && $('#newYork').hasClass('selected')) {
			$('#mapNKY').addClass('active');
			$('#mileage').append('570');
		}
		if ($('#nky').hasClass('selected') && $('#charlotte').hasClass('selected')) {
			$('#mapNKY').addClass('active');
			$('#mileage').append('335');
		}
		if ($('#nky').hasClass('selected') && $('#washington').hasClass('selected')) {
			$('#mapNKY').addClass('active');
			$('#mileage').append('400');
		}
		if ($('#nky').hasClass('selected') && $('#chicago').hasClass('selected')) {
			$('#mapNKY').addClass('active');
			$('#mileage').append('250');
		}
		if ($('#nky').hasClass('selected') && $('#atlanta').hasClass('selected')) {
			$('#mapNKY').addClass('active');
			$('#mileage').append('370');
		}

		/* Northern KY */
		if ($('#bowlingGreen').hasClass('selected') && $('#newYork').hasClass('selected')) {
			$('#mapBowlingGreen').addClass('active');
			$('#mileage').append('720');
		}
		if ($('#bowlingGreen').hasClass('selected') && $('#charlotte').hasClass('selected')) {
			$('#mapBowlingGreen').addClass('active');
			$('#mileage').append('335');
		}
		if ($('#bowlingGreen').hasClass('selected') && $('#washington').hasClass('selected')) {
			$('#mapBowlingGreen').addClass('active');
			$('#mileage').append('530');
		}
		if ($('#bowlingGreen').hasClass('selected') && $('#chicago').hasClass('selected')) {
			$('#mapBowlingGreen').addClass('active');
			$('#mileage').append('345');
		}
		if ($('#bowlingGreen').hasClass('selected') && $('#atlanta').hasClass('selected')) {
			$('#mapBowlingGreen').addClass('active');
			$('#mileage').append('250');
		}

		/* Southeast KY */
		if ($('#owensboro').hasClass('selected') && $('#newYork').hasClass('selected')) {
			$('#mapOwensboro').addClass('active');
			$('#mileage').append('730');
		}
		if ($('#owensboro').hasClass('selected') && $('#charlotte').hasClass('selected')) {
			$('#mapOwensboro').addClass('active');
			$('#mileage').append('390');
		}
		if ($('#owensboro').hasClass('selected') && $('#washington').hasClass('selected')) {
			$('#mapOwensboro').addClass('active');
			$('#mileage').append('500');
		}
		if ($('#owensboro').hasClass('selected') && $('#chicago').hasClass('selected')) {
			$('#mapOwensboro').addClass('active');
			$('#mileage').append('285');
		}
		if ($('#owensboro').hasClass('selected') && $('#atlanta').hasClass('selected')) {
			$('#mapOwensboro').addClass('active');
			$('#mileage').append('315');
		}
	}
	$('#citySelectOne li').click(function () {
		$('#citySelectOne.citySelect li').removeClass('selected');
		$(this).addClass('selected');
		$('#mileage').empty();
		$('.mapMarker').removeClass('active');
		cities();
	});
	$('#citySelectTwo li').click(function () {
		$('#citySelectTwo.citySelect li').removeClass('selected');
		$(this).addClass('selected');
		$('#mileage').empty('new mileage');
		$('.mapMarker').removeClass('active');
		cities();
	});
});

$('.phone').click(function () {
	var label = ($(this).data('label') ? $(this).data('label') : 'none');	

	gtag('event', 'phone_number_click', {
		'app_name': 'Phone Number Click',
		'event_label': 'CED Phone Number' + label
	});

});

$('.track').click(function () {
	var label = ($(this).data('label') ? $(this).data('label') : 'none');

	gtag('event', 'tracked_click', {
		'app_name': 'Tracked Click',
		'event_label': 'CED Tracked Click: ' + label
	});
});



let announcement = '';

$("#ced_announcement").hide()
$.ajax({
	method: "GET",
	url: "https://cedky.com/api/4294967295/data_content/ced_announcement"
})
	.done(function (data) {
		$("#ced_announcement").html(data[0].Message).slideDown('slow');
	});



/////// CED INQUIRY
var emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
var contact_name = $("#contact_name");
var contact_email = $("#contact_email");
var contact_phone = $("#contact_phone");
var allFields = $([]).add(contact_name).add(contact_email).add(contact_phone);
var	tips = $(".validateTips");

function updateTips(t) {
	tips
		.text(t)
		.addClass("text-danger");
	setTimeout(function () {
		tips.removeClass("text-info", 1500);
	}, 500);
}

function checkLength(o, n, min, max) {
	if (o.val().length > max || o.val().length < min) {
		o.addClass("ui-state-error");
		updateTips("Length of " + n + " must be between " +
			min + " and " + max + ".");
		return false;
	} else {
		return true;
	}
}

function checkRegexp(o, regexp, n) {
	if (!(regexp.test(o.val()))) {
		o.addClass("text-danger");
		updateTips(n);
		return false;
	} else {
		return true;
	}
}

var request_subject;

function submitResponse() {
	var valid = true;
	allFields.removeClass("text-alert");

	$(".validateTips").text("Submitting Form");

	valid = valid && checkLength(contact_name, "username", 3, 16);
	valid = valid && checkLength(contact_email, "email", 6, 80);
	valid = valid && checkLength(contact_phone, "phone", 5, 16);

	valid = valid && checkRegexp(contact_name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter.");
	valid = valid && checkRegexp(contact_email, emailRegex, "eg. john@doe.com");
	valid = valid && checkRegexp(contact_phone, /^(?=.*[0-9])[- +()0-9]+$/, "Phone field only allow :  0-9");

	if (valid) {

		var form_data = $("#ced_inquiry_form").serializeArray();

		// Add Submission Url to obj sent over
		form_data.push({ name: 'submission_url', value: window.location.href }); // Returns full URL

		request_subject = $("#request_subject").val();

		$.ajax({
			method: "POST",
			url: "https://cedky.com/api/3234235674/contact_form_submission",
			data: form_data
		})
			.done(function (msg, form_data) {
				//console.log(msg.status);
				console.log(msg);
				//dialog.dialog( "close" );
				console.log(form_data);

				console.log(request_subject);




				if (msg.status == "success") {

					$(".validateTips").hide();

					$("#ced-form-body").html("Thank you for your reply!  We will be in touch");

					setTimeout(function () {
						$('#submitCedInquiry').hide();
						$('#contact_modal').modal('hide');
					}, 3000);



				// 	gtag('event', 'form_submission', {
				// 		'app_name': 'NKY Biz Form',
				// 		'event_label': request_subject,
				// 		'event_value': 1
				// 	});

				// //Event snippet for Website traffic conversion page
				// gtag('event', 'conversion', {'send_to': 'AW-10926906063/cXtFCLis4cQDEM-1rdoo'});


					


				} else {
					$(".validateTips").text('There was an issue with submitting - please call us');
				}
			});
	}
	return valid;
}


$("#submitCedInquiry").on("click", function () {
	console.log("Submit hit");

	submitResponse();
});


/// NEED TO CAPTURE ADWORDS ANALYTICS IN CED CAMPAIGN MANAGER
// IF param hit exist in url
// AJAX GET for https://cedky.com/r-nf/ + hit_param
if (urlParams.has("hit")) {
	var hitUrl = urlParams.get("hit");

	$.ajax({
		method: "GET",
		url: "https://cedky.com/r-nf/" + hitUrl
	})
		.done(function (msg) {
			//console.log(msg.status); 
			//console.log(msg); 
	});
}
