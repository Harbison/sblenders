


        <!--Footer-->
        <footer id="footer" class="bg-dkr-blue clr-gray padding-top-100 padding-btm-20">
            
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h5 class="h5 clr-white">About the Kentucky Cabinet for<br>Economic Development</h5>
                <hr class="spacer-20">
                The Cabinet for Economic Development is the primary state agency in Kentucky responsible for encouraging job creation and retention, and new investment in the state.
                <hr class="spacer-20">
                <a class="btn sm" href="/Home/AboutUs/">Read More</a>
                <hr class="spacer-60">
            </div>
            <div class="col-12 col-md-6">
                <h5 class="h5 clr-white">Quick Links</h5>
                <hr class="spacer-20">
                <ul class="quickLinks">



             
                        <li><a href="https://properties.zoomprospector.com/kentucky?page=1&amp;s%5BSortDirection%5D=true&amp;s%5BSortBy%5D=featured&amp;s%5BIsBuilding%5D=false">Available Sites &amp; Buildings</a></li>
                        <li><a href="/Entrepreneurship">Start/Grow Business</a></li>
                        <li><a href="/Workforce">Workforce Training</a></li>
                        <li><a href="/Locating_Expanding/Financial_Incentives">Financial Incentives</a></li>
                        <li><a href="/KYFacts">Search Statistics</a></li>
                        <li><a href="/International/Exports">Export My Products</a></li>
                        <li><a href="/Entrepreneurship/Access_To_Capital">Capital</a></li>
                        <li><a href="/Entrepreneurship/Investor_Information">Investor Information</a></li>
                        <li><a href="/Home/ContactUs">Contact Us</a></li>
                   


                </ul>
            </div>
        </div>
        <hr class="spacer-40">
        <hr class="margin-30 bg-gray opake">
        <ul class="footerBtmLinks">
            <li><a href="/Home/Transparency">Transparency</a></li>           
            <li><a href="/Home/Accessibility">Accessibility</a></li>
            <li><a href="/Home/Privacy">Privacy</a></li>
            <li><a href="/Home/Disclaimer">Disclaimer</a></li>
        </ul>
        <ul class="socialIcons">
            <li class="fb track" data-label="Footer social FB (_Footer) @Context.Request.Host@Context.Request.Path"><a href="https://www.facebook.com/CEDkygov/" target="_blank"></a></li>
            <li class="tw track" data-label="Footer social TW (_Footer) @Context.Request.Host@Context.Request.Path"><a href="https://twitter.com/CEDkygov" target="_blank"></a></li>
            <li class="li track" data-label="Footer social Linked IN (_Footer) @Context.Request.Host@Context.Request.Path"><a href="https://www.linkedin.com/company/cedkygov/" target="_blank"></a></li>
            <li class="ig track" data-label="Footer social INStagram (_Footer) @Context.Request.Host@Context.Request.Path"><a href="https://www.instagram.com/cedkygov/" target="_blank"></a></li>
            
        </ul>
        <div class="clear"></div>
        <hr class="spacer-20">
        <div class="text-center">
            <span class="txt-xs">
                © Copyright  Kentucky Cabinet for Economic Development |
              
                 
                    <span class="track" data-label="Footer address (_Footer) @Context.Request.Host@Context.Request.Path">Old Capitol Annex 300 West Broadway | Frankfort, KY 40601 | 502 - 564 - 7670(local) | 800 - 626 - 2930(toll free)</span>
              
            </span>
        </div>
    </div>


        </footer>

        <div id="mobileCTA">
            <div id="mobileCTAbtn" class="btn xs track" data-label="Do Business in KY (wideLayout - mobile)">
                Do Business In KY
            </div>
        </div>




        <script src="site/lib/jquery/dist/jquery.min.js"></script>

        <!-- slick -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>

        <script src="site/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="site/js/site.min.js" asp-append-version="true"></script>


        <!-- Moment -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>



<script>
    // $('.rowSlider').slick({
    //     infinite: true,
    //     slidesToShow: 4,
    //     slidesToScroll: 4,
    //     dots: false,
    //     arrows: false,
    //     autoplay: true,
    //     autoplaySpeed: 4000,
    //     pauseOnDotsHover: true,
    //     responsive: [
    //         {
    //             breakpoint: 992,
    //             settings: {
    //                 slidesToShow: 3,
    //                 slidesToScroll: 3,
    //             }
    //         },
    //         {
    //             breakpoint: 767,
    //             settings: {
    //                 slidesToShow: 2,
    //                 slidesToScroll: 2
    //             }
    //         },
    //         {
    //             breakpoint: 480,
    //             settings: {
    //                 slidesToShow: 1,
    //                 slidesToScroll: 1
    //             }
    //         }
    //     ]
    // });
</script>

       

        </main>
    </div>













</body>
</html>