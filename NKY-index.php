<?php 
$title = "Your New Kentucky Home | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<div class="container-vid" style="z-index: 1; ">
    <div class="actions">
        <a class="btn read play video1">PLAY</a> 
    </div>

    <video id="vidTop" autoplay="" muted="" loop="">
        <source src="/site/images/nky-landing-10bit.mp4" type="video/mp4">
    </video>
</div><div class="row">
<div class="col">
<img src="/site/images/title-white.png" class="title" />
</div>
</div>

<!-- BLUE TITLE PARAGRAPH -->
<section style="background: #104467; color: white; margin-top: -15px;">
    <div class="container">
        <div class="row">
            
            <div class="col-12 p-5">
                
                <div class="med-text text-center">
              Here in Kentucky, business is fast, and the pace of life is just right. We found the way to create a brighter future by working together and by leading with our values of kindness and hard work. Now, we're looking at a new Kentucky, where good jobs and a good life are possible for everyone. <b style='font-family: "ProximaNovaBold", sans-serif;'> Y'all interested? Come join us and explore your new Kentucky home.</b>
                
                </div>
                
                <p class="text-center">
                            <br clear="all">
                    <a href="" data-toggle="modal" data-target="#contact_modal" class="btn new big" style="border: 1px solid white;">Get in touch</a>
                </p>
            </div>
        </div>
    </div>
</section>




<!-- INKY -->
<section>
    <div class="blue_band">
        <div class="container text-center">
            <img class="blue_band_state_top" src="/site/images/state.jpg" style="max-height: 50px;" >

             <a href="/LP/NKY_Access_To_Capital">Invest in KY</a>

             <a href="/LP/NKY_lifeInKy">Live in KY</a>

             <img class="blue_band_state_mid" src="/site/images/state.jpg" style="max-height: 50px;" >

             <a href="/LP/NKY_WorkForce">Work in KY</a>

             <a href="https://www.kentuckytourism.com/events/">FUN In KY</a>
        </div>

    </div>
 </section>


<!-- number 1s -->
<section>
    <div class="row  number_ones">
        <div class="col-lg-3 col-md-6 col-sm-12">
                <img src="/site/images/1o.jpg" class="img-fluid zoom" >
        </div>    
        <div class="col-lg-3 col-md-6 col-sm-12">
                <img src="/site/images/2o.jpg" class="img-fluid  zoom" >
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
                <img src="/site/images/3o.jpg" class="img-fluid  zoom" >
        </div> 
        <div class="col-lg-3 col-md-6 col-sm-12">
                <img src="/site/images/4o.jpg" class="img-fluid  zoom" >
        </div> 
    </div>
</section>

<hr class="spacer-25">


<!-- NEW -->
<section>
<div class="row p-5">
<div class="col-12 col-md-4">
<h2>SUCCESS STORIES</h2>
<hr class="spacer-25">
<p>
Kentucky’s supportive and thriving business climate
combined with innovative programs, enhanced quality of
life and communities committed to helping businesses
succeed, makes it easy to see why Kentucky is always a
smart bet
</p>
<img src="/site/images/success.jpg" class="img-fluid"/>
</div>
<div class="col-12 col-md-8">
<h2>KENTUCKY IN THE NEWS</h2>
<hr class="spacer-25">
<div id="MainContent_NewsPanel" class="news-stories">


</div>
<a href="/Newsroom/News_Releases" class="btn read"> More News</a>
</div>
</div>
</section>




<!-- DISTANCE -->
<section class="section">
    <div class="container">
        <div class="row">
            <div id="citySelectContainer" class="col-12 col-md-6 col-lg-5">
                <h3 class="h3 clr-blue">Kentucky is within one day's drive of two-thirds of the U.S. population</h3>
                <hr class="spacer-20" />Kentucky's logistical advantage is unmatched. Click the cities below to measure distance.<hr class="spacer-20" />
                <div class="inner">
                    <div class="txt-md font-weight-bold">DISTANCE: <span class="clr-orange"><span id="mileage">738</span> miles</span></div>
                    <hr class="spacer-20" />
                    <ul id="citySelectOne" class="citySelect">
                        <li id="louisville" class="selected">Louisville</li>
                        <li id="lexington">Lexington</li>
                        <li id="bowlingGreen">Bowling Green</li>
                        <li id="owensboro">Owensboro</li>
                        <li id="nky">Covington</li>
                    </ul>
                    <ul id="citySelectTwo" class="citySelect right">
                        <li id="newYork" class="selected">New York</li>
                        <li id="charlotte">Charlotte</li>
                        <li id="washington">Washington DC</li>
                        <li id="chicago">Chicago</li>
                        <li id="atlanta">Atlanta</li>
                    </ul>
                    <div class="clearfix">&nbsp;</div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-7">
                <div id="cityMap" class="position-relative"><img class="fullWidth" src="https://ced.ky.gov/media/WebMedia/logistics-map.png" alt="Logistics Map" />
                    <div id="kyCities">
                        <div id="mapLouisville" class="mapMarker active">&nbsp;</div>
                        <div id="mapLexington" class="mapMarker">&nbsp;</div>
                        <div id="mapNKY" class="mapMarker">&nbsp;</div>
                        <div id="mapBowlingGreen" class="mapMarker">&nbsp;</div>
                        <div id="mapOwensboro" class="mapMarker">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container">
<img class="img-fluid" src="/site//images/social.jpg"><br>
</div>


<section>
<div class="container">
    <div class="row py-5 no-gutters">

        <div class="col-12 col-md-6 col-lg-6 light-gray py-5 text-blue text-center">
            <h1 style="line-height: 1em">Our New <br>Kentucky Home</h1>                         

            <span class="text-medium">Where business is fast, <br>but the pace of life is just right. <br><br></span>
            
            <center>
            <a class="btn read" style="display: block; width: 75%;" href="/LP/NKY_lifeInKy" title="Learn More">Learn More</a>
            </center>

        </div>

        <div class="col-12 col-md-6 d-print-none light-gray p-3">
            
        <div class="container-vid" style="z-index: 1; ">
            <div style="top: 76%;left: 39%;display: block;position: absolute;z-index: 2;">
                <button class="btn read video1">PLAY VIDEO</button> 
            </div>

            <video id="vidBot" autoplay="" muted="" loop="">
                    <source src="/site/images/hype_sm.mp4?21" type="video/mp4">
            </video>
        </div>

        </div>
    </div>
</div>
</section>






<!-- SOCIAL -->
<section class="mb-5">
    <div class="container">
        <div class="row my-5 text-center">
            <div class="col-md-2"> </div>
            <div class="col-md-8 col-sm-12">
                <div class="med-text">
                    <h2 class="text-blue">Great companies need great people, and great people need great communities.<br><i> Kentucky has all three &ndash; great companies, great people and great communities.</i></h2>
                    We know how to attract and retain great talent. With vibrant communities, lower cost of living, good schools and more, our families and our businesses are both thriving.<br><br>
                    <center><b style='font-family: "ProximaNovaBold", sans-serif;'>Come see why life is better in your new Kentucky home.</b></center>
                </div>
            </div>
            <div class="col-md-2"> </div>
        
        </div>
    </div>
</section>






<!-- GET IN TOUCH -->
<section class="position-relative padding-160 clr-white">
    <div id="staffSliderContainer">
    <div id="staffBG" class="container bg-fltr bg-dkr-fltr section bg-cover" style="background-image: url('https://ced.ky.gov/media/WebMedia/state-capitol-building-2.jpg');">&nbsp;</div>
    <header class="upLevel text-center">
    <div class="container padding-h-30">
    <h2 class="h2 clr-blue">Get in touch with people that can help</h2>
    <hr class="spacer-20" />
    <div>These are the faces of our cabinet and your gateway to growing your business in Kentucky. While our project managers specialize in various industries and regions, they are all driven to make Kentucky the most business-friendly state in America. Meet them here, and don&rsquo;t hesitate to reach out!</div>
    <hr class="spacer-40" /></div>
    </header>
    <div class="text-center">
    <div id="staffSliderArrows">&nbsp;</div>
    </div>
    <div id="staffSlider" class="upLevel">
    <div id="AndyLuttner" class="staffMember AndyLuttner">
    <div class="staffPic">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Andy Luttner
    <div class="zone">Business Development Director</div>
    </div>
    <div class="info fadeIn"><a>(502) 782-1957</a></div>
    </div>
    </div>
    <div id="CorkyPeek" class="staffMember CorkyPeek">
    <div class="staffPic">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Corky Peek
    <div class="zone">Business Development Director</div>
    </div>
    <div class="info fadeIn"><a>(270) 210-0033</a></div>
    </div>
    </div>
    <div id="KristinaSlattery" class="staffMember KristinaSlattery">
    <div class="staffPic">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Kristina Slattery
    <div class="zone">Commissioner, Business Development</div>
    </div>
    <div class="info fadeIn"><a>(502) 782-1946</a></div>
    </div>
    </div>
    <div id="Finn" class="staffMember">
    <div class="staffPic" style="background-image: url('https://cedky.com/cdn/1727_Finn_Weisse.jpg');">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Finn Weisse
    <div class="zone">Executive Director, European Office</div>
    </div>
    <div class="info fadeIn">&nbsp;</div>
    </div>
    </div>
    <div id="MikeTakahashi" class="staffMember MikeTakahashi">
    <div class="staffPic">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Mike Takahashi
    <div class="zone">Executive Director, Asia Office (Tokyo, Japan)</div>
    </div>
    <div class="info fadeIn"><a>(502) 782-1968</a></div>
    </div>
    </div>
    <div id="AshleeChilton" class="staffMember AshleeChilton">
    <div class="staffPic">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Ashlee Chilton
    <div class="zone">Senior Project Manager</div>
    </div>
    <div class="info fadeIn"><!-- <a>(502) 782-1968</a> --></div>
    </div>
    </div>
    <div id="MalcolmJollie" class="staffMember MalcolmJollie">
    <div class="staffPic">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Malcolm Jollie
    <div class="zone">Senior Project Manager</div>
    </div>
    <div class="info fadeIn"><!-- <a>(502) 782-1968</a> --></div>
    </div>
    </div>
    <div id="AmyBowman" class="staffMember AmyBowman">
    <div class="staffPic">&nbsp;</div>
    <div class="infoContainer">
    <div class="name">Amy Bowman
    <div class="zone">Business Development Coordinator</div>
    </div>
    <div class="info fadeIn"><!-- <a>(502) 782-1968</a> --></div>
    </div>
    </div>
    <div id="DanielleMilbern" class="staffMember">
        <div class="staffPic" style="background-image: url('https://cedky.com/cdn/1727_DanielleMilbern.jpg');">&nbsp;</div>
        <div class="infoContainer">
            <div class="name">Danielle Milbern
                <div class="zone">Project Manager - Agriculture Specialist</div>
            </div>
            <div class="info fadeIn"><!-- <a>(502) 782-1968</a> --></div>
        </div>
    </div>
    <div id="ScottMoseley" class="staffMember">
        <div class="staffPic" style="background-image: url('https://cedky.com/cdn/1727_ScottMoseley.jpg');">&nbsp;</div>
        <div class="infoContainer">
            <div class="name">Scott Moseley
                <div class="zone">Project Manager</div>
            </div>
            <div class="info fadeIn"><!-- <a>(502) 782-1968</a> --></div>
        </div>
    </div>

    <div id="BrittanyPetty" class="staffMember">
        <div class="staffPic" style="background-image: url('https://cedky.com/cdn/1727_BrittanyPetty.jpg');">&nbsp;</div>
        <div class="infoContainer">
            <div class="name">Brittany Petty
                <div class="zone">Project Manager</div>
            </div>
            <div class="info fadeIn"><!-- <a>(502) 782-1968</a> --></div>
        </div>
    </div>


    <hr class="spacer-40" />
    <div class="text-center upLevel"><a class="btn" target="_blank" href="https://ced.ky.gov/Home/Staff">See our Full Staff</a></div>
    </div>
</section>

<!--ced_testimonials_manufacturing_carousel-->
<section class="section">
    <div class="container">
        <header class="text-center">
            <div class="clr-gray subHead">Kentucky is proud to promote</div>
            <hr class="spacer-10">
            <h2 class="h2 clr-blue">The Company We Keep</h2>
            <hr class="spacer-40">
        </header>
        <div id="logoSlider">
            <div class="slide">
                <a href="https://www.toyota.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/toyota-2.png" alt="Toyota" /></a>
            </div>
            <div class="slide">
                <a href="https://www.amazon.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/amazon.png" alt="Amazon" /></a>
            </div>
            <div class="slide">
                <a href="https://www.ups.com/us/en/global.page" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/ups.png" alt="UPS" /></a>
            </div>
            <div class="slide">
                <a href="https://www.ford.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/ford.png" alt="Ford" /></a>
            </div>
            <div class="slide">
                <a href="https://www.dhl.com/en.html" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/DHL.png" alt="DHL" /></a>
            </div>
            <div class="slide">
                <a href="https://www.jimbeam.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/jim-beam.png" alt="Jim Beam" /></a>
            </div>
            <div class="slide">
                <a href="https://www.beamsuntory.com/en" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/beam-suntory.png" alt="Beam Suntory" /></a>
            </div>
            <div class="slide">
                <a href="https://www.chevrolet.com/performance/corvette" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/corvette.png" alt="Corvettte" /></a>
            </div>
            <div class="slide">
                <a href="https://www.ge.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/GE.png" alt="General Electric" /></a>
            </div>
            <div class="slide">
                <a href="https://www.alltech.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/alltech.png" alt="Alltech" /></a>
            </div>
            <div class="slide">
                <a href="https://nucor.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/nucor.png" alt="Nucor" /></a>
            </div>
            <div class="slide">
                <a href="https://www.valvoline.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/valvoline.png" alt="Valvoline" /></a>
            </div>
            <div class="slide">
                <a href="https://www.tempursealy.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/tempur_sealy.png" alt="Tempur Sealy" /></a>
            </div>
            <div class="slide">
                <a href="https://www.lexmark.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/lexmark.png" alt="Lexmark" /></a>
            </div>
            <div class="slide">
                <a href="https://www.yum.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/yum-brands.png" alt="Yum Brands" /></a>
            </div>

        </div>
    </div>
</section>
<div id="vidBox">
    <div id="videCont">
        <video class="vidpop"  id="pvv1" controls>
                <source src="/site/images/tourism-1m.mp4" type="video/mp4">
        </video>
    </div>
</div>




<?php include('NKY-footer.php'); ?>