//// Create Article File var
var article_file = '';

if (typeof article === 'undefined'){
    article_file = articles[0].page_file;
} else {
    article_file = article;
    
}
// GET ARTICLE
var pageArticle = articles.filter(function(e){
    return e.page_file == article_file;
});

// SET PAGE TITLE
document.title = pageArticle[0].headline + ' | Kentucky Cabinet For Economic Development';

var ContentComp = { 
    name: 'content-comp',
    data: function() {
       return {
          articles: articles,
          article: pageArticle[0]
       }
    },
    methods: {
        articleDate(testDate) {
            return moment(testDate).format('MMMM Do YYYY, h:mm:ss a')
        },
        makelink(file) {
            return 'https://ced.ky.gov/NewsRoom/Article.aspx?x=' + file;
        },
        titleCase(str) {
            return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }
    },
    template: '<div class="text-center">' + 
            '<h1 class="h4">{{ titleCase(article.headline) }}</h1>' +
            '<h3 class="small">{{ article.sub_headline }} </h3>' + 
            '<hr class="padding-10" /></div>'
 }
 
 new Vue({
  el: '#vue-app',
  components: {
    'content-comp': ContentComp
  }
 })
 

// Element for addition captions to images
jQuery(".newsReleaseImg").each(function(index){
    jQuery(this).replaceWith(
            '<figure class="figure p-3 ' +  ( index%2 == 0 ? 'float-left' : 'float-right' )  + ' style="clear:left;">' +
            '<img src="' + $(this).attr('src') + '" class="figure-img img-fluid rounded" alt="' + ( $(this).attr('alt') ?? 'Image' ) + '">' +
            '<figcaption class="figure-caption">' + ( $(this).data('caption') ?? '' )  + '</figcaption>' +
            '</figure>'
    ); 
});
