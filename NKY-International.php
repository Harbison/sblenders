<?php 
$title = "Internation Overview | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">International</h1>
               
            </div>
        </div>
    </div>
</section>


<iframe style="aspect-ratio: 16 / 9; width: 100%;"  src="https://www.youtube.com/embed/6jd2hzkbShc?si=upmaVXpXGfMweydU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>



<!-- quote for workforce -->
<section class="container mt-5 ">
        <div class="row text-center">
            <div class="col-md-4 col-sm-8">
               <span class="text-medium text-bold text-blue full">540 Internationally owned
                </span>
                <span class="text-medium text-blue full">businesses in Kentucky</span>
            </div>
            <div class="col-md-2 col-sm-4">
                 <img class="img-fluid" src="/site/images/international.jpg">

            </div>
            <div class="col-md-4 col-sm-8">
               <span class="text-medium text-bold text-blue full">33 Nations have
                </span>
                <span class="text-medium text-blue full">Kentucky-based facilities</span>
            </div>
            <div class="col-md-2 col-sm-4">
                <img class="img-fluid" src="/site/images/i-5.jpg">
            </div>
</section>



<section class="container">
<h2>Overview</h2>
<hr class="margin-40">
<div class="row">
    <div class="col-12">
    <p>Vítejte. Welkom. Bienvenue. Herzlich willkommen. Fáilte. Benvenuto. Yôkoso. Bienvenido.<br>How do you say welcome in 100 languages at once? <strong>Kentucky.</strong></p>
    <p>Kentucky’s strong international ties – in both <a href="https://ced.ky.gov/International/International_Facilities">internationally owned facilities</a> and <a href="https://ced.ky.gov/International/Exports">exports</a> – speaks to the fact executives the world over recognize the Bluegrass State as a premier location and partner for business.</p>
    <p>Foreign Direct Investment in Kentucky includes nearly 540 facilities that employ more than 116,000 people. Kentucky’s low business costs, robust shipping and logistics options, a dedicated workforce and a network of state and local economic development experts and resources make it the clear choice for foreign direct investment. Kentucky’s international offices and available assistance are valuable assets for international companies looking to expand in the United States.</p>
    <p>In addition, Kentucky products and services are valued by international customers. <a href="https://ced.ky.gov/International/Exports">Exports</a> by Kentucky businesses continue to break records, topping $40 billion in 2023 as the Cabinet for Economic Development continued working with the state’s businesses to increase international sales opportunities. The state’s Kentucky Export Initiative provides education, hands-on assistance, resources, matchmaking opportunities and grants to companies that wish to sell their products and services abroad.</p>
   
<hr class="spacer-60">
<hr class="spacer-40">
<img class="img-fluid" src="https://ced.ky.gov/media/world-flag-map.png">
<hr class="spacer-40">
 </div>
</div>
</section>



<?php include('NKY-footer.php'); ?>