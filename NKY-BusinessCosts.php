<?php 
$title = "Business Costs | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Business Costs</h1>
                <p class="text-blue">
                <span class="med-text">
              Kentucky has long enjoyed a competitive advantage in the provision of energy, natural gas and water.
Businesses benefit from Kentucky’s low-cost options.
                </span>
            </div>
        </div>
    </div>
</section>

<!-- quote for workforce -->
<section class="container mt-5 ">
        <div class="row">
            <div class="col-md-4 col-sm-8">
               <span class="text-big text-bold text-blue">Labor costs
                20% lower
                </span>
                <span class="text-big text-blue">national average</span>
            </div>
            <div class="col-md-2 col-sm-4">
                 <img class="img-fluid" src="/site/images/lower.jpg">

            </div>
            <div class="col-md-4 col-sm-8">
               <span class="text-big text-bold text-blue">3rd Nationally
                </span>
                <span class="text-big text-blue">utility affordability</span>
            </div>
            <div class="col-md-2 col-sm-4">
                <img class="img-fluid" src="/site/images/utility.jpg">
            </div>
</section>


<section class="container mt-5">
<hr class="margin-40">
<p>
    <strong>Utilities</strong><br>Kentucky has long enjoyed a competitive advantage in the provision of energy, natural gas, and water. Businesses benefit from Kentucky’s low-cost options.</p><a  class="btn read" href="https://cedky.com/cdn/kyedc/utilitiesinky.pdf?02032016" target="_blank" rel="noopener">Learn more about Kentucky’s Utilities</a><br><br>
<p>
<strong>Incentives</strong><br>Kentucky offers a variety of financial incentives to help offset costs and create new jobs and investment in the Commonwealth. From small business growth to large corporate investment, the Cabinet can help businesses with a variety of incentive options. We find the right solutions.</p><a class="btn read" target="_blank" href="https://ced.ky.gov/Locating_Expanding/kybizince">Learn more about our incentives here</a><br><br>
<p>
<strong>Taxes</strong><br>Taxes are an important factor when a company considers a new location or expansion. The Kentucky Department of Revenue has answers to all tax questions.</p><a class="btn read"href="//taxanswers.ky.gov" target="_blank" rel="noopener">Tax Answers Available Here</a>.<br><br>
<p>
<strong>Labor Costs</strong><br>Not only is the cost of doing business lower in Kentucky, so are labor costs. In fact, labor costs are significantly below the national average – almost 20 percent lower than the United States average.</p><a href="https://ced.ky.gov/Workforce/Workforce_Data"  target="_blank" class="btn read">Learn more about Kentucky’s workforce</a><br><br>
<p>
<strong>Cost of Living</strong><br>From housing and groceries to taxes and utility costs, you will find your money goes farther in Kentucky. In fact, according to CNBC, Kentucky has among the lowest cost of living in the nation. This means you’ll have more opportunity to enjoy Kentucky’s beautiful parks and entertainment venues.</p><a  target="_blank" class="btn read"href="https://ced.ky.gov/Locating_Expanding/Living_In_Kentucky ">Learn more about life in Kentucky</a><br><br>
</ul>
</section>




 <?php include('NKY-footer.php'); ?>