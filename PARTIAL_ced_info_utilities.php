<section class="padding-100 bg-ltr-gray text-center">
    <div class="container">
                <h2 class="clr-blue">Low Utility Costs</h2>
                <hr class="spacer-30">
                <p>Among the more significant location factors having a direct influence on bottom line costs is the annual capital that must be committed to utility consumption. Kentucky has among the lowest cost of electricity in the industrial sector in the United States, coming in at <b>18.9%</b> lower than the national average.
                </p>


        
    </div>
</section>