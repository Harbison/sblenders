<section class="col-12 col-md-9">
<h1 class="h1 clr-blue">Kentucky Entertainment Incentive (KEI) Program</h1>
<hr class="margin-40" />
<h2>The purpose of the KEI Program is to encourage:</h2>
<hr class="spacer-20" />
<ul class="singleSpace">
<li>The film and entertainment industry to choose locations in the Commonwealth for the filming and production of motion picture or entertainment productions;</li>
<li>The development of a film and entertainment industry in Kentucky;</li>
<li>Increased employment opportunities for the citizens of the Commonwealth within the film and entertainment industry; and</li>
<li>The development of a production and postproduction infrastructure in the Commonwealth for film production and touring Broadway show production facilities containing state-of-the-art technologies.</li>
</ul>
<p style="font-weight: bold;">Key Guidelines:</p>
<ul class="singleSpace">
<li>To qualify for tax incentives, the Approved Company shall incur a minimum combined total of Qualifying Expenditures and Qualifying Payroll Expenditures in Kentucky based on the type of Motion Picture or Entertainment Production.</li>
<li>Projects incurring eligible costs in enhanced incentive counties may be eligible for an increased tax credit. Incentive amounts are determined based on the Kentucky county where the Qualifying Expenditures and Qualifying Payroll Expenditures occur.</li>
<li>Projects may be eligible for up to $10 million in tax credits, with a maximum of $75 million available for all approved projects per calendar year.</li>
<li>Qualifying expenditures within the program include set construction and operations, lease or rental of property as a set location, as well as audio and visual equipment and services, in addition to other accommodations. Qualifying expenditures must be incurred with businesses located in the Commonwealth and used only for the Motion Picture or Entertainment Production filmed or produced in the Commonwealth.</li>
</ul>
<p><strong>Expenditures not eligible for consideration as Qualifying Expenditures include, but are not limited to, the following:</strong></p>
<ul class="singleSpace">
<li>Expenditures made to vendors located outside of Kentucky</li>
<li>Fringes and Per Diem</li>
<li>Payroll Service fees</li>
<li>Airfare</li>
<li>KEI application and administrative fees</li>
<li>Legal and accounting expenses</li>
<li>Contingency expenses</li>
<li>Cast and Crew screening and wrap-up parties</li>
<li>Bank, financing and completion bond fees</li>
<li>Script publication and license fees</li>
<li>Miscellaneous expenses and fees</li>
<li>Insurance Premiums</li>
<li>Loss/Damage Fees</li>
<li>Gifts</li>
<li>Alcohol/Tobacco</li>
<li>Publicity Fees</li>
<li>Non-Resident Kit/Box Rentals</li>
<li>Online Purchases</li>
</ul>
<p>&nbsp;</p>
<p style="font-weight: bold;">Tax incentives awarded through the KEI program are refundable and nontransferable and may be claimed against the Approved Company's Kentucky corporate, limited liability or individual income tax. Unused credits may not be carried forward.</p>
<p style="font-weight: bold;">&nbsp;</p>
<p style="font-weight: bold;">Additional Resources</p>
<ul class="singleSpace">
<li><a title="KEI Guidelines" href="https://cedky.com/cdn/1980_KEIguidelines.pdf">Guidelines</a></li>
<li><a title="Approved KEI Projects" href="https://cedky.com/cdn/1980_KEI_Web_Report.pdf?1-30" target="_blank" rel="noopener">Approved KEI Projects</a></li>
<li><a title="Reel Scout" href="https://ky.reel-scout.com/crew_login.aspx" target="_blank" rel="noopener">Reel Scout - Vendors and Crew</a></li>
<li><a href="https://ky.reel-scout.com/loc_results.aspx">Kentucky Location Gallery</a></li>
</ul>
<p style="font-weight: bold;">Previous Years</p>
<ul class="singleSpace">
<li><a title="2022 Approvals" href="https://cedky.com/cdn/1980_KEI_-_2022_Approvals.pdf" target="_blank" rel="noopener">2022 Approvals</a></li>
<li><a title="2023 Approvals" href="https://cedky.com/cdn/1980_KEI_-_2023_Approvals.pdf" target="_blank" rel="noopener">2023 Approvals</a></li>
<li><a title="2024 KEI Approvals" href="https://cedky.com/cdn/1980_KEI_Web_Report_2024.pdf" target="_blank" rel="noopener">2024 Approvals</a></li>
</ul>
</section>