<?php 
$title = "Workforce and Talent | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Workforce and Talent</h1>
                <p class="text-blue">
                <span class="med-text">
              Kentucky’s businesses and economy are booming because of the talented, hard-working Kentuckians who make our workforce strong. From improving training facilities, empowering apprentices, providing employers with training funding and more, we’re making sure a strong workforce remains our #1 priority in our new Kentucky home.
                </span>
           
            </div>
        </div>
    </div>
</section>

<iframe style="aspect-ratio: 16 / 9; width: 100%;" src="https://www.youtube.com/embed/7qxTvEZTtBQ?si=sPKUp5k9R3n-NmAk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


<!-- quote for workforce -->
<section class="container mt-5 light-gray">
        <div class="row">
             <div class="col-lg-5 p-3">
                <img class="img-fluid" src="/site/images/danimer.jpg">
            </div>
            <div class="col-lg-2  p-4">
                <img class="img-fluid" src="/site/images/quote.png">
            </div>
            <div class="col-lg-5 p-4">
                <span class="text-medium text-bold text-blue">Kentucky's state resources and strong local workforce have provided us witha significant leg up in getting this project off the ground.&quot; </span><br><span class="text-medium text-blue"> - Stephen Croskey, Danimer Scientific</span>
            </div>
</section>


<section class="container mt-5">
<h1 class="h1">Kentucky Emphasizes Workforce Development</h1>
<hr class="margin-40">
<p>Recognizing that a company’s workforce is its #1 priority, Kentucky is taking steps to ensure that workers in the Commonwealth are equipped with superior training and skills needed to compete in the global economy. Kentucky has aligned resources to ensure companies and individuals receive the assistance they need to be successful.</p>
<p>Download the brochure below:</p>
<a class="btn read" href="https://cedky.com/cdn/1726_09142020_Work_First_v1.4.pdf?29" target="_blank" rel="noopener">Download Brochure</a><hr class="spacer-60">
<h3>Here are examples of the progress we are making:</h3>
<hr class="spacer-20">
<p><strong>Increased funding through the Bluegrass State Skills Corporation (BSSC)</strong><br>Providing employers with more training funds to develop new and existing employees. Currently, more than 200 employers have grants and tax credits worth over $12.6 million that are training nearly 43,000 employees.</p>
<a class="btn read" href="/Workforce/BSSC">Visit Website</a><hr class="spacer-60">
<p><strong>Investing in your current workforce through KCTCS TRAINS</strong><br>With KCTCS TRAINS funding, our colleges partner with companies who invest in their workforce to develop and provide in-depth, customized employee training at a fraction of the cost, allowing companies to train, retain, and upskill more Kentucky workers.</p>
<a class="btn read" href="https://workforce.kctcs.edu/trains.aspx" target="_blank" rel="noopener">Visit Website</a><hr class="spacer-60">
<p><strong>Improving Training Facilities</strong><br>New and improved training facilities are coming online statewide, thanks to more than $200 million in additional funding. Nearly 30,000 additional workers will receive training each year.</p>
<a class="btn read" href="//educationcabinet.ky.gov/Initiatives/Work-Ready-Skills-Initiative/Pages/Work-Ready-Skills-Initiative.aspx" target="_blank" rel="noopener">Visit Website</a><hr class="spacer-60">
<p><strong>Empowering Apprentices</strong><br>Empowering apprentices through a registered apprenticeship model, combining on-the-job learning with related technical instruction. Apprentices are currently being trained in more than 100 occupations.</p>
<a class="btn read" href="//educationcabinet.ky.gov/Initiatives/apprenticeship/Pages/default.aspx" target="_blank" rel="noopener">Visit Website</a><hr class="spacer-60">
<p><strong>Encouraging Manufacturing Careers</strong><br>Increasing manufacturing careers through an innovative work/study program called KY FAME. Students split their week between college courses and on-the-job training in a high-tech manufacturing operation. After 18 months, students receive an advanced manufacturing technician (AMT) certification and can begin work or continue college.</p>
<a class="btn read" href="//kyfame.com" target="_blank" rel="noopener">Visit Website</a><hr class="spacer-60">
<p><strong>Offering Dual Credit Scholarships</strong><br>Providing tuition for high school juniors and seniors to take courses that earn high school and college credit simultaneously. In three years, the rate of enrollment for dual credit courses has increased 92 percent and the successful completion rate stands at 95 percent.</p>
<a class="btn read" href="//educationcabinet.ky.gov/Initiatives/Pages/DualCredit.aspx" target="_blank" rel="noopener">Visit Website</a><hr class="spacer-60">
<p><strong>Providing Second Chances</strong><br>Supporting job placement assistance to nonviolent offenders after they have completed their sentence to allow them to re-enter the workforce, thus lowering the state’s rate of recidivism and saving taxpayer money. Assistance includes housing, transportation and substance abuse treatment. In addition, certain low-level felons can expunge their records once they have completed their sentences.</p>
<hr class="spacer-60">
<p><strong>Equipping Offenders with Trade Skills</strong><br>Offering apprenticeship programs for adult and juvenile offenders while incarcerated to give them nationally recognized journeyman credentials in skilled trades upon their release.</p>
<hr class="spacer-60">
<p><strong>Empowering Our People</strong><br>Empowering residents to re-enter the workforce by providing training, re-employment services, job placement and employer recruitment activities at one of 12 career center hubs. More than 130,000 individuals have received career coaching and job placement services so far this year. (Kentucky Career Centers)</p>
<a class="btn read" href="//kcc.ky.gov/Pages/default.aspx">Visit Website</a><hr class="spacer-60">
<p><strong>Increasing the Workforce</strong><br>Increasing the workforce by nearly 125,000 by working with the federal government to require able-bodied Medicaid beneficiaries to participate in community engagement activities. These include education, job training, employment, caregiving or volunteering activities for at least 20 hours a week.</p>
<a class="btn read" href="//kentuckyhealth.ky.gov/Pages/index.aspx" target="_blank" rel="noopener">Visit Website</a><hr class="spacer-60">
<p><strong>Preparing Communities</strong><br>Certifying communities that document their achievement on graduation rates, community commitment, educational attainment, career readiness, registered apprenticeship and internet availability. Nearly 75 percent of all Kentucky counties are certified Work Ready or Work Ready in Progress.</p>
<a class="btn read" href="/Workforce/Work_Ready_Communities" target="_blank" rel="noopener">Visit Website</a><hr class="spacer-60"></section>




<?php include('NKY-footer.php'); ?>