<section class="col-12 col-md-9">
<h1 class="h1 clr-blue">Bluegrass State Skills Corporation</h1>
<hr class="margin-40" />
<p>The importance of a skilled workforce has never been greater. As companies think globally, it is vital that Kentucky's workforce be prepared to meet the hiring demands of employers. The Cabinet for Economic Development plays a significant role in keeping Kentucky's businesses competitive in the global economy by providing financial assistance to train their workforce. Through the Cabinet&rsquo;s Grant-in-Aid and Skills Training Investment Credit, the Kentucky Skills Network provides incentives to support Kentucky business&rsquo; efforts to help new and existing employees stay competitive through flexible, employer-driven skills-upgrade training.</p>
<p>The competitive Grant-in-Aid (GIA) provides cash reimbursements for occupational and skills upgrade training at Kentucky businesses. The Skills Training Investment Credit (STIC) offers state income tax credits for companies to offset the costs for approved training programs.</p>
<p>Businesses involved in manufacturing, agribusiness, nonretail service or technology, headquarters operations, hospital operations, coal severing and processing, alternative fuel, gasification, renewable energy production or carbon dioxide transmission pipelines may be eligible for BSSC incentives.&nbsp; Training consortia may also qualify for the GIA program.</p>
<p>Applications for both programs are accepted and considered for approval by the Bluegrass State Skills Corporation Board of Directors at scheduled meetings throughout the year.</p>
<h3>Grant-in-Aid (GIA) and Skills Training Investment Credit (STIC) Documents</h3>
<ul class="singleSpace" type="disc">
<li><a href="https://cedky.com/cdn/142_GIA_STIC_Fact_Sheet.pdf" target="_blank" rel="noopener">GIA &amp; STIC Fact Sheet</a></li>
<li><a href="https://cedky.com/cdn/142_gia_stic_guidelines.pdf" target="_blank" rel="noopener">GIA &amp; STIC Guidelines</a></li>
<li><a title="GIA - STIC Application (excel)" href="https://cedky.com/cdn/142_GIA-STIC_Application.xlsx?6_24" target="_blank" rel="noopener">GIA - STIC Application (excel)</a></li>
</ul>
<br />
<h3>BSSC Documents</h3>
<ul class="singleSpace" type="disc">
<li><a href="https://cedky.com/cdn/142_BSSC_Training_Documentation_Guide.pdf" target="_blank" rel="noopener noreferrer" data-auth="NotApplicable" data-linkindex="5">BSSC Training Documentation Guide</a></li>
<li><a href="https://cedky.com/cdn/142_Consortia_Member_Information_Form.xlsx" target="_blank" rel="noopener">Consortia Member Information Form (excel)</a></li>
</ul>
<hr class="spacer-20" />
<h6>BSSC Board Book</h6>
<hr class="spacer-20" />
<ul class="singleSpace">
<li><a title="Feb 5, 2025 BSSC Board Book" href="https://cedky.com/cdn/142_February_5_2025_BSSC_Board_Book_-_Public.pdf" target="_blank" rel="noopener">Feb 5, 2025 BSSC Board Book</a></li>
</ul>
<button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse24" aria-expanded="false" aria-controls="2024"> 2024 </button>
<button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse23" aria-expanded="false" aria-controls="2023"> 2023 </button>
<button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse22" aria-expanded="false" aria-controls="2022"> 2022 </button>
<button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse21" aria-expanded="false" aria-controls="2021"> 2021 </button>
<button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse20" aria-expanded="false" aria-controls="2020"> 2020 </button>
<button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse19" aria-expanded="false" aria-controls="2019"> 2019 </button>
<button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="2018"> 2018 </button>

<div id="collapse24" class="collapse" aria-labelledby="headingOne" data-parent="#collapse24">
	<div class="card-body">
	<p><strong>2024</strong></p>
	<ul class="singleSpace">
		<li><a title="Nov 6 BSSC Book" href="https://cedky.com/cdn/142_November_6__2024_BSSC_Board_Book.pdf" target="_blank" rel="noopener">Nov. 6, 2024 BSSC Board Book</a></li>
		<li><a href="https://cedky.com/cdn/142_August_7_2024_BSSC_Board_Book.pdf" target="_blank" rel="noopener">Aug 7, 2024 BSSC Board Book</a></li>
		<li><a href="https://cedky.com/cdn/142_May_1_2024_BSSC_Board_Book.pdf" target="_blank" rel="noopener">May 1, 2024 BSSC Board Book</a></li>
		<li><a title="Feb 7, 2024 BSSC Board Book" href="https://cedky.com/cdn/142_February_7_2024_BSSC_Board.pdf" target="_blank" rel="noopener">Feb 7, 2024 BSSC Board Book</a></li>
	</ul>

	</div>
</div>

<div id="collapse23" class="collapse" aria-labelledby="headingOne" data-parent="#collapse23">
	<div class="card-body">
	<p><strong>2023</strong></p>
	<ul class="singleSpace">
		<li><a title="November 1 2023 BSSC Board Book" href="https://cedky.com/cdn/142_November_1_2023_BSSC_Board_Book.pdf" target="_blank" rel="noopener">Nov 1, 2023 BSSC Board Book</a></li>
		<li><a title="Aug 2, 2023 BSSC Board Book" href="https://cedky.com/cdn/142_August_2,_2023_BSSC_Board_Book.pdf">Aug 2, 2023 BSSC Board Book</a></li>
		<li><a href="https://cedky.com/cdn/142_May-3-2023_BSSC_Board_Book.pdf" target="_blank" rel="noopener">May 3, 2023 BSSC Board Book</a></li>
		<li><a href="https://cedky.com/cdn/142_2023-2-1_-_BSSC_Board_Book.pdf" target="_blank" rel="noopener">Feb 1, 2023 BSSC Board Book</a></li>
	</ul>

	</div>
</div>

<div id="collapse22" class="collapse" aria-labelledby="headingOne" data-parent="#collapse22">
	<div class="card-body">
	<p><strong>2022</strong></p>
	<ul class="singleSpace">
		<li><a title="Nov 2, 2022 BSSC Board Book" href="https://cedky.com/cdn/142_November_2-2022_BSSC_Board_Book.pdf">Nov 2, 2022 BSSC Board Book</a></li>
		<li><a title="Aug 3, 2022, BSSC Board Book" href="https://cedky.com/cdn/142_August_3__2022_-_BSSC_Board_Book.pdf">Aug 3, 2022, BSSC Board Book</a></li>
		<li><a title="May 2022, BSSC Board Book" href="https://cedky.com/cdn/142_May_2022_-_BSSC_Board_Book.pdf">May 2022, BSSC Board Book</a></li>
		<li><a title="February 2022, BSSC Board Book" href="https://cedky.com/cdn/142_February_2022,_BSSC_Board_Book.pdf">February 2022, BSSC Board Book</a></li>
	</ul>

	</div>
</div>

<div id="collapse21" class="collapse" aria-labelledby="headingOne" data-parent="#collapse21">
	<div class="card-body">
	<p><strong>2021</strong></p>
	<ul class="singleSpace">
		<li><a href="https://cedky.com/cdn/142_November_2021,_BSSC_Board_Book.pdf">November 2021, BSSC Board Book</a></li>
		<li><a title="August 2021, BSSC Board Book" href="https://cedky.com/cdn/142_BSSC_Board_Book_-_August_4_2021.pdf">August 2021, BSSC Board Book</a></li>
		<li><a title="May 2021, BSSC Board Book" href="https://cedky.com/cdn/142_May_2021_BSSC_Board_Book.pdf">May 2021, BSSC Board Book</a></li>
		<li><a title="March 2021, BSSC Board Book" href="https://cedky.com/cdn/142_March_2021_BSSC_Board_Book.pdf" target="_blank" rel="noopener">March 2021, BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_January_2021.pdf?34" target="_blank" rel="noopener">January 2021 BSSC Board Book</a></li>
	</ul>

	</div>
</div>

<div id="collapse20" class="collapse" aria-labelledby="headingOne" data-parent="#collapse20">
	<div class="card-body">
	<p><strong>2020</strong></p>
	<ul class="singleSpace">
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_December_2020.pdf?34" target="_blank" rel="noopener">December 2020 BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_September_2020.pdf?34" target="_blank" rel="noopener">September 2020 BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_July_2020.pdf?34" target="_blank" rel="noopener">July 2020 BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_May_2020.pdf?34" target="_blank" rel="noopener">May 2020 BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_March_2020.pdf?34" target="_blank" rel="noopener">March 2020 BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_January_2020.pdf?34" target="_blank" rel="noopener">January 2020 BSSC Board Book</a></li>
	</ul>

	</div>
</div>

<div id="collapse19" class="collapse" aria-labelledby="headingOne" data-parent="#collapse19">
	<div class="card-body">
	<p><strong>2019</strong></p>
	<ul class="singleSpace">
	<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_December_2019.pdf?34" target="_blank" rel="noopener">December 2019 BSSC Board Book</a></li>
	<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_September_2019.pdf?34" target="_blank" rel="noopener">September 2019 BSSC Board Book</a></li>
	<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_July_2019.pdf?34" target="_blank" rel="noopener">July 2019 BSSC Board Book</a></li>
	<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_May_2019.pdf?34" target="_blank" rel="noopener">May 2019 BSSC Board Book</a></li>
	<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_March_2019.pdf?34" target="_blank" rel="noopener">March 2019 BSSC Board Book</a></li>
	<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_January_2019.pdf?34" target="_blank" rel="noopener">January 2019 BSSC Board Book</a></li>
	</ul>

	</div>
</div>

<div id="collapse18" class="collapse" aria-labelledby="headingOne" data-parent="#collapse18">
	<div class="card-body">
	<p><strong>2018</strong></p>
	<ul class="singleSpace">
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_December.pdf?34" target="_blank" rel="noopener">December 2018 BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_September.pdf?34" target="_blank" rel="noopener">September 2018 BSSC Board Book</a></li>
		<li><a class="noCache" href="https://cedky.com/cdn/kyedc/BSSC_Board_Book_August.pdf?34" target="_blank" rel="noopener">August 2018 BSSC Board Book</a></li>
	</ul>

	</div>
</div>




<hr class="spacer-40" />
<p>To view the BSSC annual reports, click below.</p>
<p><a class="btn noCache" href="http://ced.ky.gov/NewsRoom/Annual_Reports" target="_blank" rel="noopener">BSSC Annual Reports</a></p>
<hr class="spacer-60" />
<p>To view a list of training consortia/partnerships, click below.</p>
<a class="btn noCache" href="https://cedky.com/cdn/142_BSSC_Consortia_List.pdf" target="_blank" rel="noopener">Kentucky Training Consortia/Partnerships</a><hr class="spacer-60" />
<h6>Registered Apprenticeships</h6>
<hr class="spacer-20" />
<p>It&rsquo;s recommended that BSSC grant recipients consider becoming a part of Kentucky&rsquo;s registered apprenticeship program. Details are available here:</p>
<p><a class="btn noCache" href="https://educationcabinet.ky.gov/Initiatives/apprenticeship/Pages/default.aspx?34" target="_blank" rel="noopener">Registered Apprenticeships</a></p>
<hr class="spacer-20" />
<h6>Program Contacts</h6>
<hr class="spacer-20" />
<p>Kristina Slatterly<br />Commissioner<br>
Business Development<br />(502) 564-7670<br /><a href="mailto:kristina.slatterly@ky.gov">Kristina.Slatterly@ky.gov</a></p>
</section>