<section id="primary" class="col-12 col-md-9">
<h1 class="h1 clr-blue">Employment</h1>
<hr class="margin-40" />
<p>Below are the current employment opportunities available at the Kentucky Cabinet for Economic Development. Letters of interest and resumes should be submitted to the attention of Shannon MacDonald, Finance and Personnel Division Director, Old Capitol Annex, 300 W. Broadway, Frankfort, KY 40601. Candidates may also submit a resume to <a href="mailto:shannonn.macdonald@ky.gov">shannonn.macdonald@ky.gov</a>.</p>
<hr class="spacer-40" />
<h2>Job Opportunities</h2>
<hr class="spacer-20" />
<h4>Director of Program Administration - Dept. of Financial Services</h4>
<p><a class="btn btn-primary" href="https://cedky.com/cdn/1807_DFS_Director_Program_Admin_Job_Description.docx">View Description</a></p>
<!--
<h4>Workforce Business Development Project Manager</h4>
<p><a class="btn btn-primary" href="https://cedky.com/cdn/1807_Workforce_Business_Development_Project_Manager_Job_Description.docx" target="_blank" rel="noopener">View Description</a></p> 
<p><em> No job listings at the moment</em></p>-->
<h2>To Apply</h2>
Please send resume to <a href="mailto:shannonn.macdonald@ky.gov">shannonn.macdonald@ky.gov</a> or by mail:<br />Human Resources Administrator<br />Cabinet for Economic Development<br />500 Mero Street, Mayo-Underwood Building - 5th floor<br />Frankfort, KY&nbsp; 40601
<p>For any questions call 502 782 1935</p>
<br />
<h2>Internship Opportunities</h2>
<em>No listings at the moment</em></section>