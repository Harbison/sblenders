<?php 
$title = "KPDI | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Kentucky Product Development Initiative (KPDI)</h1>
				<hr class="margin-40">
				<p>&nbsp;</p>
				<p>The established Kentucky Product Development Initiative (KPDI) Program of 2024 is a statewide effort to support upgrades at industrial sites throughout the commonwealth and position Kentucky for continued economic growth. KPDI, a collaboration between the Kentucky Cabinet for Economic Development and Kentucky Association for Economic Development (KAED), includes $35 million per fiscal year in state funding toward upgrades of sites and buildings across the state.</p>
				<p>KPDI applicants, such as local governments and economic development organizations, may seek funding assistance for transformative site and infrastructure improvement projects that will generate increased economic development opportunities and job creation for Kentucky residents.</p>
				<p>Through KPDI, funding is available through a competitive application process, with each of the 120 Kentucky counties eligible for a maximum funding amount calculated based on population. There is a maximum of $2 million per county per project. Applicants are encouraged to submit regional projects, which allow available funding for multiple counties to be combined, increasing the maximum allowed for a given project</p>
				<br><br>
			</div>
		</div>
	</div>
</section>




<section>
    <div class="container my-5">
        <div class="row">
				<nav>
					<div id="nav-tab0" class="nav nav-tabs" role="tablist">
						<button id="nav-home-tab0" class="nav-link active" role="tab" type="button" data-toggle="tab" data-target="#nav-home0" aria-controls="nav-home" aria-selected="true">KPDI 2024</button> 
						<button id="nav-2022-tab0" class="nav-link" role="tab" type="button" data-toggle="tab" data-target="#nav-2022" aria-controls="nav-profile" aria-selected="false">KPDI Program of 2022</button>
					</div>
				</nav>
<div id="nav-tabContent" class="tab-content">
		<div id="nav-home0" class="tab-pane fade show active" role="tabpanel" aria-labelledby="nav-home-tab1"><!-- // 2024 KPDI -->
		<hr class="spacer-20"><hr class="spacer-20">
		<h2>KPDI 2024 Governor Address<br><br></h2>
		<center>
			<video controls="controls" width="560" height="315">
		        <source src="https://cedky.com/cdn/media/2024-kpdi-gov.mp4" type="video/mp4">
		                    Your browser does not support the video tag.
		    </video>
		</center>
		<h2>KPDI 2024 Webinar<br><br></h2>
		<center><video controls="controls" width="560" height="315">
		        <source src="https://ced.ky.gov/media/KPDI-webinar.mp4" type="video/mp4">
		        Your browser does not support the video tag.
		        </video></center>
		<p><strong><br>KPDI Fact Sheet and FAQs:</strong></p>
		<p>A fact sheet with overview of program and a FAQ are available for download below.</p>
		<ul class="singleSpace">
		<li><a title="Fact Sheet" href="https://cedky.com/cdn/11202_KPDI_Program_of_2024_Fact_Sheet-Sept_2024_FINAL-updates_Feb_2025.pdf">KPDI Fact Sheet</a></li>
		<li><a href="https://cedky.com/cdn/11202_KPDI_FAQ_document-Sept_2024_FINAL.pdf">Frequently Asked Questions</a></li>
		</ul>
		<p>&nbsp;</p>
		<p><strong>Additional Resources:</strong></p>
		<ul class="singleSpace">
		<li><a href="https://cedky.com/cdn/11202_2024_Request_for_Information_-_KPDI.xlsx">Request for Information</a></li>
		<li><a href="https://cedky.com/cdn/11202_2024_Letter_of_Intent_Template_-_KPDI.docx">Sample letter of intent</a></li>
		</ul>
		<p>&nbsp;</p>
		<p><strong>Key Dates:</strong></p>
		<ul class="singleSpace">
		<li>LOI due 10/25</li>
		<li>RFI due 11/20</li>
		</ul>
		<p>&nbsp;</p>
		<p><strong>Webinar:</strong></p>
		<p>A webinar was held on Sept 26, 2024, which provided a general overview of KPDI 2024.</p>
		<ul class="singleSpace">
		<li><a href="https://ced.ky.gov/media/KPDI-webinar.mp4">Full recording on Sept 26, 2024 Webinar</a></li>
		<li><a href="https://cedky.com/cdn/11202_KPDI_-_Kickoff_Webinar_-_09.26.24_-_FINAL.pdf">Webinar Slides</a></li>
		</ul>
		<p>&nbsp;</p>
		<p>Applications and questions about the KPDI program can be submitted to <a href="mailto:KPDI@KAEDOnline.org">KPDI@KAEDOnline.org</a>.</p>
	</div>
	<div id="nav-2022" class="tab-pane fade" role="tabpanel" aria-labelledby="nav-2022-tab0"><!-- //////// ROUND 2 --><hr class="spacer-20">
	<nav>
		<div id="nav-tab" class="nav nav-tabs" role="tablist"><button id="nav-home-tab1" class="nav-link active" role="tab" type="button" data-toggle="tab" data-target="#nav-home1" aria-controls="nav-home1" aria-selected="true">KPDI ROUND 2</button> <button id="nav-profile-tab1" class="nav-link" role="tab" type="button" data-toggle="tab" data-target="#nav-profile1" aria-controls="nav-profile1" aria-selected="false">KPDI ROUND 1</button></div>
	</nav>
<div id="nav-tabContent" class="tab-content">
<div id="nav-home1" class="tab-pane fade show active" role="tabpanel" aria-labelledby="nav-home-tab1"><!-- //////// ROUND 2 --><hr class="spacer-20"><center>
<h2 style="text-align: left;">Round 2: A message from Gov. Andy Beshear<br><br></h2>
</center><center></center><center><video controls="controls" width="560" height="315">
                                  <source src="https://ced.ky.gov/media/Big/KPDI_Round_2.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                                </video></center><hr class="margin-20">
<h2>Round 2 Webinar<br><br></h2>
<center><video controls="controls" width="560" height="315">
                                  <source src="https://ced.ky.gov/media/Big/KPDI–Technical_Webinar_March_30_2023.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                                </video></center>
<p><strong><br>KPDI Fact Sheet and FAQs:</strong></p>
<p>A fact sheet with overview of program and a FAQ are available for download below.</p>
<ul class="singleSpace">
<li><a title="Fact Sheet" href="https://cedky.com/cdn/11202_KPDI_Fact_Sheet_3-2023_FINAL.pdf">KPDI Fact Sheet</a></li>
<li><a href="https://cedky.com/cdn/11202_KPDI_FAQ_document_3-2023_FINAL.pdf">Frequently Asked Questions</a></li>
</ul>
<p>&nbsp;</p>
<p><strong>Additional Resources:</strong></p>
<ul class="singleSpace">
<li><a href="https://cedky.com/cdn/11202_2023_KPDI_Round_2_Program_Materials.pdf">Process and application instructions</a></li>
<li><a href="https://cedky.com/cdn/11202_2023_Request_for_Information_-_Kentucky_PDI.xlsx">Application</a></li>
<li><a href="https://cedky.com/cdn/11202_2023_Letter_of_Intent_Template_-_Kentucky_PDI.docx">Sample letter of intent</a></li>
</ul>
<p>&nbsp;</p>
<p><strong>Key Dates:</strong></p>
<p>April 28, 2023 Letter of intent participation deadline</p>
<p>June 23, 2023 Request For Information (RFI) Application deadline</p>
<p>&nbsp;</p>
<p><strong>Free Webinar:</strong></p>
<p>A free webinar was held on March 30, which provided a general overview of Round 2 of the program.</p>
<ul class="singleSpace">
<li><a href="https://ced.ky.gov/media/Big/KPDI%E2%80%93Technical_Webinar_March_30_2023.mp4">Full recording on March 30, 2023 Webinar</a></li>
</ul>
<p>&nbsp;</p>
<p>Applications and questions about the KPDI program can be submitted to <a href="mailto:KPDI@KAEDOnline.org">KPDI@KAEDOnline.org</a>.</p>
<!-- //////// ROUND 1 --></div>
<div id="nav-profile1" class="tab-pane fade" role="tabpanel" aria-labelledby="nav-profile-tab1"><hr class="spacer-20">
<h2 class="h2">Round 1: A message from Gov. Andy Beshear<br><br></h2>
<center><iframe title="YouTube video player" src="https://www.youtube.com/embed/lizLUZ2XrlA" width="560" height="315" frameborder="0" allowfullscreen=""></iframe></center>
<p>&nbsp;</p>
<h2 class="h2">Round 1 Webinar<br><br></h2>
<center><video controls="controls" width="560" height="315">
                                      <source src="https://ced.ky.gov/media/Big/KPDI_Meeting-03032023.mp4" type="video/mp4">
                                    Your browser does not support the video tag.
                                    </video></center>
<p><strong>KPDI Fact Sheet and FAQs:</strong></p>
<p>A fact sheet with overview of program and a FAQ are available for download below.</p>
<ul class="singleSpace">
<li><a title="Fact Sheet" href="https://cedky.com/cdn/11202_PDI_Fact_Sheet_7-2022_Final.pdf">KPDI Fact Sheet</a></li>
</ul>
<h3><br>KPDI Due Diligence and Funding Update</h3>
<ul class="singleSpace">
<li><a href="https://cedky.com/cdn/11202_03032023_KPDI_v1_website.pdf">2023-03-03 Due Diligence and Funding Update</a></li>
</ul>
<p>&nbsp;</p>
<section id="primary" class="col-12 col-md-9">
<h1 class="h1 clr-blue">Kentucky Product Development Initiative (KPDI)</h1>
<hr class="margin-40">
<div class="bg-orange clr-white btn btn-block">
<p>A webinar detailing the first round of the Kentucky Product Development Initiative (KPDI) Program of 2024 is scheduled for Thursday, September 26, at 1:30 p.m. EDT via Zoom. Participants can access the webinar <a style="color: white;" href="https://us02web.zoom.us/j/84222122945" target="_blank" rel="noopener">here</a>. No registration is required.</p>
</div>
<p>&nbsp;</p>
<p>The newly established Kentucky Product Development Initiative (KPDI) is a statewide effort to support upgrades at industrial sites throughout the commonwealth and position Kentucky for continued economic growth. KPDI, a collaboration between the Kentucky Cabinet for Economic Development and Kentucky Association for Economic Development (KAED), includes $100 million in state funding toward upgrades of sites and buildings across the state.</p>
<p>KPDI applicants, such as local governments and economic development organizations, may seek funding assistance for transformative site and infrastructure improvement projects that will generate increased economic development opportunities and job creation for Kentucky residents.</p>
<p>Through KPDI, funding is available through a competitive application process, with each of the 120 Kentucky counties eligible for a maximum funding amount calculated based on population. There is a maximum of $2 million per county per project. Applicants are encouraged to submit regional projects, which allow available funding for multiple counties to be combined, increasing the maximum allowed for a given project. <br><br></p>
<nav>
<div id="nav-tab" class="nav nav-tabs" role="tablist"><button id="nav-home-tab" class="nav-link active" role="tab" type="button" data-toggle="tab" data-target="#nav-home" aria-controls="nav-home" aria-selected="true">KPDI ROUND 2</button> <button id="nav-profile-tab" class="nav-link" role="tab" type="button" data-toggle="tab" data-target="#nav-profile" aria-controls="nav-profile" aria-selected="false">KPDI ROUND 1</button> <!--     <button class="nav-link" id="nav-contact-tab" data-toggle="tab" data-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">KPDI ROUND 3</button> --></div>
</nav>
<div id="nav-tabContent" class="tab-content">
<div id="nav-home" class="tab-pane fade show active" role="tabpanel" aria-labelledby="nav-home-tab"><!-- //////// ROUND 2 --><hr class="margin-20"><center>
<h2 style="text-align: left;">A message from Gov. Andy Beshear<br><br></h2>
</center><center></center><center><video controls="controls" width="560" height="315">
                                  <source src="https://ced.ky.gov/media/Big/KPDI_Round_2.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                                </video></center><hr class="margin-20">
<h2>Round 2 Webinar<br><br></h2>
<center><video controls="controls" width="560" height="315">
                                  <source src="https://ced.ky.gov/media/Big/KPDI–Technical_Webinar_March_30_2023.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                                </video></center>
<p><strong><br>KPDI Fact Sheet and FAQs:</strong></p>
<p>A fact sheet with overview of program and a FAQ are available for download below.</p>
<ul class="singleSpace">
<li><a title="Fact Sheet" href="https://cedky.com/cdn/11202_KPDI_Fact_Sheet_3-2023_FINAL.pdf">KPDI Fact Sheet</a></li>
<li><a href="https://cedky.com/cdn/11202_KPDI_FAQ_document_3-2023_FINAL.pdf">Frequently Asked Questions</a></li>
</ul>
<p>&nbsp;</p>
<p><strong>Additional Resources:</strong></p>
<ul class="singleSpace">
<li><a href="https://cedky.com/cdn/11202_2023_KPDI_Round_2_Program_Materials.pdf">Process and application instructions</a></li>
<li><a href="https://cedky.com/cdn/11202_2023_Request_for_Information_-_Kentucky_PDI.xlsx">Application</a></li>
<li><a href="https://cedky.com/cdn/11202_2023_Letter_of_Intent_Template_-_Kentucky_PDI.docx">Sample letter of intent</a></li>
</ul>
<p>&nbsp;</p>
<p><strong>Key Dates:</strong></p>
<p>April 28, 2023 Letter of intent participation deadline</p>
<p>June 23, 2023 Request For Information (RFI) Application deadline</p>
<p>&nbsp;</p>
<p><strong>Free Webinar:</strong></p>
<p>A free webinar was held on March 30, which provided a general overview of Round 2 of the program.</p>
<ul class="singleSpace">
<li><a href="https://ced.ky.gov/media/Big/KPDI%E2%80%93Technical_Webinar_March_30_2023.mp4">Full recording on March 30, 2023 Webinar</a></li>
</ul>
<p>&nbsp;</p>
<p>Applications and questions about the KPDI program can be submitted to <a href="mailto:KPDI@KAEDOnline.org">KPDI@KAEDOnline.org</a>.</p>
<!-- //////// ROUND 1 --></div>
<div id="nav-profile" class="tab-pane fade" role="tabpanel" aria-labelledby="nav-profile-tab"><hr class="margin-20">
<h2 class="h2">Round 1: A message from Gov. Andy Beshear<br><br></h2>
<center><iframe title="YouTube video player" src="https://www.youtube.com/embed/lizLUZ2XrlA" width="560" height="315" frameborder="0" allowfullscreen=""></iframe></center>
<p>&nbsp;</p>
<h2 class="h2">Round 1 Webinar<br><br></h2>
<center><video controls="controls" width="560" height="315">
                                      <source src="https://ced.ky.gov/media/Big/KPDI_Meeting-03032023.mp4" type="video/mp4">
                                    Your browser does not support the video tag.
                                    </video></center>
<p><strong>KPDI Fact Sheet and FAQs:</strong></p>
<p>A fact sheet with overview of program and a FAQ are available for download below.</p>
<ul class="singleSpace">
<li><a title="Fact Sheet" href="https://cedky.com/cdn/11202_PDI_Fact_Sheet_7-2022_Final.pdf">KPDI Fact Sheet</a></li>
</ul>
<h3><br>KPDI Due Diligence and Funding Update</h3>
<ul class="singleSpace">
<li><a href="https://cedky.com/cdn/11202_03032023_KPDI_v1_website.pdf">2023-03-03 Due Diligence and Funding Update</a></li>
</ul>
<p>&nbsp;</p>
<p><strong>Additional Resources:</strong></p>
<ul class="singleSpace">
<li><a href="https://cedky.com/cdn/11202_Process_and_Application_Instructions.pdf">Process and application instructions</a></li>
<li><a href="https://cedky.com/cdn/11202_Application_Request_for_Information_KPDI.xlsx">Application</a></li>
<li><a href="https://cedky.com/cdn/11202_Sample_Letter_of_Intent-_Kentucky_PDI.docx">Sample letter of intent</a></li>
</ul>
<p>&nbsp;</p>
<p><strong>Key Dates:</strong></p>
<p>August 5, 2022 - Letter of Intent deadline</p>
<p>September 16, 2022 - Applicant submission deadline</p>
<p>&nbsp;</p>
<p><strong>Free Webinars:</strong></p>
<p>Two introductory webinars highlighting the new KPDI program were held on July 14 and July 21, which provided a general overview of the program and an in-depth look at the application process.</p>
<ul class="singleSpace">
<li><a href="https://www.youtube.com/watch?v=b-5_Lk6JHys">Full recording of July 14 webinar</a></li>
<li><a href="https://cedky.com/cdn/11202_KPDI_Webinar_-_7.14.22_v2.pdf">View the presentation here</a></li>
<li><a href="https://youtu.be/FwKbfTWZt3w">Full recording of July 21 webinar</a></li>
<li><a href="https://cedky.com/cdn/11202_KPDI_-_Technical_Webinar_-_7.21.22_v2.pdf">View the presentation here</a></li>
</ul>
<p>&nbsp;</p>
<p>Applications and questions about the KPDI program can be submitted to <a href="mailto:KPDI@KAEDOnline.org">KPDI@KAEDOnline.org</a>.</p>
</div>
<!--   <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        Round 3
                  </div> --></div>
</section>
<p><strong>Additional Resources:</strong></p>
<ul class="singleSpace">
<li><a href="https://cedky.com/cdn/11202_Process_and_Application_Instructions.pdf">Process and application instructions</a></li>
<li><a href="https://cedky.com/cdn/11202_Application_Request_for_Information_KPDI.xlsx">Application</a></li>
<li><a href="https://cedky.com/cdn/11202_Sample_Letter_of_Intent-_Kentucky_PDI.docx">Sample letter of intent</a></li>
</ul>
<p>&nbsp;</p>
<p><strong>Key Dates:</strong></p>
<p>August 5, 2022 - Letter of Intent deadline</p>
<p>September 16, 2022 - Applicant submission deadline</p>
<p>&nbsp;</p>
<p><strong>Free Webinars:</strong></p>
<p>Two introductory webinars highlighting the new KPDI program were held on July 14 and July 21, which provided a general overview of the program and an in-depth look at the application process.</p>
<ul class="singleSpace">
<li><a href="https://www.youtube.com/watch?v=b-5_Lk6JHys">Full recording of July 14 webinar</a></li>
<li><a href="https://cedky.com/cdn/11202_KPDI_Webinar_-_7.14.22_v2.pdf">View the presentation here</a></li>
<li><a href="https://youtu.be/FwKbfTWZt3w">Full recording of July 21 webinar</a></li>
<li><a href="https://cedky.com/cdn/11202_KPDI_-_Technical_Webinar_-_7.21.22_v2.pdf">View the presentation here</a></li>
</ul>
<p>&nbsp;</p>
<p>Applications and questions about the KPDI program can be submitted to <a href="mailto:KPDI@KAEDOnline.org">KPDI@KAEDOnline.org</a>.</p>
</div>
<!--   <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    Round 3
              </div> -->
</div>
</div>
</div>

</div>
</div>
</div>
</section>




<?php include('NKY-footer.php'); ?>
