<div id="accordion_identifier">
  <div class="">
    <div class="">
      <h2 class="mb-0">
        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_identifier" aria-expanded="false" aria-controls="2023">
          BUTTON_TEXT
        </button>
      </h2>
    </div>
    <div id="collapse_identifier" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_identifier">
      <div class="card-body">
          <p><strong>HEADER_HEADER_HEADER</strong></p>
          <ul class="singleSpace">
            <li><a title="title_title_title" href="link_link_link" target="_blank" rel="noopener">TEXT_TEXT_TEXT</a></li>
            <li><a title="title_title_title" href="link_link_link" target="_blank" rel="noopener">TEXT_TEXT_TEXT</a></li>
          </ul>
      </div>
    </div>
  </div>
</div>