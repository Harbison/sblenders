
<footer id="footer" class="bg-dkr-blue clr-gray padding-top-100 padding-btm-20">
            
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h5 class="h5 clr-white">About the Kentucky Cabinet for<br>Economic Development</h5>
                <hr class="spacer-20">
                The Cabinet for Economic Development is the primary state agency in Kentucky responsible for encouraging job creation and retention, and new investment in the state.
                <hr class="spacer-20">
                <a class="btn sm" href="https://ced.ky.gov/Home/AboutUs/">Read More</a>
                <hr class="spacer-60">
            </div>
            <div class="col-12 col-md-6">
                <h5 class="h5 clr-white">Quick Links</h5>
                <hr class="spacer-20">
                <ul class="quickLinks">



                        <li><a href="https://properties.zoomprospector.com/kentucky?page=1&amp;s%5BSortDirection%5D=true&amp;s%5BSortBy%5D=featured&amp;s%5BIsBuilding%5D=false">Available Sites &amp; Buildings</a></li>
                        <li><a href="https://ced.ky.gov/Entrepreneurship">Start/Grow Business</a></li>
                        <li><a href="https://ced.ky.gov/Workforce">Workforce Training</a></li>
                        <li><a href="https://ced.ky.gov/Locating_Expanding/Financial_Incentives">Financial Incentives</a></li>
                        <li><a href="https://ced.ky.gov/KYFacts">Search Statistics</a></li>
                        <li><a href="https://ced.ky.gov/International/Exports">Export My Products</a></li>
                        <li><a href="https://ced.ky.gov/Entrepreneurship/Access_To_Capital">Capital</a></li>
                        <li><a href="https://ced.ky.gov/Entrepreneurship/Investor_Information">Investor Information</a></li>
                        <li><a href="https://ced.ky.gov/Home/ContactUs">Contact Us</a></li>


                </ul>
            </div>
        </div>
        <hr class="spacer-40">
        <hr class="margin-30 bg-gray opake">
        <ul class="footerBtmLinks">
            <li><a href="https://ced.ky.gov/Home/Transparency">Transparency</a></li>           
            <li><a href="https://ced.ky.gov/Home/Accessibility">Accessibility</a></li>
            <li><a href="https://ced.ky.gov/Home/Privacy">Privacy</a></li>
            <li><a href="https://ced.ky.gov/Home/Disclaimer">Disclaimer</a></li>
        </ul>
        <ul class="socialIcons">
            <li class="fb track" data-label="Footer social FB (_Footer) ced.ky.gov/"><a href="https://www.facebook.com/CEDkygov/" target="_blank"></a></li>
            <li class="tw track" data-label="Footer social TW (_Footer) ced.ky.gov/"><a href="https://twitter.com/CEDkygov" target="_blank"></a></li>
            <li class="li track" data-label="Footer social Linked IN (_Footer) ced.ky.gov/"><a href="https://www.linkedin.com/company/cedkygov/" target="_blank"></a></li>
            <li class="ig track" data-label="Footer social INStagram (_Footer) ced.ky.gov/"><a href="https://www.instagram.com/cedkygov/" target="_blank"></a></li>
            
        </ul>
        <div class="clear"></div>
        <hr class="spacer-20">
        <div class="text-center">
            <span class="txt-xs">
                © Copyright 2024 Kentucky Cabinet for Economic Development |
                    <span class="track" data-label="Footer address (_Footer) ced.ky.gov/">500 Mero Street, 5th floor | Frankfort, KY 40601 | 502 - 564 - 7670 (local) | 800 - 626 - 2930 (toll free)</span>
            </span>
        </div>
    </div>
</footer>

 <!-- Modal -->
<div class="modal fade" id="contact_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contact Us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="ced-form-body" class="form-group">
               
                    <fieldset id="ced_inquiry_form">
                        <input type="hidden" name="request_subject" id="request_subject" value="New Kentucky Home Form Submission">
                     

                        <label for="name">Contact Name</label>
                        <input type="text" name="contact_name" id="contact_name" value="" class="text form-control">

                        <label for="company">Company</label>
                        <input type="text" name="company" id="company" value="" class="text form-control">

                        <label for="phone">Phone</label>
                        <input type="text" name="contact_phone" id="contact_phone" value="" class="text form-control">

                        <label for="email">Email</label>
                        <input type="text" name="contact_email" id="contact_email" value="" class="text form-control">

                        <label for="how">How can we assist?</label>
                        <textarea name="how_can_we_help" class="textarea form-control"></textarea>
                        <input type="hidden" name="form_id" value="ab04903e56">
                        <br>
                        

        </div>

      </div>
      <div class="modal-footer">
        <input type="button" value="Submit Response" id="submitCedInquiry" class="btn btn-primary btn-block">
        </fieldset>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

        
        <script src="site/lib/jquery/dist/jquery.min.js"></script>
        <script src="site/js/nky.js"></script>

        <!-- slick -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>

        <script src="https://ced.ky.gov/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://ced.ky.gov/js/site.min.js?v=ZmQ_N_FX68qvcxZKhGQCygiurLsPPvwh2Q_hr_SSEK0"></script>

          <!-- Moment -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>

         <!-- INDEX NEWS ITEMS -->
            <script>$.ajax({
                method: "GET",
                url: "https://cedky.com/api/3234235674/articles/nkynews-0-3"
            })
                .done(function (data) {
                    var str = '';
                    var lnk = '';
                    for (const el of data) {
                        // console.log(el.post_link);
                        // console.log(typeof el.post_link);

                        if (el.post_link !== "") {
                            lnk = el.post_link;
                        } else {
                            lnk = "https://ced.ky.gov/Newsroom/NewsPage/" + el.post_slug;
                        }
                       // lnk = el.post_link;

                        var newsdate = moment(el.post_publish_date).format('MMMM DD, YYYY');


                        let newsimg = el.post_img ?? '/site/images/teamky.jpg';

                 str += " \
                 <div class='row'>\
                    <div class='col-md-4 news_img_div'>\
                        <a target='_blank' href='" + lnk + "'><img src='"+ newsimg +"' class='img-fluid news_img' /></a>\
                    </div>\
                    <div class='col-md-8'>\
                        <span class='news_source'>" + newsdate + "</span>\
                        <a target='_blank' href='" + lnk + "'>" + el.post_title + "</a>\
                    </div>\
                </div>\
                <hr class='spacer-25'>";



                    }

                    $("#MainContent_NewsPanel").html(str);
                });</script>




