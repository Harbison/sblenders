<?php $ced_title = 'CED Annual Reports'; ?>
<?php include('PARTIAL_ced_header.php'); ?>
<?php include('PARTIAL_ced_newsroom.php'); ?>

<section class="col-12 col-md-9">
<h1 class="h1 clr-blue">Annual Reports</h1>
<hr class="margin-40" />
<p>Pursuant to KRS 154.12-2035, the Cabinet for Economic Development (CED) must report annually on certain program activity to the Governor and the Legislative Research Commision by November 1st of each fiscal year. These annual reports are available below.</p>
<p>&nbsp;</p>

<div class="annualReports">
	<h4 class="h4">Bluegrass State Skills Corporation (BSSC)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 BSSC Annual Report" href="https://cedky.com/cdn/141_BSSC_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_bssc">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_bssc" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_bssc" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_bssc">
	      <div class="card-body">
	          <p><strong>Previous BSSC Reports</strong></p>
	          <ul class="singleSpace">
	            <li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_BSSC_Annual_Report_2023.pdf">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_BSSC_Annual_Report_2022.pdf">2022 Annual Report</a></li>
				<li><a href="https://cedky.com/cdn/141_4_-_BSSC_Annual_Report_2021.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_BSSC_Annual_Report_v13.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_BSSC_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_BSSC_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_BSSC_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
	          </ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">BSSC Audited Financial Statements</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 BSSC Audited Financial Statement" href="https://cedky.com/cdn/141_2024_BSSC_Audited_Financial_Statement.pdf" target="_blank">2024 BSSC Audited Financial Statement</a></li>
	</ul>
	<div id="accordion_bssc_audited">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_bssc_audited" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_bssc_audited" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_bssc_audited">
	      <div class="card-body">
	          <p><strong>Previous BSSC Audited Financial Statements</strong></p>
	          <ul class="singleSpace">
	          	<li><a title="2023 BSSC Audited Financial Statement" href="https://cedky.com/cdn/141_2023_BSSC_Audited_Financial_Statement.pdf">2023 BSSC Audited Financial Statement</a></li>
				<li><a title="2022 BSSC Audited Financial Statement" href="https://cedky.com/cdn/141_BSSC_Financial_Statement.pdf">2022 BSSC Audited Financial Statement</a></li>
				<li><a href="https://cedky.com/cdn/141_2021_BSSC_Audited_Financial_Statements_June_30,_2021_1_.pdf">2021 BSSC Audited Financial Statement</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_BSSC_Audited_Financial_Statement.pdf?7" target="_blank" rel="noopener">2020 BSSC Audited Financial Statement</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_BSSC_Audited_Financial_Statement.pdf?7" target="_blank" rel="noopener">2019 BSSC Audited Financial Statement</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_BSSC_Audited_Financial_Statement.pdf?7" target="_blank" rel="noopener">2018 BSSC Audited Financial Statement</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_BSSC_Audited_Financial_Statements.pdf?7" target="_blank" rel="noopener">2017 BSSC Audited Financial Statements</a></li>
	          </ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">CED Active Projects</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 Ced active Projects" href="https://cedky.com/cdn/141_CED_Active_Projects_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_ced_active">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_ced_active" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_ced_active" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_ced_active">
	      <div class="card-body">
	          <p><strong>Previous CED Active Project Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_All_Active_Projects_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_FI_search_results_Active_FY22.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_9_-_FI_search_results_Active_FY21.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_CED_Active_Projects.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_KEDFA_Active_Annual_Report.pdf" target="_blank">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_CED_Active_Projects.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_CED_Active_Projects.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
	          </ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">CED Projects Approved</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 Ced Approved Projects" href="https://cedky.com/cdn/141_CED_Approved_Projects_2023-24.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_ced_approved">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_ced_approved" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_ced_approved" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_ced_approved">
	      <div class="card-body">
	          <p><strong>Previous CED Approved Project Reports</strong></p>
	          <ul class="singleSpace">
<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_All_Approved_Projects_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_FI_search_results_Approved_Projects_FY22.pdf">2022 Annual Report</a></li>
<li><a class="noCache" href="https://cedky.com/cdn/141_10_-_FI_search_results_Approved_Projects_FY21.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_CED_Approved_Projects.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_KEDFA_Approved_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_CED_Approved_Projects.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_CED_Approved_Projects.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Economic Development Fund (EDF) Program</h4>
	<p><i>formerly Economic Development Bond</i></p>
	 <ul class="singleSpace">
	    <li><a title="2024 EDF Projects" href="https://cedky.com/cdn/141_EDF_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_edf">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_edf" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_edf" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_edf">
	      <div class="card-body">
	        <p><strong>Previous EDF Reports</strong></p>
	        <ul class="singleSpace">
				<li><a title="2023 EDF Annual Report" href="https://cedky.com/cdn/141_EDF_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 EDF Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_EDF_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_5_-_2021_EDF_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_EDB_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_EDB_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_EDB_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_EDB_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Government Resources Accelerating Needed Transformation (GRANT) Program of 2024</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 GRANT Projects" href="https://cedky.com/cdn/141_GRANT_Program_of_2024_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
</div>

<div class="annualReports">
	<h4 class="h4">Incentives for Energy-Related Business Act (IEBA)</h4>
	<p><i>formerly Incentives for Energy Independence Act</i></p>
	 <ul class="singleSpace">
	    <li><a title="2024 IEBA Projects" href="https://cedky.com/cdn/141_IEBA_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_ieba">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_ieba" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_ieba" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_ieba">
	      <div class="card-body">
	        <p><strong>Previous IEBA Reports</strong></p>
	        <ul class="singleSpace">
				<li><a title="2023 IEDBA Annual Report" href="https://cedky.com/cdn/141_IEDBA_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 IEDBA Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_IEIA_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_8_-_2021_IEIA_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_IEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_IEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_IEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_IEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
			 </ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky County Population Ranking Report</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KY County Population Report" href="https://cedky.com/cdn/141_Ky_County_Population_Ranking_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Economic Development Finance Authority Loan and Grant Program</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KY County Population Report" href="https://cedky.com/cdn/141_KEDFA_Loans_and_Grants_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
</div>

<div class="annualReports">
	<h4 class="h4">KEDFA Audited Financial Statements</h4>
	 <ul class="singleSpace">
	 	 
	    <li><a title="2024 KEDFA Audited Financial Statement" href="https://cedky.com/cdn/141_KEDFA_Report-2024.pdf" target="_blank">2024 Audited Financial Statement</a></li>
	</ul>
	<div id="accordion_kedfa_audited">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_kedfa_audited" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_kedfa_audited" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_kedfa_audited">
	      <div class="card-body">
	          <p><strong>Previous KEDFA Audited Financial Statements</strong></p>
	          <ul class="singleSpace">
				<li><a title="https://cedky.com/cdn/141_KEDFA_Financials_2023.pdf" href="https://cedky.com/cdn/141_KEDFA_Financials_2023.pdf" target="_blank" rel="noopener">2023 KEDFA Audited Financial Statements</a></li>
				<li><a title="2022 KEDFA Audited Financial Statements" href="https://cedky.com/cdn/141_KEDFA-Issued_Report.pdf">2022 KEDFA Audited Financial Statements</a></li>
				<li><a title="https://cedky.com/cdn/141_KEDFA_Audited_Financial_Statement.pdf" href="https://cedky.com/cdn/141_KEDFA_Audited_Financial_Statement.pdf">2021 KEDFA Audited Financial Statements</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_KEDFA_Audited_Financial_Statements.pdf?7" target="_blank" rel="noopener">2020 KEDFA Audited Financial Statements</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_KEDFA_Audited_Financial_Statements.pdf?7" target="_blank" rel="noopener">2019 KEDFA Audited Financial Statements</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_KEDFA_Audited_Financial_Statements.pdf?7" target="_blank" rel="noopener">2018 KEDFA Audited Financial Statements</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_KEDFA_Audited_Financial_Statements.pdf?7" target="_blank" rel="noopener">2017 KEDFA Audited Financial Statements</a></li>
 			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Enterprise Initiative Act (KEIA)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KEIA Report" href="https://cedky.com/cdn/141_KEIA_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_keia">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_keia" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_keia" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_keia">
	      <div class="card-body">
	          <p><strong>Previous KEIA Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_KEIA_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_KEIA_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_7_-_2021_KEIA_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_KEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_KEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_KEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_KEIA_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Investment Fund Act (KIFA)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KIFA Report" href="https://cedky.com/cdn/141_KIFA_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_kifa">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_kifa" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_kifa" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_kifa">
	      <div class="card-body">
	          <p><strong>Previous KIFA Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_KIFA_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_KIFA_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_6_-_2021_KIFA_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_KIFA_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_KIFA_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_KIFA_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_KIFA_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Product Development Initiative (KPDI) Program of 2022</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KPDI Report" href="https://cedky.com/cdn/141_KPDI_Program_of_2022_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Product Development Initiative (KPDI) Program of 2024</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KPDI Report" href="https://cedky.com/cdn/141_KPDI_Program_of_2024_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Reinvestment Act (KRA)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KRA Report" href="https://cedky.com/cdn/141_KRA_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_kra">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_kra" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_kra" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_kra">
	      <div class="card-body">
	          <p><strong>Previous KRA Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_KRA_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_KRA_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_15_-_2021_KRA_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Rural Hospital Loan Program (KRHLP)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KRA Report" href="https://cedky.com/cdn/141_KRHLP_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_KRHLP">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_KRHLP" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_KRHLP" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_KRHLP">
	      <div class="card-body">
	          <p><strong>Previous KRHLP Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_Rural_Hospital_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_Rural_Hospital_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_14_-_2021_Rural_Hospital_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_Rural_Hospital_Annual_Report.pdf" target="_blank">2020 Annual Report</a></li>
			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Office of Entrepreneurship and Innovation</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 Office of Entrepreneurship Report" href="https://cedky.com/cdn/141_OEI_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_OEI">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_OEI" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_OEI" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_OEI">
	      <div class="card-body">
	          <p><strong>Previous OEI Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_OOE_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_OOE_Annual_Report_2022.pdf">2022 Annual Report</a></li>
				<li><a href="https://cedky.com/cdn/141_KY_Innovation_Annual_Report_2021_v7.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_OOE_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_OOE_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="http://cedky.com/cdn/kyedc/AnnualReports/2018_OOE_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="http://cedky.com/cdn/kyedc/AnnualReports/2018_OOE_KY_New_Energy_Venture_Fund.pdf?7" target="_blank" rel="noopener">2018 New Energy Venture Fund</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_OOE_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_OOE_KY_New_Energy_Venture_Fund.pdf?7" target="_blank" rel="noopener">2017 New Energy Venture Fund</a></li>
			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Tax Increment Financing (TIF)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 TIF Report" href="https://cedky.com/cdn/141_TIF_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_TIF">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_TIF" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_TIF" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_TIF">
	      <div class="card-body">
	          <p><strong>Previous TIF Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_TIF_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_TIF_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_11_-_2021_TIF_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_TIF_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_TIF_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_TIF_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_TIF_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
				</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>


<h1 class="h1 clr-blue">Inactive Reports</h1>
<hr class="margin-40" />
<p>Below are reports for previous programs that are no longer active, required and/or may be included in another report.</p>

<div class="annualReports">
	<h4 class="h4">Kentucky Angel Investment Annual Report</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KAIA Report" href="https://cedky.com/cdn/141_OEI_Annual_Report_2024.pdf" target="_blank">Future information is included in the Office of Entrepreneurship and Innovation report</a></li>
	</ul>
	<div id="accordion_KAIA">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_KAIA" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_KAIA" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_KAIA">
	      <div class="card-body">
	          <p><strong>Previous KAIA Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_Angel_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_Angel_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_12_-_2021_Angel_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_Angel_Investment_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_KY_Angel_Investment_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_KY_Angel_Investment_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_KY_Angel_Investment_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
				</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="annualReports">
	<h4 class="h4">Kentucky Small Business Credit Initiative (KSBCI)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 KSBCI Report" href="https://cedky.com/cdn/141_OEI_Annual_Report_2024.pdf" target="_blank">Future information is included in the Office of Entrepreneurship and Innovation report</a></li>
	</ul>
	<div id="accordion_KSBCI">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_KSBCI" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_KSBCI" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_KSBCI">
	      <div class="card-body">
	          <p><strong>Previous KSBCI Reports</strong></p>
	          <ul class="singleSpace">
				<li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_KSBCI_Annual_Report_2023.pdf" target="_blank" rel="noopener">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_2022_KSBCI_Annual_Report.pdf">2022 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/141_13_-_2021_KSBCI_Annual_Report.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_KSBCI_Annual_Report.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_KSBCI_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_KSBCI_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_KSBCI_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
			</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>


<div class="annualReports">
	<h4 class="h4">Agricultural Warehousing Sites Cleanup</h4> 
	<div id="accordion_AgriculturalWarehousing">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_AgriculturalWarehousing" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_AgriculturalWarehousing" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_AgriculturalWarehousing">
	      <div class="card-body">
	          <p><strong>Previous Agricultural Warehousing Reports</strong></p>
	          <ul class="singleSpace">
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_Ag_Warehousing_Site_Clean_Up_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_Ag_Warehousing_Site_Clean_Up_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
				</ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>


<hr />
<p>Please see the searchable database for current information regarding project activity at any time below.</p>
<a class="btn" href="https://fisearch.ced.ky.gov/" target="_blank" rel="noopener">Financial Incentives Database</a>



</section>









<?php include('PARTIAL_ced_footer.php'); ?>