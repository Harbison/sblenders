
var counties = [
   'All Kentucky Counties',
   'All Lenders',
	'Adair',
	'Allen',
	'Anderson',
	'Ballard',
	'Barren',
	'Bath',
	'Bell',
	'Boone',
	'Bourbon',
	'Boyd',
	'Boyle',
	'Bracken',
	'Breathitt',
	'Breckinridge',
	'Bullitt',
	'Butler',
	'Caldwell',
	'Calloway',
	'Campbell',
	'Carlisle',
	'Carroll',
	'Carter',
	'Casey',
	'Christian',
	'Clark',
	'Clay',
	'Clinton',
	'Crittenden',
	'Cumberland',
	'Daviess',
	'Edmonson',
	'Elliott',
	'Estill',
	'Fayette',
	'Fleming',
	'Floyd',
	'Franklin',
	'Fulton',
	'Gallatin',
	'Garrard',
	'Grant',
	'Graves',
	'Grayson',
	'Green',
	'Greenup',
	'Hancock',
	'Hardin',
	'Harlan',
	'Harrison',
	'Hart',
	'Henderson',
	'Henry',
	'Hickman',
	'Hopkins',
	'Jackson',
	'Jefferson',
	'Jessamine',
	'Johnson',
	'Kenton',
	'Knott',
	'Knox',
	'LaRue',
	'Laurel',
	'Lawrence',
	'Lee',
	'Leslie',
	'Letcher',
	'Lewis',
	'Lincoln',
	'Livingston',
	'Logan',
	'Lyon',
	'Madison',
	'Magoffin',
	'Marion',
	'Marshall',
	'Martin',
	'Mason',
	'McCracken',
	'McCreary',
	'McLean',
	'Meade',
	'Menifee',
	'Mercer',
	'Metcalfe',
	'Monroe',
	'Montgomery',
	'Morgan',
	'Muhlenberg',
	'Nelson',
	'Nicholas',
	'Ohio',
	'Oldham',
	'Owen',
	'Owsley',
	'Pendleton',
	'Perry',
	'Pike',
	'Powell',
	'Pulaski',
	'Robertson',
	'Rockcastle',
	'Rowan',
	'Russell',
	'Scott',
	'Shelby',
	'Simpson',
	'Spencer',
	'Taylor',
	'Todd',
	'Trigg',
	'Trimble',
	'Union',
	'Warren',
	'Washington',
	'Wayne',
	'Webster',
	'Whitley',
	'Wolfe',
	'Woodford'
	];

var ContentComp = { 
   name: 'content-comp',
   data: function() {
      return {
         glenders: [],
         counties: counties,
         selectedType: 'All Lenders'
      }
   },
   computed: {
     filteredLenders() {
         if (this.selectedType === 'All Lenders') {
               return this.glenders;

         } else {
               var needle = this.selectedType;
               var filter = this.glenders.filter(function(lender) {
                  if (lender.service_area === 'All Kentucky Counties') {
                       return true;
                  } else if (lender.service_area.indexOf(needle) !== -1) {
                    return true ;
                  }
              });
              return filter;
           }
         }
   },
   methods: {
     selectType(target) {
       this.selectedType = target.value;
     },
      getLenders(){
         var self = this;

         var url  = "https://cedky.localhost/api/4294967295/data/sblenders";
         var xhr  = new XMLHttpRequest()
         xhr.open('GET', url+'/1', true)
         xhr.onload = function () {
            var raw = JSON.parse(xhr.responseText);

            if (xhr.readyState == 4 && xhr.status == "200") {
               
               self.glenders = JSON.parse(raw.data_content);
         
            } else {
               console.error(raw);
            }
         }
         xhr.send(null);


       }
   },
   mounted: function(){
      this.getLenders();
   },
   template: '<div>' +
     '<div class="filter" >' + 
         'Filter by <b>Service Area</b>:' + 
         '<select name="LeaveType" @change="selectType($event.target)" class="form-control" v-model="selectedType">' +
         '<option v-for="county in counties" :value="county">{{county}}</option>' +
         '</select>' +  
         '</div>' +
         '<div v-for="lender in filteredLenders" :key="lender.lender_name" class="row-striped"><div class="p-3">' +
            '<b>{{ lender.lender_name }}</b><br>' +
            '{{ lender.contact_name }}<br>' +
            '{{ lender.contact_email }}<br>' +
            '{{ lender.add_1 }}<br>' +
            '{{ lender.city_state_zip }}<br>' +
            'Phone {{ lender.phone }}<br>' +
            'Fax {{ lender.fax }}<br><br>' +
            '<b>Service Area: </b>{{ lender.service_area }}<br><br>' +
         '</div></div>' +
      '</div>'
}

new Vue({
 el: '#vue-app',
 components: {
   'content-comp': ContentComp
 }
})


