<?php
	session_start();
	include('inc.access.php');
	include($_SERVER['DOCUMENT_ROOT'] . '/cmsconfig.php');
	$Link = mysqli_connect(DB_HOST, DB_USER, DB_PWD);
	mysqli_select_db($Link,DB_NAME);

	if (!checkSession(1)){
		echo "<script>
				parent.opener.window.location.href = parent.opener.window.location.href;
				window.close();
				</script>";
		session_write_close();
		exit();
	}
	session_write_close();

	if ($_POST){
		$id = intval($_POST['id']);
		switch ($_POST['action']){
			case 'editblocktitle':
				$title = mysqli_escape_string($Link,stripcslashes($_POST['blocktitle']));
				$query = "UPDATE blocks set title='$title' where blockid='$id'";
				mysqli_query($Link,$query);
				break;
			case 'editsubtitle':
				$title = mysqli_escape_string($Link,stripcslashes($_POST['subblocktitle']));
				$query = "UPDATE subblocks set title='$title' where subid='$id'";
				mysqli_query($Link,$query);
				break;
			case 'editsub':
				$content = stripcslashes($_POST['subcontent']);
				if (empty($_POST['popup'])? $popup = 0 : $popup = 1); 
		
imghunt_after();  
				$content = mysqli_escape_string($Link,$content);
				$query = "UPDATE subblocks set content='$content', popup=$popup where subid='$id'";
				mysqli_query($Link,$query);
				break;
		}

		mysqli_close($Link);
		
		echo "<script>
				parent.opener.window.location.href = parent.opener.window.location.href;
				window.close();
				</script>";
		exit();
	}


	// IMAGE PROCESSING FUNCTIONS................

	function imghunt_before(){
		global $content;
		$pattern_img = "/<img.*?>/";
		preg_match_all($pattern_img, $content, $imgmatches);

		$pattern_width = "/ width=\"\d+\"/";
		$pattern_height = "/ height=\"\d+\"/";
		$pattern_src = "/ src=\".*?\"/";

		foreach ($imgmatches[0] as $imgtag){
			preg_match($pattern_width, $imgtag, $matches);
			$width = substr(substr($matches[0], 0, -1), 8);

			preg_match($pattern_height, $imgtag, $matches);
			$height = substr(substr($matches[0], 0, -1), 9);

			preg_match($pattern_src, $imgtag, $matches);		// extract the entire src attribute
			// removes everything but filename
			$src =  substr(substr($matches[0], strrpos($matches[0], '/') + 2), 0, -1);		

			echo "<input type=\"hidden\" name=\"imgs_before[$src]\" value=\"$width|$height\">\n";
		}
	}


	function imghunt_after(){
		global $content, $popup;
		
		$pattern_img = "/<img.*?>/i";
		preg_match_all($pattern_img, $content, $imgmatches);

		$pattern_width = "/ width=\"\d+\"/";
		$pattern_height = "/ height=\"\d+\"/";
		$pattern_src = "/ src=\".*?\"/";
		$pattern_align = "/ align=\".*?\"/";
		$pattern_style = "/ style=\".*?\"/";

		$imgs_after = array();

		foreach ($imgmatches[0] as $imgtag){
			preg_match($pattern_width, $imgtag, $matches);
			$width = substr(substr($matches[0], 0, -1), 8);

			preg_match($pattern_height, $imgtag, $matches);
			$height = substr(substr($matches[0], 0, -1), 9);

			preg_match($pattern_src, $imgtag, $matches);
			// removes everything but filename
			$src =  substr(substr($matches[0], strrpos($matches[0], '/') + 2), 0, -1);		

			preg_match($pattern_style, $imgtag, $matches);
			$style = substr(substr($matches[0], 0, -1), 8);

			$imgs_after[$src] = array($width, $height);
			
			if ($_POST['imgs_before'][$src]){  // the image exists -- check dimensions
				$content_img = 'c'.$src;
				list($before_width, $before_height) = explode('|', $_POST['imgs_before'][$src]);

				if (!empty($_POST['original_size'])){	// make all images their original sizes
					list($raw_width, $raw_height) = getimagesize(IMG_VAULT . $src);	// get dimensions from raw image
					if ($raw_width != $width || $raw_height != $height){	// are the sizes different?
						// resize it to the raw dimensions
						shell_exec("convert " . IMG_VAULT . "$src -resize {$raw_width}x{$raw_height} -quality 80 " . IMG_VAULT . "$content_img");
						if ($popup)
							$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$raw_width\" height=\"$raw_height\" align=\"$align\" style=\"$style cursor: pointer;\" onclick=\"img_popup(this, '$src')\">", $content);
						else
							$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$raw_width\" height=\"$raw_height\" align=\"$align\" style=\"$style\">", $content);
					}
				}

				else if ($before_width != $width || $before_height != $height){ // image was resized
$content_img = 'c'.$src;
					shell_exec('convert ' . IMG_VAULT . "$src -resize {$width}x{$height} -quality 80 " . IMG_VAULT . "$content_img"); // resize
					list($new_width, $new_height) = getimagesize(IMG_VAULT . $content_img);  // get new dimensions -- see note 1
$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$new_width\" height=\"$new_height\" align=\"$align\" onclick=\"img_popup(this, '$src')\">", $content);
					if ($popup)
						$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$new_width\" height=\"$new_height\" align=\"$align\" style=\"$style cursor: pointer;\" onclick=\"img_popup(this, '$src')\">", $content);
					else
						$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$new_width\" height=\"$new_height\" align=\"$align\" style=\"$style\">", $content);
				}

				else {	//	existing image but no resize
					if ($popup)
						$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$width\" height=\"$height\" align=\"$align\" style=\"$style cursor: pointer;\" onclick=\"img_popup(this, '$src')\">", $content);
					else
						$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$width\" height=\"$height\" align=\"$align\" style=\"$style\">", $content);
				}
			}

			else {	// this image is new
				$content_img = 'c'.$src;

				if ($_POST['original_size'])	// if user selected 'maintain original size'
					list($width, $height) = getimagesize(IMG_VAULT . $src);	// get dimensions from uploaded (raw) image
				else if (!$width || !$height) // if no initial resize, assume thumbnail size
					list($width, $height) = getimagesize(IMG_VAULT . 't' . $src);

				shell_exec("convert " . IMG_VAULT . "$src -resize {$width}x{$height} -quality 80 " . IMG_VAULT . "$content_img");
				list($new_width, $new_height) = getimagesize(IMG_VAULT . $content_img);  // get new dimensions -- 
				// ImageMagick's Convert automatically constrains the image to a rectangle, so we use the sizes it computes
		$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$new_width\" height=\"$new_height\" align=\"$align\" onclick=\"img_popup(this, '$src')\" style=\"$style\">", $content);
				if ($popup)
					$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$new_width\" height=\"$new_height\" align=\"$align\" style=\"$style cursor: pointer;\" onclick=\"img_popup(this, '$src')\">", $content);
				else
					$content = str_replace($imgtag, "<img src=\"/imagevault/$content_img\" width=\"$new_width\" height=\"$new_height\" align=\"$align\" style=\"$style\">", $content);
			}
		}
		if (!empty($_POST['imgs_before'])) {
			foreach ($_POST['imgs_before'] as $src => $info){  // look for deleted images
				if (!$imgs_after[$src]){	// if img_before is not present after, it was deleted
					$content_img = 'c'.$src;				
					if (is_file(IMG_VAULT . $content_img)){
						@unlink(IMG_VAULT . $content_img);
					}
				}
			}
		}
	}


	//// BUILD LINK LIST
	function linkList(){
		$Link = mysqli_connect(DB_HOST, DB_USER, DB_PWD);
		mysqli_select_db($Link,DB_NAME);

		//$sql = "SELECT CONCAT(virtualfile, '.html') as value, linkname as title FROM pages ORDER BY linkname";
		//UNION 
		//SELECT CONCAT('/imagevault/f', filename, '.', extension) as value, `desc` as title FROM filebin ORDER BY desc

		$sql = "SELECT CONCAT(p.virtualfile, '.html') as value, p.linkname as title FROM pages as p
			UNION SELECT CONCAT('/imagevault/f', f.filename, '.', f.extension) as value, f.desc as title FROM filebin as f";
		$results = mysqli_query($Link, $sql);

		$assets = [];
			
		while ($row = $results->fetch_assoc()) {
		    $assets[] = $row;
		}

		return $assets;

	}
	
	// Decide what type of editor to display (subblock title, subblock content, etc.)

	if ($_GET['action'] == 'editsub'){
		$subid = intval($_GET['subid']);
		$query = "select content, popup FROM subblocks where subid='$subid'";
		$result = mysqli_query($Link,$query);
		$row = mysqli_fetch_assoc($result);
		mysqli_close($Link);
		$content = $row['content'];
		$popup = $row['popup'];
?>


<html>
<head>
 <title>Content Editorz2</title>
<script src="tinymce/tinymce.min.js"></script>
<script>
function green(it){
	it.style.background = "url('/cms/images/greengradient.jpg')";
}


var links3dd = <?php echo json_encode(linkList()); ?>;

// var tinyMCELinkList = [
//     // Name, URL
// 	{title: 'LINK !', value: 'https://www.tiny.cloud'},
// 	{title: 'My page 2', value: 'https://about.tiny.cloud'}
// ];


  tinymce.init({
    selector: '#subcontent',
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste fontsize"
    ],
    fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
	height: 350,
	extended_valid_elements: "*[*]",
	image_advtab: true,
	plugins : 'advlist autolink link image lists charmap print preview code paste',
    link_list: links3dd,
    paste_data_images: true,
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image fontsizeselect"


  });
	


</script>

</head>
<style>

body{
	font-family: sans-serif;
	font-size: 8pt;
	background-color: #F0F0EE;
	margin: 0px;
	}

#vault{
	border: 0px;
	border-bottom: 1px solid #cccccc;
	width: 100%;
	height: 240px;
	overflow: auto;
	margin-bottom: 5px;
	}

.button{
	cursor: pointer;
	margin-left: 2px;
	margin-right: 2px;	
	color: white;
	padding: 0px;
	font-size: 8pt;
	background: url('/cms/images/bluegradient.jpg');
}

</style>
<body>
<form method="post" action="edit.php">
	<textarea id="subcontent" name="subcontent" style="width: 100%; height: 330px; padding: 5px;"><?php echo $content; ?></textarea>

 	<iframe id="vault" src="imagevault.php" frameborder="0"></iframe>

 	<center>
		<input class="button" type="submit" value="Save & Close" onclick="green(this);">
		<input class="button" type="button" value="Cancel" onclick="window.close();">
		<input type="checkbox" name="original_size" value="1">Maintain original sizes
		<input type="checkbox" name="popup" value="1" <?php if ($popup) echo 'checked'; ?>>Pop-up on click
	</center>

	<input type="hidden" name="action" value="editsub">
	<input type="hidden" name="id" value="<?php echo $_GET['subid']; ?>">
<?php 
imghunt_before();
	} else { 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Edit Title</title>
<style>

body{
	margin: 0px;
	font-family: sans-serif;
	font-size: 10pt;
	background: #F0F8FF;
	}

form{
	margin: 0px;
	padding: 0px;
	}

.table{
	width: 100%;
	border-collapse: collapse;
	background: #F0F8FF;
	text-align: center;
	}

.table th{
	background: url('/cms/images/pattern1.jpg');
	}

.table td, th{
	border-bottom: 1px solid gray;
	padding: 3px;
	}

.textbox{
	width: 95%;
	border: 1px solid gray;
	padding: 1px;
	}

.textbox:focus{
	background: #FFFFCC;
	}

.tr1{
	background: #DCEFFF;
	}

.button{
	cursor: pointer;
	margin-left: 2px;
	margin-right: 2px;	
	color: white;
	padding: 0px;
	font-size: 9pt;
	background: url('/cms/images/bluegradient.jpg');
}

</style>
</head>
<body>

<form action="edit.php" method="post">
	<input type="hidden" name="action" value="<?php echo $_GET['action']; ?>">
	<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">

<?php
	$id = intval($_GET['id']);
	switch ($_GET['action']){
		case 'editblocktitle': 
			$query = "select title FROM blocks where blockid='$id'";
			$result = mysqli_query($Link,$query);
			$row = mysqli_fetch_assoc($result);
			$varname = 'blocktitle';
			break;
		case 'editsubtitle': 
			$query = "select title FROM subblocks where subid='$id'";
			$result = mysqli_query($Link,$query);
			$row = mysqli_fetch_assoc($result);
			$varname = 'subblocktitle';
			break;
	}
	mysqli_close($Link);
?>

<table class="table">
	<tr><th colspan="2">Edit Title</th></tr>
	<tr>
		<td>
			<input type="textbox" name="<?php echo $varname; ?>" value="<?php echo $row['title']; ?>" class="textbox" maxlength="150">
		</td>
	</tr>
	<tr class="tr1">
		<td>
			<input type="submit" value="Save & Close" class="button" onclick="green(this);">
			<input type="button" value="Cancel" class="button" onclick="window.close();">	
		</td>
	</tr>	
</table>

<?php } ?>

</form>
</body>
</html>
