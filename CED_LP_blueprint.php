<?php include('PARTIAL_ced_header.php'); ?>


<!-- ced_blueprint -->
<section>
    <div style="width: 100%;">
             <div class="row">
                <div class="col-12"><img src="https://cedky.com/cdn/11818_blueprint-header.jpg" width="100%"></div>
            </div>
    </div>
</section>


<section class="section">
    <div class="container">
        <div class="row row-eq-height justify-content-center">
            <div class="col-8">
                <h1 class="clr-blue">Executive Summary</h1>
<p>
This collaborative blueprint is a product of collaboration between the Kentucky Cabinet for Economic
Development (KCED) and the Kentucky Association for Economic Development (KAED). This blueprint
is by Kentucky’s economic developers, for Kentucky’s economic developers. It lays out an economic
development blueprint that economic developers and their stakeholders could follow to spur growth in
Kentucky over the next five years. The blueprint was not designed to cover policy areas, or other topics
that fall outside economic developers’ primary areas of influence.
</p><p>
The blueprint emphasizes a set of priorities, including five priority sectors and five priority enablers
that will help realize growth in these priority sectors, where economic developers may choose to
align, prioritize, and act to generate growth for the Commonwealth of Kentucky. These ten priorities
also reflect how the Commonwealth’s diverse communities can contribute to advancing Kentucky’s
prosperity in their unique yet still interdependent ways.
</p><p>
This blueprint builds upon the foundations of Kentucky’s recent momentum, including historic wins in
future-facing sectors such as electric vehicles. It looks toward sustaining this momentum by providing
a guide for Kentucky’s economic development professionals, in coordination with KCED and KAED,
based upon data, insights, and experience in Kentucky and more broadly.
</p><p>
The blueprint also highlights how the role played by economic developers varies by priority. Economic
developers will take the lead in identifying and delivering specific opportunities, including developing
state, regional, and local priorities, specific strategies around site development, and assembling
incentive packages, at the local and state levels to best enable success and attractive incentive
packages. In other areas, their role will be more indirect. They will help convene and support other
professionals who are recognized as the most expert and accountable in their domains, such as
workforce and community development practitioners. Economic developers can thus play a critical
role as the essential collaborators who bring together stakeholders to deliver results in the five priority
sectors and five priority enablers across Kentucky’s diverse regions.
</p><p>
Therefore, this blueprint seeks to foster clear ownership and defined collaboration to accelerate
Kentucky’s economic development.
</p>
        </div>
        <div class="col-4">

<section>
    <style>
        .col-lg-3 img {
            width: 80px;
        }

        .docs > .row {
            border-bottom: 2px solid #f7f7f7;
            padding-bottom: 25px;
            margin-bottom: 25px;
        }
    </style>
    <div class="docs ">
        <div class="row">
            <div class="col-lg-12 text-center">
            <h3>Downloadable Documentation</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <a href="https://cedky.com/cdn/11818_Full_report_Print_.pdf" target="_blank">
                <img src="https://cedky.com/cdn/pdf.webp" />
                </a>
            </div>
            <div class="col-lg-9">
                    <h5>Full Report (Print Edition PDF)</h5>
                    <li><a href="https://cedky.com/cdn/11818_Full_report_Print_.pdf" target="_blank">Click to Download</a></li>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <a href="https://cedky.com/cdn/11818_Full_Report_Digital_View_.pdf" target="_blank">
                <img src="https://cedky.com/cdn/pdf.webp" />
                </a>
            </div>
            <div class="col-lg-9">
                    <h5>Full Report (Digital Edition PDF)</h5>
                    <li><a href="https://cedky.com/cdn/11818_Full_Report_Digital_View_.pdf" target="_blank">Click to Download</a></li>
            </div>
        </div>


           <div class="row">
            <div class="col-lg-3">
                <a href="https://cedky.com/cdn/11818_Executive_Summary_Print_.pdf" target="_blank">
                <img src="https://cedky.com/cdn/pdf.webp" />
                </a>
            </div>
            <div class="col-lg-9">
                    <h5>Executive Summary (Print Edition PDF)</h5>
                    <li><a href="https://cedky.com/cdn/11818_Executive_Summary_Print_.pdf" target="_blank">Click to Download</a></li>
            </div>
        </div>


           <div class="row">
            <div class="col-lg-3">
                <a href="https://cedky.com/cdn/11818_Executive_Summary_Digital_View_.pdf" target="_blank">
                <img src="https://cedky.com/cdn/pdf.webp" />
                </a>
            </div>
            <div class="col-lg-9">
                    <h5>Executive Summary (Digital Edition PDF)</h5>
                    <li><a href="https://cedky.com/cdn/11818_Executive_Summary_Digital_View_.pdf" target="_blank">Click to Download</a></li>
            </div>
        </div>


           <div class="row">
            <div class="col-lg-3">
                <a href="https://cedky.com/cdn/11818_Collaborative_Blueprint_-_Kentucky_Regions.pdf" target="_blank">
                <img src="https://cedky.com/cdn/pdf.webp" />
                </a>
            </div>
            <div class="col-lg-9">
                    <h5>Kentucky Regions (PDF)</h5>
                    <li><a href="https://cedky.com/cdn/11818_Collaborative_Blueprint_-_Kentucky_Regions.pdf" target="_blank">Click to Download</a></li>
            </div>
        </div>


           <div class="row">
            <div class="col-lg-3">
                <a href="https://cedky.com/cdn/11818_Ten_Shared_Priorities_for_the_Commonwealth.pdf" target="_blank">
                <img src="https://cedky.com/cdn/pdf.webp" />
                </a>
            </div>
            <div class="col-lg-9">
                    <h5>10 Shared Priorities for the Commonwealth (PDF)</h5>
                    <li><a href="https://cedky.com/cdn/11818_Ten_Shared_Priorities_for_the_Commonwealth.pdf" target="_blank">Click to Download</a></li>
            </div>
        </div>

       
</section>



        </div>
    </div>
</div>

<section>
    <div class="container-fluid" style="background: #26292B; color: white; margin-top: 1em; padding-top: 1em;">
        <div class="container">
             <div class="row row-eq-height justify-content-center">
                <div class="col-12">
                <h2>Ten priorities for Kentucky</h2>
                <img src="https://cedky.com/cdn/11818_helix.svg" width="100%">
            </div>
        </div>
    </div>
</div>


<section class="section">
    <div class="container">
        <div class="row row-eq-height justify-content-center">
            <div class="col-12">
<h2>Overview of stakeholder engagement</h2>
<ul>
    <li>
Leadership from trade associations and organizations representing thousands of
members, from the Kentucky Association of Area Development Districts, the Kentucky
Association of Counties, and the Kentucky League of Cities to the Kentucky Chamber of
Commerce and Kentucky Association of Manufacturers.
</li><li>

1,000+ validated responses from two Kentucky Association for Economic Developers/
Kentucky Cabinet of Economic Development joint surveys.
</li><li>
50+ stakeholders engaged through nine virtual workshops with the Economic Development
Advisory Board, the East Kentucky Advisory Board, and representatives of Kentucky’s utilities.
See Appendix III for a list of members for the Economic Development Advisory Board and the
East Kentucky Advisory Board.
</li><li>
110+ stakeholders engaged through five regional in-person and cross-functional workshops
hosted in West Kentucky (Madisonville), South-Central Kentucky (Glasgow), Central
Kentucky (Georgetown), and East Kentucky (Pikeville and Morehead).
</li><li>
75+ stakeholders engaged in one-on-one or small group interviews, including 30+ Kentucky
state government agency leaders and 15+ workforce and education leaders.
</li></ul>

<img src="https://cedky.com/cdn/11818_stakeholders.jpg" width="100%">

</div></div></div>
</section>

<section>
    <div class="container-fluid" style="margin-top: 1em; padding-top: 1em;">
        <div class="container">
             <div class="row row-eq-height justify-content-center">
                <div class="col-12">
                <h2>Kentucky Regions</h2>
                <img src="https://cedky.com/cdn/11818_regions.svg" width="100%">
            </div>
        </div>
    </div>
</div>


<section>
    <div class="container-fluid" style="margin-top: 1em; padding-top: 1em;">
        <div class="container">

<h1>Key Analyses and Data Trends</h1>

<h2>Economic Overview</h2>
<ol>
<li><a href="https://cedky.com/cdn/11818_Economic_Overview_-_Sectors_and_Firms.pdf" target="_blank">Sectors and Firms </a></li>

<li><a href="https://cedky.com/cdn/11818_Economic_Overview_-_Talent___Human_Capital.pdf" target="_blank">Talent & Human Capital </a></li>

<li><a href="https://cedky.com/cdn/11818_Economic_Overview_-_Capital_and_Innovation.pdf" target="_blank">Capital and Innovation </a></li>

<li><a href="https://cedky.com/cdn/11818_Economic_Overview_-_Infrastructure_.pdf" target="_blank">Infrastructure </a></li>

<li><a href="https://cedky.com/cdn/11818_Economic_Overview_-_Business_Climate.pdf" target="_blank">Business Climate </a></li>

<li><a href="https://cedky.com/cdn/11818_Economic_Overview_-_Economic_Development_Operating_Capabilities.pdf" target="_blank">Economic Development Operating Capabilities </a></li>

<li><a href="https://cedky.com/cdn/11818_Foreign_Direct_Investment___Export_Analysis.pdf" target="_blank">Foreign Direct Investment & Export Analysis </a></li>
</ol>
 

<h2>Regional Industry Specialization</h2>
<ol>
<li><a href="https://cedky.com/cdn/11818_Regional_Industry_Specialization_-_Statewide_and_All_Regions.pdf" target="_blank">Statewide and All Regions </a></li>

<li><a href="https://cedky.com/cdn/11818_Regional_Industry_Specialization_-_Central_Region.pdf" target="_blank">Central Region </a></li>

<li><a href="https://cedky.com/cdn/11818_Regional_Industry_Specialization_-_East_Region.pdf" target="_blank">East Region </a></li>

<li><a href="https://cedky.com/cdn/11818_Regional_Industry_Specialization_-_South_Central_Region.pdf" target="_blank">South Central Region </a></li>

<li><a href="https://cedky.com/cdn/11818_Regional_Industry_Specialization_-_West_Region.pdf" target="_blank">West Region </a></li>
</ol>





</div>
</div>
</section>


<?php include('PARTIAL_ced_footer.php'); ?>