<?php 
$title = "Starting a Business | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Starting a Business</h1>
                <hr class="margin-40">

<p>
Whether you are a small business owner or considering starting your own business, the <a href="http://onestop.ky.gov/Pages/default.aspx" target="_blank">Kentucky Business One Stop</a> portal was designed for you to find the requirements and tools you need to own and operate a business in Kentucky.
</p>
    
<p> 
The portal contains step-by-step instruction as well as links such as:
</p>

<ul class="singleSpace">
    <li>Plan a business</li>
    <li>Start a business</li>
    <li>Operate a business</li>
    <li>Expand a business</li>
    <li>Move to Kentucky</li>
</ul>
<br>
<p>
Our Office of Entrepreneurship is also available to answer questions and connect you to the appropriate resources.  Visit <a href="//kyinnovation.com" target="_blank">KYInnovation.com</a> for more information.
</p>

</section>












<?php include('NKY-footer.php'); ?>