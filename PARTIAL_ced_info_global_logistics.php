<section>
    <div class="container-fluid bg-blue">
        <div class="row">
            <div class="col-12 padding-40 text-center">
                <h2 class="clr-white">Global Logistics Capacity<br>
                    <small class="txt-md">1st in air cargo by volume, Kentucky is the only state to boast three major air cargo hubs: (UPS, DHL and Amazon Air)</small>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-3 bg-fltr bg-dk-fltr clr-white bg-cover" style="background-image:url(//ced.ky.gov/Webfiles/imgs/content/campaign/DHL.jpg);">
                <div class="infoBox">
                    <div class="content">
                        DHL’s global hub in Kentucky handles about 90% of DHL shipments entering the U.S. 
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 bg-fltr bg-dk-fltr clr-white bg-cover" style="background-image:url(//ced.ky.gov/Webfiles/imgs/content/campaign/fedex.jpg);">
                <div class="infoBox">
                    <div class="content">
                        FedEx recently expanded its ground facilities in Louisville and Northern Kentucky.
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 bg-fltr bg-dk-fltr clr-white bg-cover" style="background-image:url(//ced.ky.gov/Webfiles/imgs/content/campaign/UPS.jpg);">
                <div class="infoBox">
                    <div class="content">
                        UPS Worldport in Kentucky is the largest fully automated package-handling facility in the world.
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3 bg-fltr bg-dk-fltr clr-white bg-cover" style="background-image:url(//ced.ky.gov/Webfiles/imgs/content/campaign/prime-air.jpg);">
                <div class="infoBox">
                    <div class="content">
                        Amazon Air’s $1.5 billion hub came online in 2021.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>