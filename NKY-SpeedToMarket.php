<?php 
$title = "Speed to Market | Cabinet for Economic Development";
include('NKY-header.php'); ?>



<section>
    <div class="container my-5">


		<div class="row light-gray p-3">
			<div class="col-sm-12 col-md-6">
		    	<img src="/site/images/speed3.jpg" class="img-fluid">
		    </div>

			<div class="col-sm-12 col-md-6 ">
				<h2 class="new">Kentucky expedites approvals to get your business up and running fast.</h2>
				<p>
				   The commonwealth has taken an aggressive approach to the speed to market demand, matching companies' sense of urgency and expediting project timelines in a way many other states cannot.  This advantage has opened the doors to continue strong statewide economic growth through increased investment and job creation.
					</p><hr class="spacer-25">
				<p></p>
				<br clear="all">
			</div>
			

			</div>

		</div>
	</div>
</section>


<!-- <h2 class="text-center text-uppercase text-blue">A proven structure that assures speed to market</h2>
 -->
<div class="row mx-4">
            <div>
                <img src="/site/images/speed-graphic.jpg" class="img-fluid">
            </div>
</div>




         

<section>
    <div class="container my-5">

    	<div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Speed to Market</h1>
       		</div>
		</div>

		<div class="row light-gray p-3">
			<div class="col-sm-12 col-md-6 ">
				<h2 class="new">Kentucky's speed to market fueling unprecedented growth</h2>
				<p>
				    Kentucky is paving the way for future  development through its Build-Ready Sites and Kentucky Product Development Initiative (KPDI)
					</p><hr class="spacer-25">
				<p></p>
				<br clear="all">
			</div>
			<div class="col-sm-12 col-md-6">
		    	<img src="/site/images/speed1.jpg" class="img-fluid">
		    </div>

		</div>




		</div>
	</div>
</section>



<section id="BR_checklist" class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-12">
                    <h2>Overview</h2>
                    <p>
                        Find out how a Build-Ready site allows companies to bypass much of the red tape required when establishing a new location.
                         <hr class="spacer-60" />
                         <hr class="spacer-25" />
                    </p>
                    <a class="btn new" target="_blank" href="https://ced.ky.gov/BuildReady/Overview">Learn More &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <div class="col-lg-3 col-12">
                    <h2>Standards</h2>
                    <p>
                        All Build-Ready sites must meet strict criteria and standards. Find out why these sites are much more than just shovel-ready. 
                         <hr class="spacer-60" />
                         <hr class="spacer-25" />
                    </p>
                    <a class="btn new" target="_blank"  href="https://ced.ky.gov/BuildReady/Standards">Learn More &nbsp; <i class="fa fa-arrow-circle-right"></i>
</a>
                </div>
                <div class="col-lg-3 col-12">
                    <h2>Checklist</h2>
                    <p>
                        What has to be done before a site is certified Build-Ready? Click below to see a checklist of requirements.
                         <hr class="spacer-60" />
                         <hr class="spacer-25" />
                    </p>
                    <a class="btn new" target="_blank"  href="https://ced.ky.gov/BuildReady/Checklist">Learn More &nbsp; <i class="fa fa-arrow-circle-right"></i>
</a>
                </div>
                 <div class="col-lg-3 col-12">
                 	<img src="/site/images/speed2.jpg" class="img-fluid" />
                 </div>
        	</div>
        </div>
    </section>



<section class="container-fluid dark_blue_band py-4">
    <div class="dark_blue_band">
	    <div class="row text-center">
	    	<div class="col px-lg-5 px-sm-3">
	    		<h1 class="text-white"> Kentucky Product Development Initiative (KPDI) </h1> 
	    		<p class="text-white med-text text-center px-5">
	    			KPDI, a collaboration between the Kentucky Cabinet for Economic Development and Kentucky Association for Economic Development (KAED), includes $35 million per fiscal year in state funding toward upgrades of sites and buildings across the state. Learn more about this amazing program.

                            <br clear="all">
                    <a href="/LP/NKY_KPDI"   class="btn new big" style="border: 1px solid white;">Learn More</a>
                </p>
	    		</p>
	   		</div>
	    </div>
 </div></section>






 <?php include('NKY-footer.php'); ?>