<?php 
$title = "Contact Us | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Contact Us</h1>
        <hr class="margin-40">
        <hr class="spacer-20">
        <div class="row iconRow">
            <div class="col-12 col-md-4 text-center">
                
                <hr class="spacer-10">
                <span class="full text-big text-blue">Call Us</span>
                <hr class="spacer-5">
                <p>(800) 626-2930<br>
                (502) 564-7670<br>
                Fax: (502) 564-3256</p>
            </div>
            <div class="col-12 col-md-4 text-center">
                <hr class="spacer-10">
                <span class="full text-big text-blue">Address</span>
                <hr class="spacer-5">
                <p>500 Mero Street, 5th floor<br>
                Frankfort, KY 40601</p>
            </div>
            <div class="col-12 col-md-4 text-center">
                <hr class="spacer-10">
                <span class="full text-big text-blue">Appearance Request</span>
                <hr class="spacer-10">
                <p><a class="btn" href="https://governor.ky.gov/contact/scheduling-requests" target="_blank">Request Form</a></p>
            </div>
        </div>  
        <hr class="spacer-40">
        <div class="row text-center">
            <div class="col-12">
                To contact the Cabinet by email, send inquiries to <a href="mailto:econdev@ky.gov">econdev@ky.gov</a>.
            </div>
        </div>  
        </div>
        </div>
        </div>
        </section>        
  


  <?php include('NKY-footer.php'); ?>