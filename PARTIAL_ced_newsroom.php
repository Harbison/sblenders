<header id="header" class="bg-cover" style="background-image:url(/media/WebMedia/state-capitol-building-2.jpg);">
    <div class="gradient-fltr"></div>
    <div class="home-bg-fltr bg-fltr bg-dk-fltr"></div>
    <div id="pageHeader">
        <div class="container">
            <h2 class="h1">News Room</h2>
        </div>
    </div>
</header>


<section class="section">
    <div class="container">
        <div class="row">
            <aside id="sideBar" class="col-12 col-md-3">
                <section>
                    <div class="widget">
                        <h4 class="h4">Quick Links</h4>
                        <hr class="spacer-30">
                         <ul>
                        <section>
                            <ul id="quickLinks">
                                <li><a href="/Newsroom/News_Releases">News Releases</a></li>
                                <li><a href="/Newsroom/Newsletters">Newsletters</a></li>
                                <li><a href="/Newsroom/Publications">Publications</a></li>
                                <li><a href="/Newsroom/KEDFA_Meeting_Approvals">KEDFA Meeting News</a></li>
                                <li><a href="/Newsroom/Annual_Reports">Annual Reports</a></li>
                                <li><a href="/Newsroom/Articles">Articles</a></li>
                                <li><a href="/Newsroom/Webinars">Webinars</a></li>
                                </div>
                </section>
            </aside>