<?php 
$title = "Top Industries | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Top Industries</h1>
                <p class="text-blue">
                <span class="med-text">
              Kentucky is a national leader in <b>manufacturing, electric vehicle battery production, automotive manufacturing,air cargo by weight,bourbon exports </b>and more. With a skilled and dedicated workforce, prime geographic location that ensures reliability in the supply chain and distribution, and a commitment to help companies build a foundation for the future, Kentucky has everything to help industries succeed.
                </span>
            </div>
        </div>
    </div>
</section>

<iframe style="aspect-ratio: 16 / 9; width: 100%;"  src="https://www.youtube.com/embed/7qxTvEZTtBQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>




<!-- LIFE COMMUNITY IMAGES -->
<section class="container mt-5">
	    <div class="row text-center">

	    	<div class="col-lg-6 mb-3">
	    		<iframe style="aspect-ratio: 16 / 9; width: 100%;" src="https://www.youtube.com/embed/jvM6sjcFlpg?si=LK0vUhOktKsw8CTA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
	    		Manufacturing 
	    	</div>
	    	<div class="col-lg-6 mb-3">
	    		<iframe style="aspect-ratio: 16 / 9; width: 100%;"  src="https://www.youtube.com/embed/5ZGJM-23_ok?si=u7MtVt8gRtsuJjNf" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
	    		Bourbon
	    	</div>
	    	<!-- <div class="col-lg-4 mb-3">
	    		<img class="img-fluid"  src="/site/images/ashland.jpg">
	    		Electric Vehicles
	    	</div> -->
	    </div>
</section>


<section class="container mt-3">
	<div class="row">
		<div class="col-lg-2">
			<img src="/site/images/i-auto.jpg" height="100">
		</div>
		<div class="col-lg-10 text-medium">
	Kentucky ranks #1 in vehicle production per capita and is the national
	leader for electric vehicle battery production.
		</div>
<hr class="spacer-40"> 
		<div class="col-lg-2">
			<img src="/site/images/i-2.jpg" height="100">
		</div>
		<div class="col-lg-10 text-medium">
	Kentucky is a national leader in the manufacturing sector, with more than 250,000 Kentuckians employed in the industry.
		</div>
<hr class="spacer-40"> 
		<div class="col-lg-2">
			<img src="/site/images/i-3.jpg" height="100">
		</div>
		<div class="col-lg-10 text-medium">
	Kentucky’s central location has made it a national logistics hub and a primelocation for the hospitality businesses thanks to its booming tourism industry.
		</div>
<hr class="spacer-40"> 
		<div class="col-lg-2">
			<img src="/site/images/i-4.jpg" height="100">
		</div>
		<div class="col-lg-10 text-medium">
	Kentucky is home to a number of business, financial, information technology, and other professional services, which have seen success thanks to our centrallocation and strong workforce.
		</div>
<hr class="spacer-40"> 
		<div class="col-lg-2">
			<img src="/site/images/i-5.jpg" height="100">
		</div>
		<div class="col-lg-10 text-medium">
	Kentucky’s commitment to innovation and commitment to cultivating industryclusters has helped fuel success in areas such as aerospace, agritech and lifesciences.
		</div>
	</div>
</section>



<section class="section">
    <div class="container">
        <div class="text-center">
            <div class="clr-gray subHead">Kentucky is proud to promote</div>
            <hr class="spacer-10">
            <h2 class="h2 clr-blue">The Company We Keep</h2>
            <hr class="spacer-40">
        </div>
        <div id="logoSlider">
            <div class="slide">
                <a href="https://www.toyota.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/toyota-2.png" alt="Toyota" /></a>
            </div>
            <div class="slide">
                <a href="https://www.amazon.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/amazon.png" alt="Amazon" /></a>
            </div>
            <div class="slide">
                <a href="https://www.ups.com/us/en/global.page" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/ups.png" alt="UPS" /></a>
            </div>
            <div class="slide">
                <a href="https://www.ford.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/ford.png" alt="Ford" /></a>
            </div>
            <div class="slide">
                <a href="https://www.dhl.com/en.html" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/DHL.png" alt="DHL" /></a>
            </div>
            <div class="slide">
                <a href="https://www.jimbeam.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/jim-beam.png" alt="Jim Beam" /></a>
            </div>
            <div class="slide">
                <a href="https://www.beamsuntory.com/en" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/beam-suntory.png" alt="Beam Suntory" /></a>
            </div>
            <div class="slide">
                <a href="https://www.chevrolet.com/performance/corvette" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/corvette.png" alt="Corvettte" /></a>
            </div>
            <div class="slide">
                <a href="https://www.ge.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/GE.png" alt="General Electric" /></a>
            </div>
            <div class="slide">
                <a href="https://www.alltech.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/alltech.png" alt="Alltech" /></a>
            </div>
            <div class="slide">
                <a href="https://nucor.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/nucor.png" alt="Nucor" /></a>
            </div>
            <div class="slide">
                <a href="https://www.valvoline.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/valvoline.png" alt="Valvoline" /></a>
            </div>
            <div class="slide">
                <a href="https://www.tempursealy.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/tempur_sealy.png" alt="Tempur Sealy" /></a>
            </div>
            <div class="slide">
                <a href="https://www.lexmark.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/lexmark.png" alt="Lexmark" /></a>
            </div>
            <div class="slide">
                <a href="https://www.yum.com/" target="_blank"><img src="https://ced.ky.gov/media/WebMedia/yum-brands.png" alt="Yum Brands" /></a>
            </div>

        </div>
    </div>
</section>




<?php include('NKY-footer.php'); ?>
