
var ContentComp = { 
    name: 'content-comp',
    data: function() {
       return {
          articles: articles.sort((a, b) => (a.publish_date < b.publish_date) ? 1 : -1)
       }
    },
    methods: {
        articleDate(testDate) {
            return moment(testDate).format('MMMM YYYY')
        },
        makeLink(file) {
            return 'Article.aspx?x=' + file;
        },
        imgUrl(file) {
            return 'PRBodies/' + file;
        },
        titleCase(str) {
            return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }
    },
    template: '<div>' +          

            '<div class="NLrow" v-for="article in articles" :key="article.page_file" >' +
            '<div class="NLtitle"><h6 class="h6">{{ articleDate(article.publish_date) }} <span style="float: right; font-size: 10pt;">Published: {{article.publisher}}</span></h6>' +
                '<hr class="spacer-10"><h2 class="h4 clr-blue">{{ titleCase(article.headline) }}</h2>' + 
                '<hr class="spacer-20"></div>' + 
            '<div class="NLimg">' +
                '<img v-if="article.feature_img" class="fullWidth" :src="imgUrl(article.feature_img)" :alt="article.headline"></div>' +
            '<div class="NLdesc">' +
                '<p>{{article.sub_headline}} </p><br>' +
                '<a :href="makeLink(article.page_file)" class="btn sm">Continue Reading</a></div>' +
            '</div>'
 }
 
 new Vue({
  el: '#vue-app',
  components: {
    'content-comp': ContentComp
  }
 })
 