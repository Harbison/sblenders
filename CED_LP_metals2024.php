<?php include('PARTIAL_ced_header.php'); ?>

<!-- ced_metals2024 -->
<section>
    <div style="width: 100%;">
             <div class="row">
                <div class="col-12"><img src="https://cedky.com/cdn/12011_video_placeholder.jpg" width="100%"></div>
            </div>
            <div class="col-12"><img src="https://cedky.com/cdn/12011_title.jpg" width="100%"></div>
    </div>
</section>


<section class="section">
    <div class="container">
        <div class="row row-eq-height justify-content-center mt-5">
            <div class="col-12">
                <h1 class="clr-blue">Strong, Flexible and Ready for the Future</h1>

                Kentucky offers a wealth of primary metal manufacturing facilities, including aluminum, copper and steel., as well as Those facilities provide materials and fabricationservices for hundreds of products around the globe. Listed below is additional information about the aluminum, copper, steel and presences in Kentucky, as well asthestate’s.ThroughtheMetalsInnovationInitiative, Kentucky’s leadingmetals companies have joined forces to collaboratively address common industry issues, suchasworkforceandsustainability,andto further establishKentucky as the epicenter of the industry’s future through innovation.

            </div>
        </div>

         <div class="row mt-5">
            <div class="col-md-6 col-sm-12">
              
            	<iframe width="560" height="315" src="https://www.youtube.com/embed/UgrnpTLsZvQ?si=43PxMVIy6l15q-2I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
              

            </div>
            <div class="col-md-6 col-sm-12">
              
            	<h3>2023 Kentucky Metals Industry Guide</h3>
              	<div class="row">
              		<div class="col-6">
              			<img width="100%" src="https://cedky.com/cdn/11724_metals-cover-2023.jpg">
              		</div>
              		<div class="col-6">
              			The 2023 Kentucky MetalsIndustry Guide provides acomprehensive view of thestate’s primary and fabricatedmetals sectors, as well as helpful information about thestate and its business environment

              			<a href="" class="btn ">View Now</a>
              		</div>
              	</div>

            </div>

        </div>
        <div class="container">
	        <div class="row row-eq-height text-center mt-5 ">
	            <div class="col-12">
	                <h1 class="clr-blue mb-5">Metals Impact in Kentucky</h1>
	            </div>
	            <div class="col-4">
	            	<h2>Facilities</h2>
	            	<img width="50%" src="https://cedky.com/cdn/12011_facilities.jpg"><br>
	            </div>
	            <div class="col-4">
	            	<h2>Employment</h2>
	            	<img width="50%" src="https://cedky.com/cdn/12011_employment.jpg"><br>
	            </div>
	            <div class="col-4">
	            	<h2>Facilities</h2>
	            	<img width="50%" src="https://cedky.com/cdn/12011_investment.jpg"><br>
	            </div>

	            <div class="col-12">
	                <a href="" class="btn">Metals Facilities</a>
	            </div>
	         </div>
     	</div>

</section>


<section class="section">
        <div class="row text-center ">
            <div class="col-12">
                <h1 class="clr-blue m-5">Major Metals Investment</h1>
                <p>
                <!-- content -->
            	</p>
        		<img class="img-fluid" src="https://cedky.com/cdn/12011_metals-investment.jpg"/>
    		</div>
    	</div>
</section>

<section class="section">
        <div class="row row-eq-height justify-content-center text-center ">
            <div class="col-12">
                <h1 class="clr-blue m-5">Why Metals Leaders Choose Kentucky</h1>
                <p>
                <!-- content -->
            	</p>

            	<img class="img-fluid" src="https://cedky.com/cdn/12011_ceo-quote.jpg"/>
        		
    		</div>
    	</div>
    	<div class="container">
    		<div class="row row-eq-height justify-content-center text-center ">
	    		<div class="col-3">
	    			<a href=""  style="width: 100%; height: 100%; line-height: 5em;"  class="btn">Speed to Market</a>
	    		</div>
	    		<div class="col-3">
	    			<a href="" style="width: 100%; height: 100%; line-height: 5em;" class="btn">Logistics</a>
	    		</div>
	    		<div class="col-3">
	    			<a href="" style="width: 100%; height: 100%; line-height: 5em;" class="btn">Cost of Business</a>
	    		</div>
	    		<div class="col-3">
	    			<a href="" style="width: 100%; height: 100%; line-height: 5em;" class="btn">Quality of Life</a>
	    		</div>
	    	</div>
    	</div>
</section>

<section>
	<div class="container">
        <div class="row row-eq-height text-center mt-5 ">
            <div class="col-12">
                <h1 class="clr-blue mb-5">Aluminum & Copper</h1>
            </div>
            <div class="col-4">
            	<h2>Facilities</h2>
            	<img height="180" src="https://cedky.com/cdn/12011_facilities.jpg"><br>
            	<p>There are more than 180 aluminum and copper-related facilities in the commonwealth
            	</p>
            </div>
            <div class="col-4">
            	<h2>Employment</h2>
            	<img height="180" src="https://cedky.com/cdn/12011_employment.jpg"><br>
            	<p>
            		The aluminum and copper industries employ over 21,000 Kentuckians full time.
            	</p>
            </div>
            <div class="col-4">
            	<h2>Investment</h2>
            	<img height="180" src="https://cedky.com/cdn/12011_investment.jpg"><br>
            	<p>
            		Since 2017, aluminum companies have announced nearly 90 new or expansion projects totaling $2.7 billion in investment and the announcement of roughly 3,000 jobs
            	</p>
            </div>

            <div class="col-12">
                <a href="" class="btn">Aluminum and Copper Facilities</a>
            </div>
         </div>
 	</div>
</section>

<section>
	<div class="container">
        <div class="row row-eq-height text-center mt-5 ">
            <div class="col-12">
                <h1 class="clr-blue mb-5">Steel and Iron</h1>
            </div>
            <div class="col-4">
            	<h2>Facilities</h2>
            	<img height="180" src="https://cedky.com/cdn/12011_facilities.jpg"><br>
            	<p>Currently, 43 steel and iron production companies operate inKentucky
            	</p>
            </div>
            <div class="col-4">
            	<h2>Employment</h2>
            	<img height="180" src="https://cedky.com/cdn/12011_employment.jpg"><br>
            	<p>
            		Steel and iron companies employ nearly 6,400 full-time workers in Kentucky
            	</p>
            </div>
            <div class="col-4">
            	<h2>Investment</h2>
            	<img height="180" src="https://cedky.com/cdn/12011_investment.jpg"><br>
            	<p>
            		Since 2017, steel companies have announced nearly 90 new or expansion projects totaling $2.7 billion in investment and the announcement of roughly 3,000 jobs
            	</p>
            </div>

            <div class="col-12">
                <a href="" class="btn">Steel and Iron Facilities</a>
            </div>
         </div>
 	</div>
</section>



<section class="section">
        <div class="row text-center ">
            <div class="col-12">
                <h1 class="clr-blue m-5">Recycling and Sustainability</h1>
                <p>
                <!-- content -->
                </p>
                <img class="img-fluid" src="https://cedky.com/cdn/12011_lifecycle.jg.jpg"/>
            </div>
        </div>
</section>


<section class="section">
    <div class="container">
        <div class="row row-eq-height justify-content-center mt-5">
            <div class="col-12">
                <h1 class="clr-blue">Fostering Metals Innovation and Collaboration</h1>
            </div>
        </div>

         <div class="row mt-5">
            <div class="col-md-4 col-sm-12">
                <img class="img-fluid" src="https://cedky.com/cdn/12011_m2.jpg"/>
            </div>
            <div class="col-md-8 col-sm-12">
                <p>

                  The Metals Innovation Initiative (MI2) is a Kentucky-based nonprofit that provides collaborative, industry- led executive leadership to attract and promote advanced research, sustainability, commercialization and talent development in Kentucky’s metals industry–and, ultimately, for Kentucky to be seen as the preeminent destination for metals innovation. The Commonwe lth’s metals industry includes more than 250 facilities, employing more than 36,000 people statewide.


                </p>
                <p>
                    "The organization was formed in 2022 by eight Kentucky metals companies—Kobe Aluminum Automotive Products, Logan Aluminum, North American Stainless, Novelis, Nucor, River Metals Recycling, Tri-Arrows Aluminum, and Wieland—and a commitment from the Kentucky Governor and the Cabinet for Economic Development for support, administered through the Kentucky Science and Technology Corporation, as well as many ecosystem partners. The concept originated from a whitepaper from AccelerateKY, formed in response to interest across the Commonwealth of Kentucky for a metals industry cluster organization and built on principles from Kentucky’s participation in the MIT Regional Entrepreneurship Acceleration Program."

                </p>
            </div>
        </div>
    </div>
</section>

<section class="section">
        <div class="row text-center ">
            <div class="col-12">
             
                <img class="img-fluid" src="https://cedky.com/cdn/12011_logos.jpg"/>
            </div>
        </div>
</section>




<section>
    <div class="container">
        <div class="row text-center g-1 ">
            <div class="col-12">
                <h1 class="clr-blue mb-5">Metals Make it in Kentucky</h1>
                <p> Kentucky metals feed industries across the state and the globe.</p>
            </div>
            <div class="col-3">
                <b>Food and Beverage</b><br>
                <img class="img-fluid" src="https://cedky.com/cdn/12011_cans.jpg"><br>
                <p>
                </p>
            </div>
            <div class="col-3">
                <b>Aerospace</b><br>
                <img class="img-fluid" src="https://cedky.com/cdn/12011_aerospace.jpg"><br>
                <p>
                </p>
            </div>
            <div class="col-3">
                <b>Automotive</b><br>
                <img class="img-fluid" src="https://cedky.com/cdn/12011_automotive.jpg"><br>
                <p>
                </p>
            </div>
            <div class="col-3">
                <b>Advanced Manufacturing</b><br>
                <img class="img-fluid" src="https://cedky.com/cdn/12011_advanced_manufacturing.jpg"><br>
                <p>
                </p>
            </div>
         </div>
    </div>
</section>





<?php include('PARTIAL_ced_footer.php'); ?>
