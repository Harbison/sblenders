<?php 
$title = "Access To Capital | Cabinet for Economic Development";
include('NKY-header.php'); ?>



<!-- PAGE CONTENT -->

<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Access to Capital</h1>
<hr class="margin-40">
<p>Capital is one of an entrepreneur’s biggest needs to launch and grow a company. Yet it is also one of the most difficult challenges. The Cabinet for Economic Development has a variety of successful tools that match entrepreneurs with the funding they need – whether that is access to investors, small business loans or tax credits. Outlined below are some of those tools.</p>
<hr class="spacer-60">
<h3>Kentucky Small Business Tax Credit</h3>
<hr class="spacer-20">
<p>If your small business has hired at least one new full-time position and invested in qualifying equipment, you could be eligible for a tax credit.</p>
<a class="btn read" target="_blank" href="https://ced.ky.gov/Entrepreneurship/KSBTC"> Learn More</a><hr class="spacer-60">
<h3>Export Grants</h3>
<hr class="spacer-20">
<p>Through the Kentucky Export Initiative, small businesses can receive STEP grant funding for export-related activities each year.</p>
<a class="btn read"  target="_blank" href="https://ced.ky.gov/International/Exports"> Learn More</a><hr class="spacer-60">
<h3>Kentucky Small Business Credit Initiative</h3>
<hr class="spacer-20">
<p>This federally funded lending support program helps lenders finance creditworthy small businesses that would typically fall just outside of their normal lending guidelines.</p>
<a class="btn read"  target="_blank" href="https://ced.ky.gov/entrepreneurship/KSBCI"> Learn More</a><hr class="spacer-60">
<h3>Small Business Loans</h3>
<hr class="spacer-20">
<p>Small business engaged in manufacturing, agribusiness, or service and technology may be eligible for loans to acquire land and buildings, purchase and install equipment, or for working capital.</p>
<a class="btn read"  target="_blank" href="https://ced.ky.gov/Entrepreneurship/KEDFA"> Learn More</a><hr class="spacer-60">
<h3>Investors</h3>
<hr class="spacer-20">
<p>Investors throughout Kentucky are eager to foster the growth of small businesses. The Cabinet has compiled information for investors, as well as lists of prospective investors.</p>
<a class="btn read"  target="_blank" href="https://ced.ky.gov/Entrepreneurship/Investor_Information"> Learn More</a><hr class="spacer-60">
<h3>Angel Investment Tax Credit</h3>
<hr class="spacer-20">
<p>The Kentucky Angel Investment Tax Credit offers credits of up to 40 percent of an investment in Kentucky small businesses.</p>
<a class="btn read"  target="_blank" href="https://ced.ky.gov/Entrepreneurship/KAITC"> Learn More</a><hr class="spacer-60">
<h3>Kentucky Investment Fund Act</h3>
<hr class="spacer-20">
<p>The Kentucky Investment Fund Act offers a 25 percent tax credit to certain personal and corporate investors in approved investment funds.</p>
<a class="btn read" href="https://ced.ky.gov/Entrepreneurship/KIFA" target="_blank" rel="noopener"> Learn More</a><hr class="spacer-60">
<h3>SBIR-STTR Matching Funds Program</h3>
<hr class="spacer-20">
<p>The Cabinet will match, on a competitive basis, Phase 1 and Phase 2 federal Small Business Innovation Research (SBIR) and Small Business Technology Transfer (STTR) awards received by Kentucky high-tech small businesses and those willing to become Kentucky-based businesses.</p>
<a class="btn read"  target="_blank" href="https://ced.ky.gov/Entrepreneurship/SBIR_STTR"> Learn More</a><hr class="spacer-60">
<h3>Kentucky Selling Farmer Tax Credit</h3>
<hr class="spacer-20">
<p>If you are a small farmer selling eligible agricultural assets to a beginning farmer, you may qualify for a tax credit.</p>
<a class="btn read"  target="_blank"  href="https://ced.ky.gov/Entrepreneurship/KSFTC"> Learn More</a><hr class="spacer-60">
<h3>Other Opportunities for Businesses</h3>
<hr class="spacer-20">
<p>Click below for a full list of Kentucky business incentives.</p>
<p><a class="btn read" href="https://ced.ky.gov/Locating_Expanding/kybizince" target="_blank" rel="noopener"> Business Incentives</a></p>
		</div>
	</div>
</div>
</section>






 <?php include('NKY-footer.php'); ?>