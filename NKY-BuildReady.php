<?php 
$title = "Build Ready | Kentucky Cabinet for Economic Development";
include('NKY-header.php'); ?>



    <div class="container">
        <div class="row">
                <nav class="navbar navbar-expand-lg" style="padding-left: 0;">
                
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item"><a href="https://ced.ky.gov/BuildReady/Overview">Overview</a></li>
                    <li class="nav-item"><a href="https://ced.ky.gov/BuildReady/Standards">Standards</a></li>
                    <li class="nav-item"><a href="https://ced.ky.gov/BuildReady/Sites">Sites</a></li>
                    <li class="nav-item"><a href="https://ced.ky.gov/BuildReady/Checklist">Checklist</a></li>
                    <li class="nav-item"><a href="https://ced.ky.gov/BuildReady/WhatNext">What Next</a></li>
                    <li class="nav-item"><a href="https://ced.ky.gov/BuildReady/FAQs">FAQs</a></li>

                    <li class="nav-item"><a href="" data-toggle="modal" data-target="#buildready_modal">Contact Us</a></li>

                   <!--  <li class="nav-item"><a id="BuildReadyContactUs" href="#">Contact Us</a></li> -->

                </ul>
                </nav>

    </div>
</div>


 <!-- Modal -->
<div class="modal fade" id="buildready_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contact Us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      Department of Community Development<br>
 (502) 564-7670<br>
 CEDBldgCoord@ky.gov
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- PAGE CONTENT -->
<section id="BR_checklist">
    <div class="container my-3">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Kentucky is Build Ready.</h1>

        </div>
    </div>
</div>
</section>

<section id="BR_checklist" class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12">
                    <h2>Overview</h2>
                    <p>
                        Find out how a Build-Ready site allows companies to bypass much of the red tape required when establishing a new location.
                    </p>
                    <a class="btn read" href="https://ced.ky.gov/BuildReady/Overview">Learn More &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                <div class="col-lg-4 col-12">
                    <h2>Standards</h2>
                    <p>
                        All Build-Ready sites must meet strict criteria and standards. Find out why these sites are much more than just shovel-ready. 
                    </p>
                    <a class="btn read" href="https://ced.ky.gov/BuildReady/Standards">Learn More &nbsp; <i class="fa fa-arrow-circle-right"></i>
</a>
                </div>
                <div class="col-lg-4 col-12">
                    <h2>Checklist</h2>
                    <p>
                        What has to be done before a site is certified Build-Ready? Click below to see a checklist of requirements.
                    </p>
                    <a class="btn read" href="https://ced.ky.gov/BuildReady/Checklist">Learn More &nbsp; <i class="fa fa-arrow-circle-right"></i>
</a>
                </div>
                <div class="clear"></div>
        </div></div></section>
         
                                                   

<?php include('NKY-footer.php'); ?>
