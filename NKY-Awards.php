<?php 
$title = "Awards and Successes | Cabinet for Economic Development";
include('NKY-header.php'); ?>


<section>
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="new text-blue">Awards & Success Stories</h1>
                <p class="text-blue">
                <span class="med-text">
                Kentucky has become a national leader, and people across the world are taking notice. See why so
many businesses and people are making the Bluegrass their new Kentucky home.
                </span>
           
            </div>
        </div>
    </div>
</section>





<!-- LIFE COMMUNITY IMAGES -->
<section class="container mt-3">
        <div class="row text-center">

            <div class="col-md-6 col-sm-12 mb-3">
                <img src="/site/images/success.png" class="img-fluid"/>
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <img  class="img-fluid" src="/site/images/lexington.jpg">
                <!-- Lexington, Kentucky -->
            </div>
        </div>
</section>








 <?php include('NKY-footer.php'); ?>