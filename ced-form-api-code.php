CURL POST FOR CED API

php

<?php 
public static function post($url, $post) {
    
    //url-ify the data for the POST
    $fields_string = http_build_query($post);

    $ch = curl_init();

    //set the url, number of POST vars, POST data

    curl_setopt( $ch, CURLOPT_POST, 1 );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields_string );
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
            
    $response = curl_exec( $ch );


    //So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

    //execute post
    $result = curl_exec($ch);
    
    
    
    if($result === false)   {
        error_log($result);
        $result = 'Curl error: ' . curl_error($ch);
    }
    curl_close( $ch );

    return $result;
    
}



?>


JS 


<script>
        var form_data = $("#contact_form").serializeArray();
        
        // Add Submission Url to obj sent over
        form_data.push({name: 'submission_url', value: window.location.href }); // Returns full URL (https://example.com/path/example.html

        request_subject =  $("#request_subject").val();

        $.ajax({
          method: "POST",
          url: "https://cedky.com/api/2341155333/contact_form_submission",
          data: form_data
        })
        .done(function( msg, form_data ) {
            //console.log(msg.status);
            console.log(msg);
            //dialog.dialog( "close" );
            console.log(form_data);

            console.log(request_subject);




            if (msg.status == "success") {
                
                $(".validateTips").hide();
                
                $("#kyinno-form-body").html("Thank you for your reply!  We will be in touch");

          
                setTimeout(function(){
                  dialog.dialog( "close" );  
                 }, 3000);
            } else {
                $(".validateTips").text('There was an issue with submitting - please call us');
            } 
        });
     
</script>
