<?php $ced_title = 'CED KY Opportunity Zones'; ?>
<?php include('PARTIAL_ced_header.php'); ?>

<section id="mapContainer">	
	<div class="map-inner">
		<div class="map-container">
			<iframe id="countyMap" class="active" data-map-type="countyMap" width="500" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" title="KY Opportunity Zones" src="https://www.arcgis.com/apps/Embed/index.html?webmap=ad0ce67bf9944ffd9dd0d3fadef32211&extent=-89.8254,35.9785,-81.7119,39.5808&zoom=true&previewImage=false&scale=true&search=true&searchextent=true&disable_scroll=true&theme=light"></iframe>
			
			<iframe id="siteBuildingMap" data-map-type="siteBuildingMap" width="500" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" title="KY Opportunity Zones with Available Sites & Buildings" src="https://www.arcgis.com/apps/Embed/index.html?webmap=8829683f5e404f52accbc953f8b3acb8&extent=-89.8107,35.9337,-81.6972,39.5381&zoom=true&previewImage=false&scale=true&search=true&searchextent=true&disable_scroll=true&theme=light"></iframe>
		</div>
	</div>
	
	<div id="mapIntro" class="bg-fltr bg-fltr-black clr-white">
		<div id="mapType">
			<div class="countyMap active" data-map-type="countyMap">County Map</div> |
			<div class="siteBuildingMap"  data-map-type="siteBuildingMap">Industrial Sites &amp; Buildings Map</div>
		</div>
		<div id="mapControls">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>		
		<div id="countyMapIntro" class="mapIntro active" data-map-type="countyMap">
			<h4 class="h4 clr-white">Interactive</h4>
			<h2 class="h2 clr-green">Explore Kentucky’s <span class="clr-white">Opportunity Zones</span></h2>
			<hr class="spacer-40">
			<p>How to use:</p>
			<p>The highlighted counties on the map include one or more opportunity zones.</p>
			<ul>
				<li>To see if a particular address is located within an Opportunity Zone, type in the address in the box on the upper right corner area of the map.</li>
				<li>To zoom into street level in a particular county, click repeatedly on that county until you reach the area you wish to view.</li>
			</ul>
			<hr class="spacer-40">
			<div class="readyToUse txt-md">Ready To Use?</div>
			<hr class="spacer-20">
			You can also use the links at the top of this intro to switch maps.
		</div>
		<div id="siteBuildingMapIntro" class="mapIntro" data-map-type="siteBuildingMap">
			<h4 class="h4 clr-white">Available Industrial</h4>
			<h2 class="h2 clr-green">Sites &amp; Buildings <span class="clr-white">Map</span></h2>
			<hr class="spacer-40">
			<p>How to use:</p>
			<p>Kentucky has a variety of industrial sites and buildings in Opportunity Zones that are available immediately. 
Click on the map to learn more detail.</p>
			<hr class="spacer-40">
			<div class="readyToUse txt-md">Ready To Use?</div>
			<hr class="spacer-20">
			You can also use the links at the top of this intro to switch maps.
		</div>
	</div>
</section>


<section class="col-12 col-md-9">
<h1 class="h1 clr-blue">Explore Kentucky’s Opportunity Zones</h1>
<hr class="margin-40" />
<p>With 144 Opportunity Zones in 84 counties, Kentucky offers a wealth of opportunities.</p>
<p>&nbsp;</p>

<div class="annualReports">
	<h4 class="h4">Bluegrass State Skills Corporation (BSSC)</h4>
	 <ul class="singleSpace">
	    <li><a title="2024 BSSC Annual Report" href="https://cedky.com/cdn/141_BSSC_Annual_Report_2024.pdf" target="_blank">2024 Annual Report</a></li>
	</ul>
	<div id="accordion_bssc">
	  <div class="">
	    <div class="">
	      <h2 class="mb-0">
	        <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse_bssc" aria-expanded="false" aria-controls="2023">
	          Previous Years
	        </button>
	      </h2>
	    </div>
	    <div id="collapse_bssc" class="collapse" aria-labelledby="headingOne" data-parent="#accordion_bssc">
	      <div class="card-body">
	          <p><strong>Previous BSSC Reports</strong></p>
	          <ul class="singleSpace">
	            <li><a title="2023 Annual Report" href="https://cedky.com/cdn/141_BSSC_Annual_Report_2023.pdf">2023 Annual Report</a></li>
				<li><a title="2022 Annual Report" href="https://cedky.com/cdn/141_BSSC_Annual_Report_2022.pdf">2022 Annual Report</a></li>
				<li><a href="https://cedky.com/cdn/141_4_-_BSSC_Annual_Report_2021.pdf" target="_blank" rel="noopener">2021 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2020_BSSC_Annual_Report_v13.pdf?7" target="_blank" rel="noopener">2020 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2019_BSSC_Annual_Report.pdf?7" target="_blank" rel="noopener">2019 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2018_BSSC_Annual_Report.pdf?7" target="_blank" rel="noopener">2018 Annual Report</a></li>
				<li><a class="noCache" href="https://cedky.com/cdn/kyedc/AnnualReports/2017_BSSC_Annual_Report.pdf?7" target="_blank" rel="noopener">2017 Annual Report</a></li>
	          </ul>
	      </div>
	    </div>
	  </div>
	</div>
</div>




</section>









<?php include('PARTIAL_ced_footer.php'); ?>